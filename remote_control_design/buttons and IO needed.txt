PIN ASSIGNMENTS
===============

RFM69 uses:
-----------
X SCK/PCINT5/PB5
X PCINT4/MISO/PB4
X OC2A/MOSI/PCINT3/PB3
X OC1B/SS/PCINT2/PB2
X PCINT18/PD2

Left joystick uses:
-------------------
X ADC0/PC0 (horizontal axis)
X ADC1/PC1 (vertical axis)
- PD0 (push down select)

Right joystick uses:
--------------------
X ADC2/PC2 (horizontal axis)
X ADC3/PC3 (vertical axis)
- PD1 (push down select)

Left trigger uses:
------------------
X PB0

Right trigger uses:
-------------------
X PB1

Side buttons use:
-----------------
X PD3
X PD4

Battery level reader:
---------------------
X ADC4/PC4

Low battery LED:
----------------
X PD5

Buzzer uses:
------------
X PD6

BUTTON FUNCTIONS?
=================

- auto-calibrate (both joystick buttons down?)
- altitude control (left and right triggers)
- take off and landing (two buttons on the right)