#pragma once

#include <avr/io.h>

void inputInit();

uint8_t isButtonADown();
uint8_t isButtonBDown();

uint8_t isTriggerLeftDown();
uint8_t isTriggerRightDown();

uint8_t isJoystickLeftDown();
uint8_t isJoystickRightDown();

void getJoystickLeft(uint8_t *x, uint8_t *y);
void getJoystickRight(uint8_t *x, uint8_t *y);
