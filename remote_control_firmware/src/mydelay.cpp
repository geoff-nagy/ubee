#include "mydelay.h"

#include <util/delay.h>

void delayMicroseconds(uint16_t us)
{
	while(--us)
	{
		_delay_us(1);
	}
}

void delayMilliseconds(uint16_t ms)
{
	while(--ms)
	{
		_delay_ms(1);
	}
}
