#include "led.h"

#include <avr/io.h>
#include <util/delay.h>

#define LED_PORT PORTD
#define LED_DIR DDRD
#define LED_PIN 5

#define ON_TIME_MS 20
#define OFF_TIME_MS 40

void ledInit()
{
	LED_PORT &= ~(1 << LED_PIN);
	LED_DIR |= (1 << LED_PIN);
}

void ledFlash(uint8_t times)
{
	while(times--)
	{
		ledOn();
		_delay_ms(ON_TIME_MS);
		ledOff();
		_delay_ms(OFF_TIME_MS);
	}
}

void ledOn()
{
	LED_PORT |= (1 << LED_PIN);
}

void ledOff()
{
	LED_PORT &= ~(1 << LED_PIN);
}
