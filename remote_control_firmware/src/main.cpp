#include "buzzer.h"
#include "led.h"
#include "input.h"

#include "rfm69/RFM69.h"

#include <avr/io.h>
#include <util/delay.h>

// - - - wireless config settings - - - //

// common across MQC and remote control
#define FREQUENCY RF69_915MHZ
#define NETWORK_ID 45

// IDs are device-specific
#define REMOTE_ID 43												// ID of remote control
#define MQC_ID 44													// ID of MQC device

// defines how TX retries are handled, in the event the remote wants data back from us
#define NUM_RETRIES 5
#define RETRY_WAIT_MS 20

// - - - messaging settings - - - //

// command bytes
#define MSG_FLASH_LED 1						// instruct drone to flash its LED
#define MSG_SET_ID 2						// instruct drone to change its ID
#define MSG_SET_PARAMS 3					// instruct drone to change a set of parameters
#define MSG_REQUEST_DATA 4					// instruct drone to return some data
#define MSG_SINGLE_FLIGHT_COMMAND 5			// send flight control command (pitch, roll, yaw)
#define MSG_COMPOUND_FLIGHT_COMMAND 6		// send multiple different flight control commands to multiple ID'd drones
#define MSG_ACK 7							// generic acknowledgment
#define MSG_NAK 8							// generic non-acknowledgment
#define MSG_RECALIBRATE_IMU 9				// instruct drone to recalibrate its IMU
#define MSG_START_IMU_TEST 10				// instruct drone to start continuous transmission of IMU values
#define MSG_END_IMU_TEST 11					// instruct drone to stop continuous transmission of IMU values
#define MSG_IMU_STATE 12					// this is a special message from the drone containing IMU state
#define MSG_DATA_PACKET 13					// some data packet sent by the drone

// how often we transmit flight commands
#define TX_INTERVAL_MS 50

// - - - flight param settings - - - //

// used when setting control yoke values
#define NUM_FLIGHT_COMMAND_BYTES 4									// corresponds to the four bytes that represent control yoke state

// - - - misc debugging/visual info settings - - - //

// number of times to flash LED for certain events
#define LED_NUM_FLASHES_INIT 4										// after successful initialization
#define LED_NUM_FLASHES_CONFIG_COMMAND 2							// after receiving a non-[dis]arm configuration command
#define LED_NUM_FLASHES_ARM_COMMAND 6								// after receiving an arm or disarm command
#define LED_NUM_FLASHES_ERROR 10									// after some kind of error occurs

// - - - controller yoke settings - - - //

#define THROTTLE_DEAD_ZONE 5										// no throttle response below this yoke strength
#define THROTTLE_MIN_VAL 60											// minimum throttle response immediately above THROTTLE_DEAD_ZONE

#define YOKE_CENTER 128												// values considered to be the yoke center
#define YOKE_SMALL_DEAD_ZONE_RADIUS 2								// values within this much of the yoke center (128) are considered neutral (128)
#define YOKE_LARGE_DEAD_ZONE_RADIUS 10

// - - - special types - - - //

typedef enum THRUST_MODE
{
	THRUST_MODE_DOWN = 0,
	THRUST_MODE_HOVER,
	THRUST_MODE_UP
} ThrustMode;

// - - - prototypes - - - //

uint8_t deadZonifyThrottle(uint8_t yoke, uint8_t deadZone, uint8_t minThrottle);
uint8_t deadZonifyYoke(uint8_t yoke, uint8_t radius);

// - - - main function - - - //

int main(void)
{
	RFM69 rfm;
	unsigned long currentTime;
	unsigned long lastTXTime;

	uint8_t joystickLeftX, joystickLeftY;
	uint8_t joystickRightX, joystickRightY;

	uint8_t triggerLeftDown, triggerRightDown;
	uint8_t buttonADown, buttonBDown;
	uint8_t joystickLeftDown, joystickRightDown;

	uint8_t msgType;
	uint8_t command[7];
	ThrustMode thrustMode;

	// initialize Arduino library
	init();

	// initialize buzzer
	buzzerInit();

	// initialize input
	inputInit();

	// initialize LED output settings and turn on LED
	ledInit();
	ledOn();

	// initialize the RF transceiver
	rfm.initialize(FREQUENCY, REMOTE_ID, NETWORK_ID);
	rfm.setHighPower(true);
	rfm.setPowerLevel(31);

	// play successful start up tone
	//buzzerPlayIntro();

	// prime TX timing
	lastTXTime = millis();

	// main control loop
	while(true)
	{
		// see if it's time to do a read of the input and send it over the air
		currentTime = millis();
		if(currentTime - lastTXTime >= TX_INTERVAL_MS)
		{
			// wait until next interval
			lastTXTime = currentTime;

			// read joystick axes
			getJoystickLeft(&joystickLeftX, &joystickLeftY);
			getJoystickRight(&joystickRightX, &joystickRightY);

			// adjust throttle output a bit to have a bit of a dead zone and a value from THROTTLE_MIN_VAL to 255
			joystickLeftY = deadZonifyThrottle(joystickLeftY, THROTTLE_DEAD_ZONE, THROTTLE_MIN_VAL);

			// apply dead zone to other yokes, too
			joystickLeftX = deadZonifyYoke(joystickLeftX, YOKE_LARGE_DEAD_ZONE_RADIUS);
			joystickRightY = deadZonifyYoke(joystickRightY, YOKE_SMALL_DEAD_ZONE_RADIUS);
			joystickRightX = deadZonifyYoke(joystickRightX, YOKE_SMALL_DEAD_ZONE_RADIUS);

			// read buttons
			triggerLeftDown = isTriggerLeftDown();
			triggerRightDown = isTriggerRightDown();
			buttonADown = isButtonADown();
			buttonBDown = isButtonBDown();
			joystickLeftDown = isJoystickLeftDown();
			joystickRightDown = isJoystickRightDown();

			// respond to user input
			if(joystickLeftDown)
			{
				// send IMU calibration command
				buzzerPlayNotification();
				command[0] = MSG_RECALIBRATE_IMU;			// command
				command[1] = 1;								// length
				command[2] = 255;							// hard-coded broadcast ID
				rfm.sendWithRetry(MQC_ID, command, 3, NUM_RETRIES, RETRY_WAIT_MS);
			}
			else if(joystickRightDown)
			{
				// send ping command
				buzzerPlayNotification();
				command[0] = MSG_FLASH_LED;					// command
				command[1] = 1;								// length
				command[2] = 255;							// hard-coded broadcast ID
				rfm.sendWithRetry(MQC_ID, command, 3, NUM_RETRIES, RETRY_WAIT_MS);
			}
			else
			{
				// decide on hover mode based on trigger inputs
				thrustMode = THRUST_MODE_HOVER;
				if(triggerLeftDown && !triggerRightDown)
				{
					thrustMode = THRUST_MODE_DOWN;
				}
				else if(!triggerLeftDown && triggerRightDown)
				{
					thrustMode = THRUST_MODE_UP;
				}

				// build flight command
				command[0] = MSG_SINGLE_FLIGHT_COMMAND;		// new flight command
				command[1] = 5;								// length of remaining message
				command[2] = 255;							// hard-coded broadcast ID
				command[3] = joystickLeftX;					// yaw
				command[4] = joystickLeftY;					// throttle
				command[5] = joystickRightX;				// strafe
				command[6] = joystickRightY;				// forward

				// send flight command
				rfm.send(MQC_ID, command, 7);
			}
		}
	}

	// good practice to have at end of main()
	while(true);
}

uint8_t deadZonifyThrottle(uint8_t yoke, uint8_t deadZone, uint8_t minThrottle)
{
	if(yoke <= deadZone)
	{
		yoke = 0;
	}
	else
	{
		yoke = minThrottle + ((float)(yoke - deadZone) / (255.0 - deadZone)) * (255.0 - minThrottle);
	}

	return yoke;
}

uint8_t deadZonifyYoke(uint8_t yoke, uint8_t radius)
{
	uint8_t result = 128;

	if(yoke > (YOKE_CENTER + radius))
	{
		result = 128 + ((float)(yoke - (YOKE_CENTER + radius)) / (128.0 - radius)) * 129;
	}
	else if(yoke < (YOKE_CENTER - radius))
	{
		result = (((float)(yoke - (YOKE_CENTER - radius)) / (128.0 - radius))* 128) - 128;
	}

	return result;
}
