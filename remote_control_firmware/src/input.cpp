#include "input.h"

#include <avr/io.h>
#include <util/delay.h>

// - - - pin assignments - - - //

#define TRIGGER_LEFT_PORT PORTB
#define TRIGGER_LEFT_DIR DDRB
#define TRIGGER_LEFT_INPUT PINB
#define TRIGGER_LEFT_PIN 0

#define TRIGGER_RIGHT_PORT PORTB
#define TRIGGER_RIGHT_DIR DDRB
#define TRIGGER_RIGHT_INPUT PINB
#define TRIGGER_RIGHT_PIN 1

#define BUTTON_A_PORT PORTD
#define BUTTON_A_DIR DDRD
#define BUTTON_A_INPUT PIND
#define BUTTON_A_PIN 3

#define BUTTON_B_PORT PORTD
#define BUTTON_B_DIR DDRD
#define BUTTON_B_INPUT PIND
#define BUTTON_B_PIN 4

#define JOYSTICK_LEFT_BUTTON_PORT PORTD
#define JOYSTICK_LEFT_BUTTON_DIR DDRD
#define JOYSTICK_LEFT_BUTTON_INPUT PIND
#define JOYSTICK_LEFT_BUTTON_PIN 0

#define JOYSTICK_RIGHT_BUTTON_PORT PORTD
#define JOYSTICK_RIGHT_BUTTON_DIR DDRD
#define JOYSTICK_RIGHT_BUTTON_INPUT PIND
#define JOYSTICK_RIGHT_BUTTON_PIN 1

#define JOYSTICK_LEFT_X_AXIS_PORT PORTC
#define JOYSTICK_LEFT_X_AXIS_DIR DDRC
#define JOYSTICK_LEFT_X_AXIS_PIN 0

#define JOYSTICK_LEFT_Y_AXIS_PORT PORTC
#define JOYSTICK_LEFT_Y_AXIS_DIR DDRC
#define JOYSTICK_LEFT_Y_AXIS_PIN 1

#define JOYSTICK_RIGHT_X_AXIS_PORT PORTC
#define JOYSTICK_RIGHT_X_AXIS_DIR DDRC
#define JOYSTICK_RIGHT_X_AXIS_PIN 2

#define JOYSTICK_RIGHT_Y_AXIS_PORT PORTC
#define JOYSTICK_RIGHT_Y_AXIS_DIR DDRC
#define JOYSTICK_RIGHT_Y_AXIS_PIN 3

// - - - misc settings - - - //

#define DEBOUNCE_DELAY_MS 20

// - - - prototypes - - - //

void debounce();

// - - - functions - - - //

void inputInit()
{
	// trigger left as input with internal pull-up
	TRIGGER_LEFT_DIR &= ~(1 << TRIGGER_LEFT_PIN);
	TRIGGER_LEFT_PORT |= (1 << TRIGGER_LEFT_PIN);
	
	// trigger right as input with internal pull-up
	TRIGGER_RIGHT_DIR &= ~(1 << TRIGGER_RIGHT_PIN);
	TRIGGER_RIGHT_PORT |= (1 << TRIGGER_RIGHT_PIN);
	
	// button A as input with internal pull-up
	BUTTON_A_DIR &= ~(1 << BUTTON_A_PIN);
	BUTTON_A_PORT |= (1 << BUTTON_A_PIN);
	
	// button B as input with internal pull-up
	BUTTON_B_DIR &= ~(1 << BUTTON_B_PIN);
	BUTTON_B_PORT |= (1 << BUTTON_B_PIN);
	
	// joystick left button as input with internal pull-up
	JOYSTICK_LEFT_BUTTON_DIR &= ~(1 << JOYSTICK_LEFT_BUTTON_PIN);
	JOYSTICK_LEFT_BUTTON_PORT |= (1 << JOYSTICK_LEFT_BUTTON_PIN);
	
	// joystick right button as input with internal pull-up
	JOYSTICK_RIGHT_BUTTON_DIR &= ~(1 << JOYSTICK_RIGHT_BUTTON_PIN);
	JOYSTICK_RIGHT_BUTTON_PORT |= (1 << JOYSTICK_RIGHT_BUTTON_PIN);
	
	// joystick left axes as inputs
	JOYSTICK_LEFT_X_AXIS_DIR &= ~(1 << JOYSTICK_LEFT_X_AXIS_PIN);
	JOYSTICK_LEFT_Y_AXIS_DIR &= ~(1 << JOYSTICK_LEFT_Y_AXIS_PIN);
	
	// joystick right axes as inputs
	JOYSTICK_RIGHT_X_AXIS_DIR &= ~(1 << JOYSTICK_RIGHT_X_AXIS_PIN);
	JOYSTICK_RIGHT_Y_AXIS_DIR &= ~(1 << JOYSTICK_RIGHT_Y_AXIS_PIN);
	
	// enable ADC
	ADCSRA |= (1 << ADEN);												// turn the sucker on
	ADMUX &= ~((1 << REFS0) | (1 << REFS1));							// use external ARef pin voltage as reference
	ADMUX |= (1 << ADLAR);												// we only need 8 bits of precision, so left-adjust all results
}

uint8_t isButtonADown()
{
	if(!(BUTTON_A_INPUT & (1 << BUTTON_A_PIN)))
	{
		debounce();
		return !(BUTTON_A_INPUT & (1 << BUTTON_A_PIN));
	}
	return 0;
}

uint8_t isButtonBDown()
{
	if(!(BUTTON_B_INPUT & (1 << BUTTON_B_PIN)))
	{
		debounce();
		return !(BUTTON_B_INPUT & (1 << BUTTON_B_PIN));
	}
	return 0;
}

uint8_t isTriggerLeftDown()
{
	if(!(TRIGGER_LEFT_INPUT & (1 << TRIGGER_LEFT_PIN)))
	{
		debounce();
		return !(TRIGGER_LEFT_INPUT & (1 << TRIGGER_LEFT_PIN));
	}
	return 0;
}

uint8_t isTriggerRightDown()
{
	if(!(TRIGGER_RIGHT_INPUT & (1 << TRIGGER_RIGHT_PIN)))
	{
		debounce();
		return !(TRIGGER_RIGHT_INPUT & (1 << TRIGGER_RIGHT_PIN));
	}
	return 0;	
}

uint8_t isJoystickLeftDown()
{
	if(!(JOYSTICK_LEFT_BUTTON_INPUT & (1 << JOYSTICK_LEFT_BUTTON_PIN)))
	{
		debounce();
		return !(JOYSTICK_LEFT_BUTTON_INPUT & (1 << JOYSTICK_LEFT_BUTTON_PIN));
	}
	return 0;
}

uint8_t isJoystickRightDown()
{
	if(!(JOYSTICK_RIGHT_BUTTON_INPUT & (1 << JOYSTICK_RIGHT_BUTTON_PIN)))
	{
		debounce();
		return !(JOYSTICK_RIGHT_BUTTON_INPUT & (1 << JOYSTICK_RIGHT_BUTTON_PIN));
	}
	return 0;	
}

void getJoystickLeft(uint8_t *x, uint8_t *y)
{
	// select horizontal channel and start the conversion
	ADMUX &= 0xf0;															// clear channel-select bits
	// [no ADMUX bits to set]
	ADCSRA |= (1 << ADSC);
	while(ADCSRA & (1 << ADSC)) { }											// wait for conversion to complete
	*x = 255 - ADCH;
	ADCSRA |= (1 << ADIF);													// do a hardware-level clear of conversion complete bit
	
	// select vertical channel and start the conversion
	ADMUX &= 0xf0;															// clear channel-select bits
	ADMUX |= (1 << MUX0);
	ADCSRA |= (1 << ADSC);
	while(ADCSRA & (1 << ADSC)) { }											// wait for conversion to complete
	*y = ADCH;
	ADCSRA |= (1 << ADIF);													// do a hardware-level clear of conversion complete bit
}

void getJoystickRight(uint8_t *x, uint8_t *y)
{
	// select horizontal channel and start the conversion
	ADMUX &= 0xf0;															// clear channel-select bits
	ADMUX |= (1 << MUX1);
	ADCSRA |= (1 << ADSC);
	while(ADCSRA & (1 << ADSC)) { }											// wait for conversion to complete
	*x = 255 - ADCH;
	ADCSRA |= (1 << ADIF);													// do a hardware-level clear of conversion complete bit
	
	// select vertical channel and start the conversion
	ADMUX &= 0xf0;															// clear channel-select bits
	ADMUX |= (1 << MUX0) | (1 << MUX1);
	ADCSRA |= (1 << ADSC);
	while(ADCSRA & (1 << ADSC)) { }											// wait for conversion to complete
	*y = ADCH;
	ADCSRA |= (1 << ADIF);													// do a hardware-level clear of conversion complete bit
}

// - - - private functions - - - //

void debounce()
{
	_delay_ms(DEBOUNCE_DELAY_MS);	
}
