#pragma once

#include <avr/io.h>

void buzzerInit();
void buzzerPlayTone(uint16_t tone, uint16_t duration);
void buzzerPlayIntro();
void buzzerPlayNotification();
