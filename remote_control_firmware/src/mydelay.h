#pragma once

#include <avr/io.h>

void delayMicroseconds(uint16_t us);
void delayMilliseconds(uint16_t ms);