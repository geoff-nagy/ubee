#include "buzzer.h"
#include "mydelay.h"

#include <avr/io.h>

#define BUZZER_PORT PORTD
#define BUZZER_DIR DDRD
#define BUZZER_PIN 6

void buzzerInit()
{
	BUZZER_PORT &= ~(1 << BUZZER_PIN);
	BUZZER_DIR |= (1 << BUZZER_PIN);
}

void buzzerPlayTone(uint16_t tone, uint16_t durationUs)
{
	uint8_t i = 0;
	for(i = 0; i < (durationUs / (tone / 100)); i ++)
	{
		BUZZER_PORT |= (1 << BUZZER_PIN);
		delayMicroseconds(tone / 2);
		BUZZER_PORT &= ~(1 << BUZZER_PIN);
		delayMicroseconds(tone / 2);
	}	
}

void buzzerPlayIntro()
{
	uint8_t i;
	
	for(i = 0; i < 3; i ++)
	{
		buzzerPlayTone(1136, 150);
		delayMilliseconds(30);
	}
	
	for(i = 0; i < 3; i ++)
	{
		buzzerPlayTone(1012, 150);
		delayMilliseconds(30);
	}
}

void buzzerPlayNotification()
{
	uint8_t i;
	
	for(i = 0; i < 3; i ++)
	{
		buzzerPlayTone(1012, 100);
		delayMilliseconds(20);
	}
}

