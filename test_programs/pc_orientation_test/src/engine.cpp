/*
NOTES:

- y-angle is rotation about the vector that goes into the screen (roll)
	- clockwise is positive, counter-clockwise is negative
	- varies from -90 to 90
- x-angle is rotation about the horizontal axis (pitch)
	- pitch down is negative, pitch up is positive
	- varies from -90 to 90
- REMEMBER to connect the MPU6050's VIO pin to a 3V3 source,
  or just to VDD if the supply voltage is 3V3 anyways

*/

#include "engine.h"
#include "drawing.h"
#include "arl.h"
#include "serial.h"

#include "glm/glm.hpp"
using namespace glm;

#include <cstdlib>
#include <ctime>
#include <iostream>
using namespace std;

const int Engine::WINDOW_SIZE_X = 600;
const int Engine::WINDOW_SIZE_Y = 600;

Engine::Engine()
{

}

Engine::~Engine()
{
	arl::terminate();
}

void Engine::run()
{
	const int PORT_NUMBER = 4;
	const int BAUD_RATE = 9600;

	initWindow();
	initView();
	initRenderSettings();

	CSerial serial;
	char buffer;
	char data[10];
	bool startRead;

	float pitch;
	float roll;

	// attempt to open the serial port
	if(!serial.Open(PORT_NUMBER, BAUD_RATE))
	{
		cout << "could not open serial port" << endl;
	}
	cout << endl;

	// loop endlessly
	while(!arl::getKey(arl::KEY_ESCAPE))
	{
		arl::prepareLoopStep();

		// wait for header byte
        startRead = false;
        while(!startRead)
        {
			// wait for at least 1 char
			while(serial.ReadDataWaiting() < 1);

			// see if it's a header byte
			serial.ReadData((void*)&buffer, 1);
			startRead = buffer == -1;
        }

        // wait for orientation bytes
        while(serial.ReadDataWaiting() < 8);
        serial.ReadData((void*)data, 8);

        // convert to floating-point data
        pitch = *((float*)(&data[0]));
        roll = *((float*)(&data[4]));

		// print out angle data
        //cout << endl;
        //cout << "angle x: " << angleX << endl;
        //cout << "angle y: " << angleY << endl;

        // rectangle showing incoming orientation---note that this uses immediate mode, which is deprecated
        // and not present in core GL contexts
		glPushMatrix();
			glTranslatef(0.0, 0.0, -3.0);
			glRotatef(-pitch, 1.0, 0.0, 0.0);
			glRotatef(roll, 0.0, 0.0, -1.0);
			glScalef(1.0, 0.1, 1.0);
			Drawing::cube();
		glPopMatrix();

		arl::finishLoopStep();
	}
}

void Engine::initWindow()
{
	arl::init();
	arl::window("MQC Orientation", vec2(WINDOW_SIZE_X, WINDOW_SIZE_Y), arl::WINDOW_WINDOW);
}

void Engine::initView()
{
	const float FOV = 45.0;
    const float ASPECT = (float)WINDOW_SIZE_X / (float)WINDOW_SIZE_Y;
	const float MIN_RANGE = 0.1;
    const float MAX_RANGE = 100.0;

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    gluPerspective(FOV, ASPECT, MIN_RANGE, MAX_RANGE);
    glViewport(0, 0, WINDOW_SIZE_X, WINDOW_SIZE_Y);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glClearColor(0.0, 0.0, 0.0, 1.0);
}

void Engine::initRenderSettings()
{
	glEnable(GL_DEPTH_TEST);
}
