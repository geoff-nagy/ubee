// orientation_test
// Geoff Nagy
// This is a super-quick test program to visualize the orientation of the MQC during development. Note that it
// uses deprecated OpenGL features (since this is very easy to implement) to do this.

// note: this tool was used during development of the MQC only; it's not used beyond this point at all

#include "engine.h"

int main(int args, char *argv[])
{
	Engine *engine = new Engine();
	engine -> run();

    delete engine;

	return 0;
}
