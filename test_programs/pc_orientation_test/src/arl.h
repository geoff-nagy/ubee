/*
Aurora Lite

Lightweight version of Aurora with sprite, home-brew vectors, and math removed.

Features:
	- window creation
	- time delta management
	- keypress reading
	- image loading
*/

#pragma once

#include <glm/glm.hpp>
#include <gl/glew.h>
#include <string>
#include <map>

class GLFWwindow;

class arl
{
private:
	static const float FRAME_TIME;				// ideal time, in seconds, to draw one frame
	static const float MAX_FRAME_TIME;			// max time delta that we allow

	static GLFWwindow *gameWindow;

	static float oldTime;						// time stamp of last frame
	static float newTime;						// time stamp of new frame

	static void printOpenGLInfo();

public:
	// arguments to getKey (we can add more if we want - use 'A', etc. for letters)
	static const int KEY_SPACE;
	static const int KEY_ESCAPE;
	static const int KEY_ARROW_UP;
	static const int KEY_ARROW_DOWN;
	static const int KEY_ARROW_LEFT;
	static const int KEY_ARROW_RIGHT;
	static const int KEY_LEFT_SHIFT;
	static const int KEY_RIGHT_SHIFT;
	static const int KEY_LEFT_CONTROL;
	static const int KEY_RIGHT_CONTROL;
	static const int KEY_LEFT_ALT;
	static const int KEY_RIGHT_ALT;
	static const int KEY_TAB;
	static const int KEY_ENTER;
	static const int KEY_BACKSPACE;

	// arguments to getMouseButton
	static const int MOUSE_BUTTON_LEFT;
	static const int MOUSE_BUTTON_RIGHT;

	// arguments for creating a window
	static const int WINDOW_WINDOW;
	static const int WINDOW_FULLSCREEN;

	// initialization
	static void init();
	static void terminate();

	// window handling
	static void window(std::string, glm::vec2, int);
	static void resizeWindow(glm::vec2);
	static void enableCursor(bool);

	// time delta handling
	static float getDeltaTime();

	// keypress handling
	static bool getKey(int);

	// mouse handling
	static glm::vec2 getMousePos();
	static bool getMouseButtonDown(int);

	// joystick handling
	static glm::vec2 getJoystickPos();
	static float getJoystickAxis(int axis);
	static bool getJoystickButtonDown(int);

	// graphics updating handling
	static void prepareLoopStep();
	static void finishLoopStep();
};
