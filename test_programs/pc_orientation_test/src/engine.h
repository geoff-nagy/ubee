#pragma once

class Engine
{
private:
	static const int WINDOW_SIZE_X;
	static const int WINDOW_SIZE_Y;

	void initWindow();
	void initView();
	void initRenderSettings();

public:
	Engine();
	~Engine();

	void run();
};
