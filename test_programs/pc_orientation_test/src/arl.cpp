#include "arl.h"

#include "GL/glew.h"

#include "GLFW/glfw3.h"

#include "glm/glm.hpp"
using namespace glm;

#include <map>
#include <iostream>
using namespace std;

const float arl::FRAME_TIME = 1.0 / 30.0;
const float arl::MAX_FRAME_TIME = FRAME_TIME * 4.0;

// arguments to getKey (we can add more if we want - use 'A', etc. for letters
const int arl::KEY_SPACE = GLFW_KEY_SPACE;
const int arl::KEY_ESCAPE = GLFW_KEY_ESCAPE;
const int arl::KEY_ARROW_UP = GLFW_KEY_UP;
const int arl::KEY_ARROW_DOWN = GLFW_KEY_DOWN;
const int arl::KEY_ARROW_LEFT = GLFW_KEY_LEFT;
const int arl::KEY_ARROW_RIGHT = GLFW_KEY_RIGHT;
const int arl::KEY_LEFT_SHIFT = GLFW_KEY_LEFT_SHIFT;
const int arl::KEY_RIGHT_SHIFT = GLFW_KEY_RIGHT_SHIFT;
const int arl::KEY_LEFT_CONTROL = GLFW_KEY_LEFT_CONTROL;
const int arl::KEY_RIGHT_CONTROL = GLFW_KEY_RIGHT_CONTROL;
const int arl::KEY_LEFT_ALT = GLFW_KEY_LEFT_ALT;
const int arl::KEY_RIGHT_ALT = GLFW_KEY_RIGHT_ALT;
const int arl::KEY_TAB = GLFW_KEY_TAB;
const int arl::KEY_ENTER = GLFW_KEY_ENTER;
const int arl::KEY_BACKSPACE = GLFW_KEY_BACKSPACE;

// arguments to getMouseButton
const int arl::MOUSE_BUTTON_LEFT = GLFW_MOUSE_BUTTON_LEFT;
const int arl::MOUSE_BUTTON_RIGHT = GLFW_MOUSE_BUTTON_RIGHT;

// arguments for creating a window
const int arl::WINDOW_WINDOW = 0;
const int arl::WINDOW_FULLSCREEN = 1;

GLFWwindow *arl::gameWindow = NULL;

float arl::oldTime;				// time stamp of last frame
float arl::newTime;				// time stamp of new frame

void arl::init()
{
	cout << "arl::init()" << endl << endl;
	glfwInit();
}

void arl::terminate()
{
	cout << "arl::terminate()" << endl << endl;
	glfwDestroyWindow(gameWindow);
	glfwTerminate();
}

void arl::window(string title, vec2 size, int windowMode)
{
	glfwSwapInterval(1);
	glfwWindowHint(GLFW_REFRESH_RATE, 60);
    gameWindow = glfwCreateWindow(size.x,
								  size.y,
								  title.c_str(),
								  (windowMode == WINDOW_FULLSCREEN ? glfwGetPrimaryMonitor() : NULL),
								  NULL);
	glfwMakeContextCurrent(gameWindow);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

	glOrtho(0, size.x, size.y, 0, -5.0, 5.0);
	glViewport(0, 0, size.x, size.y);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

	glClearColor(0.0, 0.2, 0.2, 1.0);
	printOpenGLInfo();

	oldTime = glfwGetTime();
	newTime = oldTime + FRAME_TIME;

	glewInit();
}

void arl::resizeWindow(vec2 size)
{
	glfwSetWindowSize(gameWindow, size.x, size.y);
	glViewport(0, 0, size.x, size.y);
}

void arl::enableCursor(bool enable)
{
	glfwSetInputMode(gameWindow, GLFW_CURSOR, (enable ? GLFW_CURSOR_NORMAL : GLFW_CURSOR_DISABLED));
}

float arl::getDeltaTime()
{
	const float EPSILON = 0.00001;

	float dt;

	// gather time values
	oldTime = newTime;
	newTime = glfwGetTime();

	// ensure we don't have any long pauses or tiny time quantums
	dt = (newTime - oldTime);// * 2.0;
	if(dt < EPSILON)
		dt = EPSILON;
	if(dt > MAX_FRAME_TIME)
		dt = MAX_FRAME_TIME;

	return dt;
}

bool arl::getKey(int key)
{
	return glfwGetKey(gameWindow, key) == GLFW_PRESS;
}

vec2 arl::getMousePos()
{
	double x, y;
	glfwGetCursorPos(gameWindow, &x, &y);
	return vec2(x, y);
}

bool arl::getMouseButtonDown(int button)
{
	return glfwGetMouseButton(gameWindow, button) == GLFW_PRESS;
}

glm::vec2 arl::getJoystickPos()
{
	vec2 result(0.0, 0.0);
	int numAxes;

	if(glfwJoystickPresent(GLFW_JOYSTICK_1))
	{
		const float *axes = glfwGetJoystickAxes(GLFW_JOYSTICK_1, &numAxes);
		if(numAxes >= 2)
		{
			result = vec2(axes[0], axes[1]);
		}
	}
	return result;
}

float arl::getJoystickAxis(int axis)
{
	float result = 0.0;
	int numAxes;

	if(glfwJoystickPresent(GLFW_JOYSTICK_1))
	{
		const float *axes = glfwGetJoystickAxes(GLFW_JOYSTICK_1, &numAxes);
		if(numAxes >= (axis - 1))
		{
			result = axes[axis];
		}
	}

	return result;
}

bool arl::getJoystickButtonDown(int button)
{
	int numButtons;
	bool result = false;

	if(glfwJoystickPresent(GLFW_JOYSTICK_1))
	{
		const unsigned char *buttons = glfwGetJoystickButtons(GLFW_JOYSTICK_1, &numButtons);
		if(numButtons >= button)
		{
			result = buttons[button] == GLFW_PRESS;
		}
	}
	return result;
}

void arl::prepareLoopStep()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void arl::finishLoopStep()
{
	glfwSwapBuffers(gameWindow);
	glfwPollEvents();
}

/* - - - private methods - - - */

void arl::printOpenGLInfo()
{
    cout << "------- GL Info -------" << endl;
    cout << "GL version is " << (char*)glGetString(GL_VERSION) << endl;
    cout << "GL vendor is " << (char*)glGetString(GL_VENDOR) << endl;
    cout << "GL renderer is " << (char*)glGetString(GL_RENDERER) << endl;
	cout << "GLSL version is " << (char*)glGetString(GL_SHADING_LANGUAGE_VERSION) << endl;
	cout << "-----------------------" << endl << endl;
}
