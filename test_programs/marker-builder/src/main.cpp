#include "sl/sl.h"

#include "glm/glm.hpp"
using namespace glm;

#include <string>
#include <sstream>
#include <iomanip>
using namespace std;

// - - - globals - - - //

const vec2 WINDOW_SIZE(768.0f, 768.0f);
const vec2 WINDOW_CENTER(WINDOW_SIZE / 2.0f);

const float MM_SIZE = 140.0f;
const float PIXELS_PER_MM = WINDOW_SIZE.x / MM_SIZE;

vec2 marker1Pos;
vec2 marker2Pos;
vec2 marker3Pos;
vec2 centerOfMass;

// - - - prototypes - - - //

vec2 windowToMM(const vec2 &window);
vec2 mmToWindow(const vec2 &mm);
void recomputeMarker3();
void recomputeCenterOfMass();

void getUserInput();
void renderGrid();
void renderWafflePlate();
void renderTrianglePlate();
void renderMarkerHoles();
void renderCenterOfMass();
void renderMarkerStats();

// - - - implementations - - - //

int main(int args, char *argv[])
{
	//float dt;

	// this initializes the sigil library and creates a window of the desired size
	slWindow(WINDOW_SIZE.x, WINDOW_SIZE.y, "uBee Marker Plate Builder", false);

	// the colour of the background is specified using RGB values in the range [0.0, 1.0]
	slSetBackColor(0.0, 0.0, 0.0);		// black

	// load up font
	slSetFont(slLoadFont("ttf/white_rabbit.ttf"), 12);

	// initialize some default values
	marker1Pos = vec2(-5.0f, 25.0f);
	marker2Pos = vec2(20.0f, -3.0f);
	marker3Pos = vec2(-20.0f, -24.0f);
	//recomputeMarker3();
	recomputeCenterOfMass();

	// the window will remain open (and the program will remain in this loop) until the user presses the 'X' to close
	while(!slShouldClose() && !slGetKey(SL_KEY_ESCAPE))
	{
		// get time step
		//dt = slGetDeltaTime();

		// reposition markers
		getUserInput();

		// render background grid
		renderGrid();

		// render basic reference shapes
		renderWafflePlate();
		renderTrianglePlate();

		// render objects we want to place
		renderMarkerHoles();
		renderCenterOfMass();

		// render stats about our markers
		renderMarkerStats();

		// display anything that SIGIL needs to as a result of drawing commands
		slRender();
	}

	// shut down our sigil window
	slClose();

	// program ends
	return 0;
}

vec2 windowToMM(const vec2 &window)
{
	return vec2(window.x / PIXELS_PER_MM, window.y / PIXELS_PER_MM);
}

vec2 mmToWindow(const vec2 &mm)
{
	return vec2(mm.x * PIXELS_PER_MM, (mm.y * PIXELS_PER_MM));
}

void recomputeMarker3()
{
	marker3Pos = -(marker1Pos + marker2Pos);
}

void recomputeCenterOfMass()
{
	centerOfMass = (marker1Pos + marker2Pos + marker3Pos) / 3.0f;
}

void getUserInput()
{
	if(slGetMouseButton(SL_MOUSE_BUTTON_1))
	{
		if(slGetKey('1'))
		{
			// convert from mouse/window coordinates to coordinates in nearest integer millimeters
			marker1Pos = round(windowToMM(vec2(slGetMouseX(), slGetMouseY()) - WINDOW_CENTER));
			recomputeMarker3();
			recomputeCenterOfMass();
		}
		else if(slGetKey('2'))
		{
			// convert from mouse/window coordinates to coordinates in nearest integer millimeters
			marker2Pos = round(windowToMM(vec2(slGetMouseX(), slGetMouseY()) - WINDOW_CENTER));
			recomputeMarker3();
			recomputeCenterOfMass();
		}
		else if(slGetKey('3'))
		{
			// convert from mouse/window coordinates to coordinates in nearest integer millimeters
			marker3Pos = round(windowToMM(vec2(slGetMouseX(), slGetMouseY()) - WINDOW_CENTER));
			recomputeCenterOfMass();
		}
	}
}

void renderGrid()
{
	int i;

	// 1 mm increments

	slSetForeColor(1.0f, 1.0f, 1.0f, 0.075f);
	for(i = 0; i < WINDOW_SIZE.x / PIXELS_PER_MM; ++ i)
	{
		slLine(i * PIXELS_PER_MM, 0.0f, i * PIXELS_PER_MM, WINDOW_SIZE.y);
	}

	slSetForeColor(1.0f, 1.0f, 1.0f, 0.075f);
	for(i = 0; i < WINDOW_SIZE.x / PIXELS_PER_MM; ++ i)
	{
		slLine(0.0f, i * PIXELS_PER_MM, WINDOW_SIZE.x, i * PIXELS_PER_MM);
	}

	// 5 mm increments

	slSetForeColor(1.0f, 1.0f, 1.0f, 0.1f);
	for(i = 0; i < WINDOW_SIZE.x / PIXELS_PER_MM; i += 5)
	{
		slLine(i * PIXELS_PER_MM, 0.0f, i * PIXELS_PER_MM, WINDOW_SIZE.y);
	}

	slSetForeColor(1.0f, 1.0f, 1.0f, 0.1f);
	for(i = 0; i < WINDOW_SIZE.x / PIXELS_PER_MM; i += 5)
	{
		slLine(0.0f, i * PIXELS_PER_MM, WINDOW_SIZE.x, i * PIXELS_PER_MM);
	}
}

void renderWafflePlate()
{
	// [todo]
}

void renderTrianglePlate()
{
	const vec2 MAIN_SIZE(mmToWindow(vec2(48.0f, 55.0f)));

	const vec2 LEFT_POS(WINDOW_CENTER + mmToWindow(vec2(((-15.0f + 1.5f) / 2.0f) - 1.5f, 0.0f)));
	const vec2 RIGHT_POS(WINDOW_CENTER + mmToWindow(vec2(((15.0f - 1.5f) / 2.0f) + 1.5f, 0.0f)));
	const vec2 SQUARE_HOLE_SIZE(mmToWindow(vec2(15.0f - 1.5f, 44.0f - 1.5f)));

	const int NUM_HOLE_VERTICES = 16;
	const float ROUND_HOLE_RADIUS = 3.0f * PIXELS_PER_MM;
	const vec2 HOLE_1_POS(WINDOW_CENTER + mmToWindow(vec2(-20.25f, 8.5f)));
	const vec2 HOLE_2_POS(WINDOW_CENTER + mmToWindow(vec2(20.25f, 8.5f)));
	const vec2 HOLE_3_POS(WINDOW_CENTER + mmToWindow(vec2(-20.25f, -8.5f)));
	const vec2 HOLE_4_POS(WINDOW_CENTER + mmToWindow(vec2(20.25f, -8.5f)));

	slSetForeColor(1.0f, 1.0f, 1.0f, 1.0f);

	slRectangleOutline(WINDOW_CENTER.x, WINDOW_CENTER.y, MAIN_SIZE.x, MAIN_SIZE.y);
	slRectangleOutline(LEFT_POS.x, LEFT_POS.y, SQUARE_HOLE_SIZE.x, SQUARE_HOLE_SIZE.y);
	slRectangleOutline(RIGHT_POS.x, RIGHT_POS.y, SQUARE_HOLE_SIZE.x, SQUARE_HOLE_SIZE.y);

	slCircleOutline(HOLE_1_POS.x, HOLE_1_POS.y, ROUND_HOLE_RADIUS, NUM_HOLE_VERTICES);
	slCircleOutline(HOLE_2_POS.x, HOLE_2_POS.y, ROUND_HOLE_RADIUS, NUM_HOLE_VERTICES);
	slCircleOutline(HOLE_3_POS.x, HOLE_3_POS.y, ROUND_HOLE_RADIUS, NUM_HOLE_VERTICES);
	slCircleOutline(HOLE_4_POS.x, HOLE_4_POS.y, ROUND_HOLE_RADIUS, NUM_HOLE_VERTICES);
}

void renderMarkerHoles()
{
	const float HOLE_RADIUS = 1.65f * PIXELS_PER_MM;
	const int NUM_HOLE_VERTICES = 16;

	const vec2 TEXT_TWEAK(-3.0f, -3.0f);

	vec2 m1, m2, m3;

	m1 = WINDOW_CENTER + mmToWindow(marker1Pos);
	m2 = WINDOW_CENTER + mmToWindow(marker2Pos);
	m3 = WINDOW_CENTER + mmToWindow(marker3Pos);

	slSetForeColor(0.0f, 1.0f, 0.0f, 1.0f);

	slCircleOutline(m1.x, m1.y, HOLE_RADIUS, NUM_HOLE_VERTICES);
	slCircleOutline(m2.x, m2.y, HOLE_RADIUS, NUM_HOLE_VERTICES);

	slText(m1.x + TEXT_TWEAK.x, m1.y + TEXT_TWEAK.y, "1");
	slText(m2.x + TEXT_TWEAK.x, m2.y + TEXT_TWEAK.y, "2");

	slSetForeColor(0.5f, 0.5f, 1.0f, 1.0f);

	slCircleOutline(m3.x, m3.y, HOLE_RADIUS, NUM_HOLE_VERTICES);

	slText(m3.x + TEXT_TWEAK.x, m3.y + TEXT_TWEAK.y, "3");
}

void renderCenterOfMass()
{
	const float CENTER_RADIUS = 0.5f * PIXELS_PER_MM;
	const int NUM_CENTER_VERTICES = 16;

	vec2 c = WINDOW_CENTER + mmToWindow(centerOfMass);

	slSetForeColor(1.0f, 0.2f, 1.0f, 1.0f);

	slCircleOutline(c.x, c.y, CENTER_RADIUS, NUM_CENTER_VERTICES);
}

void renderMarkerStats()
{
	const float TEXT_MARGIN = 25.0f;
	const float LINE_SPACING = 20.0f;

	stringstream ss;
	float dist12;
	float dist23;
	float dist31;
	float minDist;

	slSetForeColor(0.5f, 1.0f, 0.5f, 1.0f);

	ss.str(""); ss << "Marker 1 pos   : " << fixed << setprecision(2) << marker1Pos.x << ", " << marker1Pos.y; slText(TEXT_MARGIN, WINDOW_SIZE.y - TEXT_MARGIN - (LINE_SPACING * 0), ss.str().c_str());
	ss.str(""); ss << "Marker 2 pos   : " << fixed << setprecision(2) << marker2Pos.x << ", " << marker2Pos.y; slText(TEXT_MARGIN, WINDOW_SIZE.y - TEXT_MARGIN - (LINE_SPACING * 1), ss.str().c_str());

	slSetForeColor(0.5f, 0.5f, 1.0f, 1.0f);

	ss.str(""); ss << "Marker 3 pos   : " << fixed << setprecision(2) << marker3Pos.x << ", " << marker3Pos.y; slText(TEXT_MARGIN, WINDOW_SIZE.y - TEXT_MARGIN - (LINE_SPACING * 2), ss.str().c_str());

	slSetForeColor(1.0f, 0.5f, 1.0f, 1.0f);

	ss.str(""); ss << "Center of mass : " << fixed << setprecision(2) << centerOfMass.x << ", " << centerOfMass.y; slText(TEXT_MARGIN, WINDOW_SIZE.y - TEXT_MARGIN - (LINE_SPACING * 3), ss.str().c_str());

	slSetForeColor(1.0f, 0.5f, 0.5f, 1.0f);

	dist12 = length(marker1Pos - marker2Pos);
	dist23 = length(marker2Pos - marker3Pos);
	dist31 = length(marker3Pos - marker1Pos);
	minDist = glm::min(fabs(dist12 - dist23), glm::min(fabs(dist23 - dist31), fabs(dist31 - dist12)));

	ss.str(""); ss << "Marker 1 -> 2 dist : " << fixed << setprecision(2) << dist12; slText(TEXT_MARGIN, WINDOW_SIZE.y - TEXT_MARGIN - (LINE_SPACING * 5), ss.str().c_str());
	ss.str(""); ss << "Marker 2 -> 3 dist : " << fixed << setprecision(2) << dist23; slText(TEXT_MARGIN, WINDOW_SIZE.y - TEXT_MARGIN - (LINE_SPACING * 6), ss.str().c_str());
	ss.str(""); ss << "Marker 3 -> 1 dist : " << fixed << setprecision(2) << dist31; slText(TEXT_MARGIN, WINDOW_SIZE.y - TEXT_MARGIN - (LINE_SPACING * 7), ss.str().c_str());
	ss.str(""); ss << "Minimum dist diff  : " << fixed << setprecision(2) << minDist; slText(TEXT_MARGIN, WINDOW_SIZE.y - TEXT_MARGIN - (LINE_SPACING * 8), ss.str().c_str());

	vec2 v;
	v = normalize((marker1Pos - centerOfMass) + (marker2Pos - centerOfMass));

	slSetForeColor(1.0f, 1.0f, 1.0f, 1.0f);
	ss.str(""); ss << "forward: " << fixed << setprecision(4) << v.x << ", " << v.y; slText(TEXT_MARGIN, WINDOW_SIZE.y - TEXT_MARGIN - (LINE_SPACING * 10), ss.str().c_str());
}
