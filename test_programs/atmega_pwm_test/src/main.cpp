#include <avr/io.h>
#include <util/delay.h>

// button port and pin
#define BUTTON_PORT PORTD
#define BUTTON_DIR DDRD
#define BUTTON_INPUT PIND
#define BUTTON_PIN 0x07

// PWM'd output port and pin
#define PWM_PORT PORTD
#define PWM_DIR DDRD
#define PWM_PIN 0x06

// useful constants
#define DEBOUNCE_DELAY 20

// - - - initialization - - - //

void initButton()
{
	// configure button pin as input and set pull-up
	BUTTON_DIR &= ~(1 << BUTTON_PIN);
	BUTTON_PORT |= (1 << BUTTON_PIN);
}

void initPWM()
{
	// set output compare value
	OCR0A = 0xff;
	
	// clear output compare pin value and set as output
	PWM_PORT &= ~(1 << PWM_PIN);
	PWM_DIR |= (1 << PWM_PIN);
	
	// set to fast PWM mode with TOP defined as 0xFF
	TCCR0A |= (1 << WGM00);
	TCCR0A |= (1 << WGM01);
	
	// disconnect OC0A and turn off pin output
	TCCR0A &= ~(1 << COM0A1);
	TCCR0A &= ~(1 << COM0A0);
	PWM_PORT &= ~(1 << PWM_PIN);
	
	// set clock prescaler---no prescaler
	//TCCR0B |= (1 << CS02);
	//TCCR0B |= (1 << CS01);
	TCCR0B |= (1 << CS00);
}

// - - - utility functions - - - //

uint8_t isButtonDown()
{
	return !(BUTTON_INPUT & (1 << BUTTON_PIN));
}

// - - - main function - - - //

int main(void)
{
	// setup
	initButton();
	initPWM();
	
	// main loop
	while(true)
	{
		// see if button is held down
		if(isButtonDown())
		{
			// debounce and see if it's still held
			_delay_ms(DEBOUNCE_DELAY);	
			if(isButtonDown())
			{
				// turn off OC0A connect it to timer 0; set fast PWM output compare mode: clear OC0A on compare match, set OC0A at BOTTOM
				PWM_PORT &= ~(1 << PWM_PIN);
				TCCR0A |= (1 << COM0A1);
				TCCR0A &= ~(1 << COM0A0);
				
				// wait until button is released
				while(isButtonDown());
				
				// disconnect OC0A and turn off pin output
				TCCR0A &= ~(1 << COM0A1);
				TCCR0A &= ~(1 << COM0A0);
				PWM_PORT &= ~(1 << PWM_PIN);
			}
		}
	}
	
	// good practice to avoid executing random code if the above loop ever terminates
	while(true);
}
