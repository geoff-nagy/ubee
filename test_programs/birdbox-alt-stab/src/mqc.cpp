#include "mqc.h"

#include "SL/sl.h"

#include "glm/glm.hpp"
#include "glm/gtc/random.hpp"
using namespace glm;

MQC::MQC(const vec2 &pos)
{
	this -> pos = pos;
	texture = slLoadTexture("png/mqc.png");

	throttlePID = PIDController(0.025f, 0.0f, 1.8f);
	throttle = 0.0f;
	throttleOutput = 0.0f;

	desiredHeight = 0.0f;
}

MQC::~MQC() { }

void MQC::update(float dt)
{
	updatePhysics(dt);
	updateOutput(dt);
}

void MQC::updatePhysics(float dt)
{
	const float GRAVITY = 9.81f;

	const float MASS_GRAMS = 26.0f;
	const float MAX_THROTTLE_FORCE = 150.0f * 4.0f;				// each rotor produces ~14g of thrust

	const float MAX_VEL = 500.0f;

	vec2 oldVel = vel;
	vec2 thrust = vec2(0.0f, (throttleOutput * MAX_THROTTLE_FORCE) / MASS_GRAMS);

	// apply acceleration and velocity
	accel = vec2(0.0f, -GRAVITY);
	vel += accel + thrust;

	// air drag limits our upwards velocity
	if(vel.y > MAX_VEL)
	{
		vel.y = MAX_VEL;
	}

	// apply velocity to position
	pos += (vel * dt);

	// prevent from going out of bounds
	if(pos.y < 0.0f)
	{
		pos.y = 0.0f;
		accel = vec2(0.0f);
		vel.y = 0.0f;
	}
	if(pos.y > 480.0f)
	{
		pos.y = 480.0f;
		vel.y = 0.0f;
	}

	measuredHeight = pos.y + gaussRand(0.0f, (pos.y * pos.y) * 0.00001f);
}

void MQC::updateOutput(float dt)
{
	const float GRAVITY = 9.81f;

	const float THROTTLE_ACCEL = 2.0f;

	throttle = 0.0f + throttlePID.update(desiredHeight - measuredHeight);

	if(throttle > 1.0f) throttle = 1.0f;
	else if(throttle < 0.0f) throttle = 0.0f;

	if(throttleOutput < throttle)
	{
		throttleOutput += THROTTLE_ACCEL * dt;
		if(throttleOutput > throttle)
		{
			throttleOutput = throttle;
		}
	}
	else if(throttleOutput > throttle)
	{
		throttleOutput -= THROTTLE_ACCEL * dt;
		if(throttleOutput < throttle)
		{
			throttleOutput = throttle;
		}
	}
}

void MQC::render()
{
	const vec2 MQC_SIZE(128.0f, 128.0f);

	slSprite(texture, pos.x, pos.y + MQC_SIZE.y / 2.0f, MQC_SIZE.x, MQC_SIZE.y);
}

float MQC::getThrottle()
{
	return throttle;
}

float MQC::getHeight()
{
	return pos.y;
}

float MQC::getMeasuredHeight()
{
	return measuredHeight;
}

void MQC::setDesiredHeight(float desiredHeight)
{
	this -> desiredHeight = desiredHeight;
}
