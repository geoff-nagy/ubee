#pragma once

#include "pidcontroller.h"
#include "circularbuffer.h"

#include "glm/glm.hpp"

class MQC
{
public:
	MQC(const glm::vec2 &pos);
	~MQC();

	void update(float dt);
	void render();

	float getThrottle();
	float getHeight();
	float getMeasuredHeight();

	void setDesiredHeight(float desiredHeight);

private:
	void updatePhysics(float dt);
	void updateOutput(float dt);

	glm::vec2 pos;
	glm::vec2 vel;
	glm::vec2 accel;

	PIDController throttlePID;
	float throttle;
	float throttleOutput;

	float desiredHeight;
	float measuredHeight;

	int texture;
};
