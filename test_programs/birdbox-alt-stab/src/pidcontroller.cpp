#include "pidcontroller.h"

PIDController::PIDController()
	: pGain(0.0),
	  iGain(0.0),
	  dGain(0.0),
	  oldError(0.0),
	  lastResult(0.0)
{
	// does anybody have anything to say...?
}

PIDController::PIDController(float n_pGain, float n_iGain, float n_dGain)
	: pGain(n_pGain),
	  iGain(n_iGain),
	  dGain(n_dGain),
	  oldError(0.0),
	  lastResult(0.0)
{
	// ...anything else...?
}

float PIDController::update(float error)
{
	// reset our result tracking
	lastResult = 0.0;

	// apply proportional term
	lastResult += error * pGain;

	// apply integral term
	lastResult += (oldError + error) * iGain;

	// apply differential term
	lastResult += (error - oldError) * dGain;

	// save old error for later
	oldError = error;

	// we're done!
	return lastResult;
}

float PIDController::getLastResult()
{
	return lastResult;
}

void PIDController::reset()
{
	oldError = 0.0;
	lastResult = 0.0;
}
