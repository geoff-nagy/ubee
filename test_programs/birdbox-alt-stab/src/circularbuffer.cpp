#include "circularbuffer.h"

#include <cstdlib>

CircularBuffer::CircularBuffer()
{
	buffer = NULL;
	bufferSize = 0;

	bufferIndex = 0;
	numEntries = 0;

	filteredOutput = 0.0;
	bufferSum = 0.0;
}

CircularBuffer::CircularBuffer(int bufferSize)
{
	this -> bufferSize = bufferSize;
	buffer = (float*)malloc(sizeof(float) * bufferSize);

	bufferIndex = 0;
	numEntries = 0;

	filteredOutput = 0.0;
	bufferSum = 0.0;
}

CircularBuffer::~CircularBuffer()
{

}

void CircularBuffer::addEntry(float value)
{
	int i;

	// make sure we actually have a buffer!
	if(bufferSize)
	{
		// track how many entries are currently in the buffer
		numEntries ++;
		if(numEntries >= bufferSize)
		{
			numEntries = bufferSize;
		}

		// insert the value into the buffer
		buffer[bufferIndex++] = value;
		if(bufferIndex >= bufferSize)
			bufferIndex = 0;

		// sum the buffer values we have for an average
		bufferSum = 0.0;
		for(i = 0; i < numEntries; ++ i)
		{
			bufferSum += buffer[i];
		}

		// compute the new mean
		filteredOutput = bufferSum / (float)numEntries;
	}
}

float CircularBuffer::getFilteredOutput()
{
	return filteredOutput;
}

float CircularBuffer::getBufferSum()
{
	return bufferSum;
}
