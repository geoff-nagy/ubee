#pragma once

class PIDController
{
private:
	float pGain;				// gain on proportional term
	float iGain;				// gain on integral term
	float dGain;				// gain on differential term

	float oldError;				// previous error, for I and D terms

	float lastResult;			// last computed controller output

public:
	PIDController();
	PIDController(float pGain, float iGain, float dGain);

	// pass in error and get a result!
	float update(float error);

	// ...or, use the previous result!
	float getLastResult();

	// reset the controller
	void reset();
};
