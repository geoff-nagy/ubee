#include "ledflash.h"
#include "rfm69/RFM69.h"

#include <avr/io.h>
#include <util/delay.h>

// common across devices
#define FREQUENCY RF69_915MHZ
#define NETWORK_ID 45

// IDs are device-specific
#define SERVER_ID 43
#define CLIENT_ID 44

// defines how TX retries are handled
#define NUM_RETRIES 5
#define RETRY_WAIT_MS 20

// uncomment one based on whether we're compiling for the server or the client
//#define MODE_SERVER
#define MODE_CLIENT

// - - - initialization - - - //

// - - - main function - - - //

int main(void)
{
	RFM69 rfm;
	#ifdef MODE_SERVER
		uint8_t flashes = 1;
	#endif
	
	// initialize Arduino library
	init();
	
	// initialize LED output settings
	ledInit();
	
	// initialize the RF transceiver
	#ifdef MODE_SERVER
		rfm.initialize(FREQUENCY, SERVER_ID, NETWORK_ID);
		rfm.setHighPower(true);
		rfm.setPowerLevel(31);
		
	#else
		rfm.initialize(FREQUENCY, CLIENT_ID, NETWORK_ID);
	#endif
	
	// successful initialization
	ledFlash(5);
	
	// infinite loop
	while(true)
	{
		// server mode means that we transmit to the client
		#ifdef MODE_SERVER
		
			// send the number of flashes we want
			rfm.sendWithRetry(CLIENT_ID, (const void*)&flashes, 1, NUM_RETRIES, RETRY_WAIT_MS);
			
			// and make our LED flash, too
			ledFlash(flashes);
			
			// more flashes next time
			flashes ++;
			if(flashes > 3)
			{
				flashes = 1;
			}
			
			// short delay
			_delay_ms(500);
		
		#endif
		
		// client mode means that we receive from the server
		#ifdef MODE_CLIENT
		
			// see if we've received anything
			if(rfm.receiveDone())
			{
				// send an ACK immediately
				if(rfm.ACKRequested())
				{
					rfm.sendACK();	
				}
				
				// a single byte means that we should flash the LED a specific number of times
				//if(rfm.DATALEN == 1)
				{
					ledFlash(rfm.DATA[0]);
				}
			}
		
		#endif
	}
}
