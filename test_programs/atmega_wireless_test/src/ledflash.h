#pragma once

#include <avr/io.h>

void ledInit();
void ledFlash(uint8_t times);