#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#define THROTTLE_DEAD_ZONE 5										// no throttle response below this yoke strength
#define THROTTLE_MIN_VAL 50											// minimum throttle response immediately above THROTTLE_DEAD_ZONE

#define YOKE_CENTER 128												// values considered to be the yoke center
#define YOKE_SMALL_DEAD_ZONE_RADIUS 2								// values within this much of the yoke center (128) are considered neutral (128)
#define YOKE_LARGE_DEAD_ZONE_RADIUS 10

uint8_t deadZonifyThrottle(uint8_t yoke, uint8_t deadZone, uint8_t minThrottle)
{
	if(yoke <= deadZone)
	{
		yoke = 0;
	}
	else
	{
		yoke = minThrottle + ((float)(yoke - deadZone) / (255.0 - deadZone)) * (255 - minThrottle);
	}

	return yoke;
}

uint8_t deadZonifyYoke(uint8_t yoke, uint8_t radius)
{
	uint8_t result = 128;

	if(yoke > (YOKE_CENTER + radius))
	{
		result = 128 + ((float)(yoke - (YOKE_CENTER + radius)) / (128.0 - radius)) * 129;
	}
	else if(yoke < (YOKE_CENTER - radius))
	{
		result = (((float)(yoke - (YOKE_CENTER - radius)) / (128.0 - radius))* 128) - 128;
	}

	return result;
}

int main(int args, char *argv[])
{
	int i;

	for(i = 0; i <= 255; ++ i)
	{
		//printf("%d:   %d\n", i, deadZonifyYoke(i, YOKE_LARGE_DEAD_ZONE_RADIUS));
		printf("%d:   %d\n", i, deadZonifyThrottle(i, THROTTLE_DEAD_ZONE, THROTTLE_MIN_VAL));
	}

	return 0;
}
