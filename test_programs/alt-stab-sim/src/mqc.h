#pragma once

#include "pidcontroller.h"
#include "circularbuffer.h"

#include "glm/glm.hpp"

class MQC
{
public:
	MQC(const glm::vec2 &pos);
	~MQC();

	void update(float dt);
	void render();

	float getMeasuredAccelY();
	float getMeasuredVelY();
	float getThrottle();

	void setDesiredVel(float desiredVel);

private:
	void updatePhysics(float dt);
	void updateOutput(float dt);

	glm::vec2 pos;
	glm::vec2 vel;
	glm::vec2 accel;

	glm::vec2 measuredAccel;
	float measuredVel;

	PIDController throttlePID;
	float throttle;
	float throttleOutput;

	float desiredVel;

	CircularBuffer accelBuffers[3];
	int accelBufferIndex;

	int texture;
};
