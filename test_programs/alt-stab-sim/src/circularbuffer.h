#pragma once

class CircularBuffer
{
private:
	float *buffer;
	int bufferSize;
	int bufferIndex;
	int numEntries;

	float filteredOutput;
	float bufferSum;

public:
	CircularBuffer();
	CircularBuffer(int bufferSize);
	~CircularBuffer();

	void addEntry(float value);
	float getFilteredOutput();
	float getBufferSum();
};
