#include "mqc.h"

#include "SL/sl.h"

#include "glm/glm.hpp"
#include "glm/gtc/random.hpp"
using namespace glm;

MQC::MQC(const vec2 &pos)
{
	this -> pos = pos;
	texture = slLoadTexture("png/mqc.png");

	throttlePID = PIDController(0.4f, 0.0f, 4.0f);
	throttle = 0.0f;
	throttleOutput = 0.0f;

	accelBuffers[0] = CircularBuffer(20);
	accelBuffers[1] = CircularBuffer(12);
	accelBuffers[2] = CircularBuffer(13);
	accelBufferIndex = 0;

	measuredVel = 0.0f;

	desiredVel = 0.0f;
}

MQC::~MQC() { }

void MQC::update(float dt)
{
	updatePhysics(dt);
	updateOutput(dt);
}

void MQC::updatePhysics(float dt)
{
	const float GRAVITY = 9.81f;

	const float MASS_GRAMS = 26.0f;
	const float MAX_THROTTLE_FORCE = 150.0f * 4.0f;				// each rotor produces ~14g of thrust

	const float MAX_VEL = 500.0f;

	vec2 oldVel = vel;
	vec2 thrust = vec2(0.0f, (throttleOutput * MAX_THROTTLE_FORCE) / MASS_GRAMS);

	// apply acceleration and velocity
	accel = vec2(0.0f, -GRAVITY);
	vel += accel + thrust;

	// air drag limits our upwards velocity
	if(vel.y > MAX_VEL)
	{
		vel.y = MAX_VEL;
	}

	// apply velocity to position
	pos += (vel * dt);

	// prevent from going out of bounds
	if(pos.y < 0.0f)
	{
		pos.y = 0.0f;
		accel = vec2(0.0f);
		vel.y = 0.0f;
	}
	if(pos.y > 480.0f)
	{
		pos.y = 480.0f;
		vel.y = 0.0f;
	}

	// take accelerometer reading
	measuredAccel = (vel - oldVel) + vec2(0.0f, GRAVITY) + vec2(0.0f, gaussRand(0.0f, 0.5f));

	// integrate to get an estimate of velocity
	//measuredVel += measuredAccel - GRAVITY;
	/*
	accelBuffers[accelBufferIndex++].addEntry(measuredAccel.y - GRAVITY);
	if(accelBufferIndex >= 3)
	{
		accelBufferIndex = 0;
	}*/
/*
	measuredVel = (0.1f * accelBuffers[0].getBufferSum()) +
				  (0.4f * accelBuffers[1].getBufferSum()) +
				  (0.5f * accelBuffers[2].getBufferSum());
*/

	//accelBuffers[0].addEntry(measuredAccel.y);

	accelBuffers[0].addEntry(measuredAccel.y);

	//measuredVel += accelBuffers[0].getFilteredOutput();//measuredAccel.y - GRAVITY;

	//measuredVel += measuredAccel.y - GRAVITY;

	//measuredVel = (0.5f * accelBuffer.getBufferSum()) + (0.5f * (measuredAccel.y - GRAVITY));

	//if(measuredVel > 50) measuredVel = 50;
	//else if(measuredVel < -50) measuredVel = -50;
}

void MQC::updateOutput(float dt)
{
	const float GRAVITY = 9.81f;

	const float THROTTLE_ACCEL = 3.0f;

	throttle = 0.1f + throttlePID.update(GRAVITY - accelBuffers[0].getFilteredOutput());

	if(throttle > 1.0f) throttle = 1.0f;
	else if(throttle < 0.0f) throttle = 0.0f;

	//throttle = 1.0f;

	if(throttleOutput < throttle)
	{
		throttleOutput += THROTTLE_ACCEL * dt;
		if(throttleOutput > throttle)
		{
			throttleOutput = throttle;
		}
	}
	else if(throttleOutput > throttle)
	{
		throttleOutput -= THROTTLE_ACCEL * dt;
		if(throttleOutput < throttle)
		{
			throttleOutput = throttle;
		}
	}
}

void MQC::render()
{
	const vec2 MQC_SIZE(128.0f, 128.0f);

	slSprite(texture, pos.x, pos.y + MQC_SIZE.y / 2.0f, MQC_SIZE.x, MQC_SIZE.y);
}

float MQC::getMeasuredAccelY()
{
	return measuredAccel.y;
}

float MQC::getMeasuredVelY()
{
	return measuredVel;
}

float MQC::getThrottle()
{
	return throttle;
}

void MQC::setDesiredVel(float desiredVel)
{
	this -> desiredVel = desiredVel;
}
