#include "mqc.h"

#include "SL/sl.h"

#include "glm/glm.hpp"
using namespace glm;

#include <cstdlib>
#include <ctime>
#include <iostream>
#include <iomanip>
#include <sstream>
using namespace std;

int main(int args, char *argv[])
{
	const vec2 WINDOW_SIZE(512, 512);
	const char *WINDOW_TITLE = "Altitude Stabilization Test";

	const vec2 BACKGROUND_CENTER(WINDOW_SIZE / 2.0f);
	const vec2 BACKGROUND_SIZE(WINDOW_SIZE.x);

	const vec2 TEXT_START_POS(10.0f, WINDOW_SIZE.y - 20.0f);
	const float TEXT_PADDING = 16.0f;

	MQC *mqc;
	stringstream ss;
	vec2 textPos;

	int backgroundTexture;
	float dt;
	bool done;

	// create our SIGIL window
	slWindow(WINDOW_SIZE.x, WINDOW_SIZE.y, WINDOW_TITLE, 0);
	slSetBackColor(0.0, 0.0, 0.0);
	slSetFont(slLoadFont("ttf/white_rabbit.ttf"), 14);
	backgroundTexture = slLoadTexture("png/background.png");

	// set up MQC simulation
	mqc = new MQC(vec2(WINDOW_SIZE / 2.0f));

	// keep looping until user closes window or presses ESC
	done = false;
	while(!done)
	{
		dt = slGetDeltaTime();

		// update and render the world, with the camera moved
		slSetSpriteTiling(16.0f, 16.0f);
		slSprite(backgroundTexture, BACKGROUND_CENTER.x, BACKGROUND_CENTER.y, BACKGROUND_SIZE.x, BACKGROUND_SIZE.y);

		if(slGetKey('Q'))
		{
			mqc -> setDesiredVel(50.0f);
		}
		else if(slGetKey('W'))
		{
			mqc -> setDesiredVel(-50.0f);
		}
		else
		{
			mqc -> setDesiredVel(0.0f);
		}

		// update and show the quadcopter
		mqc -> update(dt);
		slSetSpriteTiling(1.0f, 1.0f);
		mqc -> render();

		textPos = TEXT_START_POS;

		ss.str("");
		ss << "accel:    " << fixed << showpoint << setprecision(2) << mqc -> getMeasuredAccelY();
		slText(textPos.x, textPos.y, ss.str().c_str());
		textPos -= vec2(0.0f, TEXT_PADDING);

		ss.str("");
		ss << "vel:      " << fixed << showpoint << setprecision(2) << mqc -> getMeasuredVelY();
		slText(textPos.x, textPos.y, ss.str().c_str());
		textPos -= vec2(0.0f, TEXT_PADDING);

		ss.str("");
		ss << "throttle: " << fixed << showpoint << setprecision(2) << mqc -> getThrottle();
		slText(textPos.x, textPos.y, ss.str().c_str());
		textPos -= vec2(0.0f, TEXT_PADDING);

		slRender();

		// is the user done?
		done = slShouldClose() || slGetKey(SL_KEY_ESCAPE);
	}

	// we're done; close the window and terminate SIGIL
	slClose();

	return 0;
}
