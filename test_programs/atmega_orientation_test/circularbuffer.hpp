#pragma once

#include <avr/io.h>

// this is a circular buffer class which only computes a filtered output
// using the averages of values actually contained in the buffer, meaning
// that the filtered output is still somewhat accurate as it initially
// gets more full

class CircularBuffer
{
private:
	float *buffer;
	uint8_t bufferSize;
	uint8_t bufferIndex;
	uint8_t numEntries;

	float filteredOutput;
	float bufferSum;

public:
	CircularBuffer(uint8_t bufferSize);

	void addEntry(float value);
	float getFilteredOutput();
};
