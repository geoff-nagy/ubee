#include "circularbuffer.hpp"

#include <avr/io.h>
#include <stdlib.h>

CircularBuffer::CircularBuffer(uint8_t bufferSize)
{
	this -> bufferSize = bufferSize;
	buffer = (float*)malloc(sizeof(float) * bufferSize);

	numEntries = 0;
	bufferIndex = 0;

	filteredOutput = 0;
	bufferSum = 0;
}

void CircularBuffer::addEntry(float value)
{
	// track how many entries are currently in the buffer
	numEntries ++;
	if(numEntries >= bufferSize)
	{
		numEntries = bufferSize;
		bufferSum -= buffer[bufferIndex];		// oldest value is only removed if the buffer is full
	}

	// insert the value into the buffer
	buffer[bufferIndex++] = value;
	if(bufferIndex >= bufferSize)
		bufferIndex = 0;
	bufferSum += value;							// add the new value to the running sum for average

	// compute the new mean
	filteredOutput = bufferSum / (float)numEntries;
}

float CircularBuffer::getFilteredOutput()
{
	return filteredOutput;
}
