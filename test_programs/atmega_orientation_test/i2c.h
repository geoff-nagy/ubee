#pragma once

#include <avr/io.h>

void i2c_init();

uint8_t i2cWrite(uint8_t device, uint8_t *data, uint16_t n);
uint8_t i2cRead(uint8_t device, char *data, uint16_t n, uint16_t address);
