#include "OrangutanSerial.h"
#include "MPU6050.h"
#include "I2Cdev.h"
#include "circularbuffer.hpp"
#include "time.hpp"

#include <avr/io.h>
#include <util/delay.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#define ACCEL_BUFFER_SIZE 7

#define LED_PORT PORTC
#define LED_PIN 3
#define LED_DIR DDRC

int main(void)
{
	uint8_t bufferSize = 10;
	uint8_t buffer[bufferSize];

	LED_DIR |= (1 << LED_PIN);
	LED_PORT &= ~(1 << LED_PIN);

	uint8_t i;

	for(i = 0; i < 5; i ++)
	{
		LED_PORT |= (1 << LED_PIN);
		_delay_ms(40);
		LED_PORT &= ~(1 << LED_PIN);
		_delay_ms(40);
	}

	
	int16_t ax, ay, az, gx, gy, gz;

	Fastwire::setup(100, false);
	_delay_ms(500);
	MPU6050 mpu6050;
	_delay_ms(500);
	mpu6050.initialize();
	serial_set_baud_rate(9600);
	_delay_ms(500);

	if(mpu6050.testConnection())
	{
		//serial_send((char*)"yes", 3);
	}
	else
	{
		//serial_send((char*)"no", 2);
	}

	//serial_send("done\r\n\n", 7);

	CircularBuffer accelBufferX(ACCEL_BUFFER_SIZE);
	CircularBuffer accelBufferY(ACCEL_BUFFER_SIZE);
	//CircularBuffer accelBufferZ(ACCEL_BUFFER_SIZE);

	uint16_t dt = 0;

	time_init();

	float pitch = 0;
	float roll = 0;

	float filterGain = 0.98;

	while(1)
	{
		uint16_t startTime = micros();

		// retrieve raw accel and gyro data
		mpu6050.getAcceleration(&ax, &ay, &az);
		mpu6050.getRotation(&gx, &gy, &gz);
		
		// rotate axes
		float accelAngleX =  (float)atan2(ax, az) * 180.0 / M_PI;//atan2(az, (sqrt(ay * ay + ax * ax))) * 180 / M_PI;
		float accelAngleY =  (float)atan2(ay, az) * 180.0 / M_PI;//-atan2(ax, (sqrt(ay * ay + az * az))) * 180 / M_PI;
		
		// integrate gyro values
		float gyroX = pitch + (gx / 131) * ((float)dt / 1000.0) / 1000.0;//gx * (((double)dt / (double)1000) / (double)131);
		float gyroY = roll - ((gy / 131) * ((float)dt / 1000.0) / 1000.0);//gy * (((double)dt / (double)1000) / (double)131);

		// now pass accelerometer and integrated gyro value into a complimentary filter
		pitch = (filterGain * gyroX) + ((1.0 - filterGain) * accelAngleY);
		roll = (filterGain * gyroY) + ((1.0 - filterGain) * accelAngleX);

		// put the angle data into a buffer to send over serial
		buffer[0] = 0xff;
		memcpy(&buffer[1], (const void*)&pitch, 4);
		memcpy(&buffer[5], (const void*)&roll, 4);

		// send the data and mark the current time
		serial_send_blocking((char*)buffer, 9);
		dt = micros() - startTime;
	}

	/*
	serial_send((char*)"S", 1);

	devStatus = mpu6050.dmpInitialize();
	//mpu6050.setXGyroOffset(220);
	//mpu6050.setYGyroOffset(76);
	//mpu6050.setZGyroOffset(-85);
	//mpu6050.setZAccelOffset(1788);//1688

	serial_send((char*)"D", 1);

	if(devStatus == 0)
	{
		mpu6050.setDMPEnabled(true);

		// configure for rising pin interrupt here

		mpuIntStatus = mpu6050.getIntStatus();
		dmpReady = true;

		packetSize = mpu6050.dmpGetFIFOPacketSize();

		serial_send((char*)"ready", 5);
	}
	else
	{
		serial_send((char*)"not ready", 9);
	}

	//serial_send("\n", 1);
	
    while(1)
    {

		// check if we've got new data (we don't need interrupts for this)
		fifoCount = mpu6050.getFIFOCount();
		if(fifoCount >= packetSize)
		{
			mpuIntStatus = mpu6050.getIntStatus();
			if(mpuIntStatus & 0x10 || fifoCount == 1024)
			{
				mpu6050.resetFIFO();
				serial_send((char*)"OVF", 3);
			}
			else if(mpuIntStatus & 0x02)
			{
				// brief wait for correct data amount
				while(fifoCount < packetSize) fifoCount = mpu6050.getFIFOCount();

				// read the data
				mpu6050.getFIFOBytes(fifoBuffer, packetSize);

				// track fifo count
				fifoCount -= packetSize;

				mpu6050.dmpGetQuaternion(&q, fifoBuffer);
				mpu6050.dmpGetEuler(euler, &q);

				int len = sprintf(buffer, "\ry: %d, p: %d, r: %d",
				                  euler[0] * 180 / M_PI,
								  euler[1] * 180 / M_PI,
								  euler[2] * 180 / M_PI);

				serial_send(buffer, len);
			}
		}

		//_delay_ms(200);

		//mpu6050.getMotion6(&ax, &ay, &az, &gx, &gy, &gz);

		//int len = sprintf(buffer, "ax: %d, ay: %d, az: %d\r", ax, ay, az);
		//serial_send(buffer, len);

		//_delay_ms(200);
		//serial_send(data, 12);
    }*/
}
