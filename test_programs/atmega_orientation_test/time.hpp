#pragma once

void time_init();

unsigned long millis();
unsigned long micros();

void delayMicros(unsigned int us);
