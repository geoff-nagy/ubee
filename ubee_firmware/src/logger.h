#pragma once

#include "rfm69/RFM69.h"

// initialize the logger with the RFM handle and the rate to take new values in milliseconds
void loggerInit(RFM69 *rfm, uint16_t rateMS);

// should be called regularly; dt is is microseconds
void loggerUpdate(uint16_t dt, float pitch, float roll, float pitchRate, float rollRate, float yawRate,
							   float pitchPIDOut, float rollPIDOut, float pitchRatePIDOut, float rollRatePIDOut, float yawRatePIDOut);
