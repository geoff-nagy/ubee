#pragma once

#include <avr/io.h>

// this is a simple buffer that clamps the delta from each new output to
// not exceed a certain threshold; hopefully useful for dampening
// accelerometer vibrations

class StepLimitFilter
{
private:
	float value;
	float maxSpeed;

public:
	StepLimitFilter();
	StepLimitFilter(float value, float maxSpeed);

	void addEntry(float value);
	float getFilteredOutput();
};
