#include "steplimitfilter.h"

#include <avr/io.h>
#include <stdlib.h>

StepLimitFilter::StepLimitFilter()
{
	value = 0.0;
	maxSpeed = 0.0;
}

StepLimitFilter::StepLimitFilter(float value, float maxSpeed)
{
	this -> value = value;
	this -> maxSpeed = maxSpeed;
}

void StepLimitFilter::addEntry(float newValue)
{
	if((newValue - value) > maxSpeed)
	{
		value += maxSpeed;
	}
	else if((value - newValue) > maxSpeed)
	{
		value -= maxSpeed;
	}
	else
	{
		value = newValue;
	}
}

float StepLimitFilter::getFilteredOutput()
{
	return value;
}
