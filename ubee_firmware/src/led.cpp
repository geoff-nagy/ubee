#include "led.h"

#include <avr/io.h>
#include <util/delay.h>

#define LED_PORT PORTC
#define LED_DIR DDRC
#define LED_PIN 3

#define SLOW_ON_TIME_MS 20
#define SLOW_OFF_TIME_MS 120

#define FAST_ON_TIME_MS 10
#define FAST_OFF_TIME_MS 50

void initLED()
{
	LED_PORT &= ~(1 << LED_PIN);
	LED_DIR |= (1 << LED_PIN);
}

void ledFlashSlow(uint8_t times)
{
	while(times--)
	{
		ledOn();
		_delay_ms(SLOW_ON_TIME_MS);
		ledOff();
		_delay_ms(SLOW_OFF_TIME_MS);
	}
}

void ledFlashFast(uint8_t times)
{
	while(times--)
	{
		ledOn();
		_delay_ms(FAST_ON_TIME_MS);
		ledOff();
		_delay_ms(FAST_OFF_TIME_MS);
	}
}

void ledOn()
{
	LED_PORT |= (1 << LED_PIN);
}

void ledOff()
{
	LED_PORT &= ~(1 << LED_PIN);
}

void led(bool on)
{
	if(on) ledOn();
	else   ledOff();	
}
