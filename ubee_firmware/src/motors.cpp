#include "motors.h"

#include <avr/io.h>

// port and pin output for motor 1
#define MOTOR1_PORT PORTD
#define MOTOR1_DIR DDRD
#define MOTOR1_PIN 0x06

// port and pin output for motor 2
#define MOTOR2_PORT PORTD
#define MOTOR2_DIR DDRD
#define MOTOR2_PIN 0x05

// port and pin output for motor 3
#define MOTOR3_PORT PORTD
#define MOTOR3_DIR DDRD
#define MOTOR3_PIN 0x03

// port and pin output for motor 4
#define MOTOR4_PORT PORTB
#define MOTOR4_DIR DDRB
#define MOTOR4_PIN 0x01

// how fast the motors can go---from 0 to 255 inclusive, since we set 8-bit hardware timers to PWM the motors
#define MIN_MOTOR_SPEED 0
#define MAX_MOTOR_SPEED 255

// private prototypes
static void initMotor1();
static void initMotor2();
static void initMotor3();
static void initMotor4();
static int16_t clamp(int16_t value, int16_t min, int16_t max);

void initMotors()
{
	initMotor1();
	initMotor2();
	initMotor3();
	initMotor4();
}

void setMotor1Speed(int16_t speed)
{
	OCR0A = (uint8_t)clamp(speed, MIN_MOTOR_SPEED, MAX_MOTOR_SPEED);
}

void setMotor2Speed(int16_t speed)
{
	OCR0B = (uint8_t)clamp(speed, MIN_MOTOR_SPEED, MAX_MOTOR_SPEED);
}

void setMotor3Speed(int16_t speed)
{
	OCR2B = (uint8_t)clamp(speed, MIN_MOTOR_SPEED, MAX_MOTOR_SPEED);
}

void setMotor4Speed(int16_t speed)
{
	OCR1A = (uint8_t)clamp(speed, MIN_MOTOR_SPEED, MAX_MOTOR_SPEED);
}

// - - - private functions - - - //

void initMotor1()
{
	// set output compare value to nothing
	OCR0A = 0x0;
	
	// clear output compare pin value and set as output
	MOTOR1_PORT &= ~(1 << MOTOR1_PIN);
	MOTOR1_DIR |= (1 << MOTOR1_PIN);
	
	// set to fast PWM mode with TOP defined as 0xFF
	TCCR0A |= (1 << WGM00);
	TCCR0A |= (1 << WGM01);
	TCCR0B &= ~(1 << WGM02);
	
	// set clock prescaler to 1
	TCCR0B &= ~(1 << CS02);
	TCCR0B &= ~(1 << CS01);
	TCCR0B |= (1 << CS00);
	
	// turn off OC0A connect it to timer 0; set fast PWM output compare mode: clear OC0A on compare match, set OC0A at BOTTOM
	MOTOR1_PORT &= ~(1 << MOTOR1_PIN);
	TCCR0A |= (1 << COM0A1);
	TCCR0A &= ~(1 << COM0A0);
}

void initMotor2()
{
	// set output compare value to nothing
	OCR0B = 0x0;
	
	// clear output compare pin value and set as output
	MOTOR2_PORT &= ~(1 << MOTOR2_PIN);
	MOTOR2_DIR |= (1 << MOTOR2_PIN);
	
	// fast PWM mode for timer 0 already set in initMotor1()
	
	// clock prescalar for timer 0 already set in initMotor2()
	
	// turn off OC0B and connect it to timer 0; set fast PWM output compare mode: clear OC0B on compare match, set OC0B at BOTTOM
	MOTOR2_PORT &= ~(1 << MOTOR2_PIN);
	TCCR0A |= (1 << COM0B1);
	TCCR0A &= ~(1 << COM0B0);
}

void initMotor3()
{
	// set output compare value to nothing
	OCR2B = 0x0;
	
	// clear output compare pin value and set as output
	MOTOR3_PORT &= ~(1 << MOTOR3_PIN);
	MOTOR3_DIR |= (1 << MOTOR3_PIN);
	
	// set fast PWM mode for timer 2
	TCCR2B &= ~(1 << WGM22);
	TCCR2A |= (1 << WGM21);
	TCCR2A |= (1 << WGM20);
	
	// set clock prescalar to 1
	TCCR2B &= ~(1 << CS22);
	TCCR2B &= ~(1 << CS21);
	TCCR2B |= (1 << CS20);
	
	// turn off OC2B and connect it to timer 2; set fast PWM output compare mode: clear OC2B on compare match; set OC2B at BOTTOM
	MOTOR3_PORT &= (1 << MOTOR3_PIN);
	TCCR2A |= (1 << COM2B1);
	TCCR2A &= ~(1 << COM2B0);
}

void initMotor4()
{
	// set output compare value to nothing
	OCR1A = 0x0;
	
	// clear output compare pin value and set as output
	MOTOR4_PORT &= ~(1 << MOTOR4_PIN);
	MOTOR4_DIR |= (1 << MOTOR4_PIN);
	
	// set 8-bit fast PWM mode for timer 1
	TCCR1B &= ~(1 << WGM13);
	TCCR1B |= (1 << WGM12);
	TCCR1A &= ~(1 << WGM11);
	TCCR1A |= (1 << WGM10);
	
	// set clock prescalar to 1
	TCCR1B |= (1 << CS10);
	TCCR1B &= ~(1 << CS11);
	TCCR1B &= ~(1 << CS12);
	
	// turn off 0C1A and connect it to timer 1; set fast PWM output compare mode: clear OC1A on compare match, set OC1A at BOTTOM
	MOTOR4_PORT &= ~(1 << MOTOR4_PIN);
	TCCR1A |= (1 << COM1A1);
	TCCR1A &= ~(1 << COM1A0);
}

int16_t clamp(int16_t value, int16_t min, int16_t max)
{
	if     (value < min) value = min;
	else if(value > max) value = max;
	return value;
}