// uBee Firmware
// Geoff Nagy
// Firmware for my custom-built micro-quadcopter

#include "led.h"
#include "battery.h"
#include "motors.h"
#include "pidcontroller.h"
#include "circularbuffer.h"
#include "steplimitfilter.h"

#include "rfm69/RFM69.h"
#include "mpu6050/MPU6050.h"
#include "mpu6050/I2Cdev.h"

#include <avr/io.h>
#include <util/delay.h>

#include <stdfix.h>

// - - - wireless config settings - - - //

// common across MQC and remote control
#define FREQUENCY RF69_915MHZ
#define NETWORK_ID 45

// IDs are device-specific
#define REMOTE_ID 43												// ID of remote control
#define MQC_ID 44													// ID of MQC device

// drone addressing broadcast ID
#define DRONE_BROADCAST_ID 255

// defines how TX retries are handled, in the event the remote wants data back from us
#define NUM_RETRIES 5
#define RETRY_WAIT_MS 250

// how often to transmit IMU status if requested
#define IMU_TRANSMIT_MS 50

// - - - messaging settings - - - //

// generic message format:

// format       : [header, 2 bytes]  |  [ ... data ... ]
// byte index   :    0         1     |     2      [...]                 (len - 2)
// byte meaning : [ cmd ]   [ len ]  |  [ id ]    [ ...command-specific data... ]

// "cmd" is one of the command bytes below
// "len" is the remaining length of the message in bytes, after (and not including) this "len" byte
// "id" is either a unicast ID [0-254] referring to a single drone or the broadcast ID 255

// this means that every command will have a "len" byte of at least 1, containing at least the ID byte

// each message begins with a command and a length
#define MSG_HEADER_SIZE 2

// command bytes
#define MSG_FLASH_LED 1						// instruct drone to flash its LED
#define MSG_SET_ID 2						// instruct drone to change its ID
#define MSG_SET_PARAMS 3					// instruct drone to change a set of parameters
#define MSG_REQUEST_DATA 4					// instruct drone to return some data
#define MSG_SINGLE_FLIGHT_COMMAND 5			// send flight control command (pitch, roll, yaw)
#define MSG_COMPOUND_FLIGHT_COMMAND 6		// send multiple different flight control commands to multiple ID'd drones
#define MSG_ACK 7							// generic acknowledgment
#define MSG_NAK 8							// generic non-acknowledgment
#define MSG_RECALIBRATE_IMU 9				// instruct drone to recalibrate its IMU
#define MSG_START_IMU_TEST 10				// instruct drone to start continuous transmission of IMU values
#define MSG_END_IMU_TEST 11					// instruct drone to stop continuous transmission of IMU values
#define MSG_IMU_STATE 12					// this is a special message from the drone containing IMU state
#define MSG_DATA_PACKET 13					// some data packet sent by the drone

// these denote groups of data that can be requested from the uBee; various flight parameters, current IMU reading, and generic uBee status
#define DATA_PARAMS_LEVELING 0
#define DATA_PARAMS_ROTATION_RATE 1
#define DATA_PARAMS_RESPONSE_AND_TRIM 2
#define DATA_STATUS 3

// different parameter group lengths; relevant to MSG_{REQUEST, SET}_PARAMS
#define NUM_LEVELING_PID_BYTES 32
#define NUM_ROTATION_RATE_PID_BYTES 48
#define NUM_RESPONSE_AND_TRIM_BYTES 24
#define NUM_STATUS_BYTES 15
#define NUM_IMU_BYTES 3

// locations in eeprom for drone configuration; I've left some space in between sections just for future convenience in case we need to add more
#define EEPROM_ID_ADDRESS 0											// where the drone ID is stored
#define EEPROM_PARAMS_ADDRESS 8										// where flight parameters are stored
#define EEPROM_IMU_CALIBRATION_ADDRESS 256							// where IMU calibration information is stored

// misc. settings
#define YAW_CIRCULAR_BUFFER_SIZE 3									// size of circular buffer for averaging out yaw rate readings
#define NUM_IMU_CALIBRATION_STEPS 50								// how many averages to sample when calibrating IMU zero position

// - - - misc debugging/visual info settings - - - //

// number of times to flash LED for certain events
#define LED_NUM_FLASHES_CONFIG_COMMAND 2							// after receiving a non-[dis]arm configuration command
#define LED_NUM_FLASHES_ERROR 10									// used to indicate some kind of error

// other settings
#define BATTERY_MONITORING_ENABLED									// when defined, battery levels are checked every few seconds

// - - - global vars - - - //

// some useful math constants
const float ONE_QTR_PI = M_PI / 4.0;
const float THREE_QTR_PI = 3.0 * M_PI / 4.0;

// handle to RFM69 interface
RFM69 rfm;

// handle to MPU6050 interface
MPU6050 mpu6050;

// drone ID---should be between 0 and 254; 255 is the broadcast ID
uint8_t id = 0;

// delta time value in microseconds
uint16_t dt = 0;

// final computed orientation of the device, used to stabilize pitch and roll
float pitch = 0.0;
float roll = 0.0;
float pitchRate = 0.0;
float rollRate = 0.0;
float yawRate = 0.0;

// are we reporting IMU values over radio?
bool reportingIMUState = false;
uint16_t imuStateUS = 0;
uint16_t imuStateMS = 0;

// we use a simple circular buffer to handle yaw stabilization
CircularBuffer yawFilter(YAW_CIRCULAR_BUFFER_SIZE);

// values for pitch, roll, and z acceleration sensing that we consider to be neutral; IMU calibration sets these values
float pitchCenter = 0.0;
float rollCenter = 0.0;
float accelZCenter = 0.0;

// these values are (continuously) set by the remote control and is what gives the pilot control
uint8_t pitchThrust = 128;
uint8_t rollThrust = 128;
uint8_t yawThrust = 128;
uint8_t throttle = 0;

// speeds for each motor; they will be clamped to [0, 255] eventually by the motor functions
int16_t motor1Speed = 0;
int16_t motor2Speed = 0;
int16_t motor3Speed = 0;
int16_t motor4Speed = 0;

// - - - global flight params - - - //

// PID controllers used to stabilize flight
PIDController pitchPID;
PIDController rollPID;
PIDController pitchRatePID;
PIDController rollRatePID;
PIDController yawRatePID;

// pitch and roll trim values for manual adjustments
float pitchTrim = 0.0;
float rollTrim = 0.0;

// strength of response to joystick/controller/piloting directions
float pitchDemandGain = 0.0;
float rollDemandGain = 0.0;
float yawDemandGain = 0.0;

// complimentary filter settings
float compFilterAlpha = 0.0;
float oneMinusCompFilterAlpha = 0.0;

// thrust and takeoff control
uint8_t maxThrust = 230;

// - - - prototypes - - - //

// loading and saving flight params to/from EEPROM
void loadIDFromEEPROM();
void saveIDToEEPROM();
void loadParamsFromEEPROM();
void saveParamsToEEPROM();
void loadCalibrationParamsFromEEPROM();
void saveCalibrationParamsToEEPROM();
void resetPIDControllers();
void resetOrientation();

// convenience functions for copying flight params around for easy RX/TX via radio
void copyParamsFrom(uint8_t paramGroup, uint8_t *bytes);
void copyParamsTo(uint8_t paramGroup, uint8_t *bytes);

// main logic handling
void processCommands(uint16_t dt);
void handleFlight(uint16_t dt);
void handleIMUTest(uint16_t dt);
void handleFlipping(uint16_t dt);
void calibrateIMU();

// useful math functions
float clampf(float value, float min, float max);
float atan2Approx(float y, float x);

// - - - main function - - - //

int main(void)
{
	uint16_t startTime;							// used to compute time delta in microseconds

	// start up our (modified) Arduino-based timing
	init();

	// initialize motor hardware timers
	initMotors();

	// initialize LED output settings
	initLED();

	// 1 flash indicates main power; should always happen if LED and power are correctly connected
	ledFlashSlow(1);
	_delay_ms(150);

	// load flight configuration parameters---these are stored in EEPROM for easy re-configuration
	loadIDFromEEPROM();
	loadParamsFromEEPROM();
	loadCalibrationParamsFromEEPROM();
	resetPIDControllers();

	// battery-level monitoring is optional
	#ifdef BATTERY_MONITORING_ENABLED
		initBatteryMonitor();
	#endif

	// initialize the RFM library
	rfm.initialize(FREQUENCY, MQC_ID, NETWORK_ID);
	rfm.setHighPower(true);
	rfm.setPowerLevel(31);

	// second flash indicates radio is ready
	ledFlashSlow(1);

	// initialize the MPU6050 library
	Fastwire::setup(100, false);
	_delay_ms(150);
	mpu6050.initialize();

	// third flash indicates IMU is ready and we are ready to proceed
	ledFlashSlow(1);

	// main control loop
	while(true)
	{
		// get current time in microseconds
		startTime = micros();

		// listen for commands coming over the radio
		processCommands(dt);

		// handle all stabilization and flight maneuvers
		handleFlight(dt);

		// handle IMU testing transmission
		handleIMUTest(dt);

		// monitor battery voltage level and report low battery if needed
		#ifdef BATTERY_MONITORING_ENABLED
			handleBatteryMonitor(dt);
		#endif

		// compute delta time
		dt = micros() - startTime;
	}

	// good practice to have this at the end of main() just in case
	while(true);
}

// - - - flight and control functions - - - //

void processCommands(uint16_t dt)
{
	uint8_t msgType = 0;						// command type of the incoming msg
	uint8_t msgLen = 0;							// remaining message bytes after the len byte
	uint8_t msgID = 0;							// ID referenced in the incoming msg
	uint8_t msgDataPacketType;
	uint8_t *commandContents;					// used to iterate through received command data
	uint8_t tempDataTX[64];						// used when sending back stats or flight params via radio
	uint8_t bytesProcessed;						// used when processing compound flight commands so we know when we've reached the end

	// see if we've received anything
	if(rfm.receiveDone())
	{
		// get basic message info; these first three bytes will always be present
		msgType = rfm.DATA[0];
		msgLen = rfm.DATA[1];
		msgID = rfm.DATA[2];

		// does this command address us?
		if((msgID == id || msgID == DRONE_BROADCAST_ID) && msgType != MSG_COMPOUND_FLIGHT_COMMAND)
		{
			// we've been sent a flight control command
			if(msgType == MSG_SINGLE_FLIGHT_COMMAND)
			{
				// set our flight controls as instructed
				commandContents = (uint8_t*)&rfm.DATA[3];
				yawThrust = *(commandContents++);
				throttle = *(commandContents++);
				rollThrust = *(commandContents++);
				pitchThrust = *(commandContents++);

				// throttle dead zone for safety
				if(throttle < 10)
				{
					throttle = 0;
				}

				// scale throttle to be in desired max throttle range
				throttle = ((float)throttle / 255.0) * maxThrust;
			}
			else if(throttle < 10)				// all other commands require the motors to be stopped
			{
				if(msgType == MSG_FLASH_LED)
				{
					// send an ACK immediately, if requested (and it should be!)
					if(rfm.ACKRequested()) rfm.sendACK();
					ledFlashFast(LED_NUM_FLASHES_CONFIG_COMMAND);
				}
				else if(msgType == MSG_SET_ID)
				{
					// change our ID and save immediately
					id = rfm.DATA[3];													// in this context, DATA[3] is the new ID
					saveIDToEEPROM();
					ledFlashFast(LED_NUM_FLASHES_CONFIG_COMMAND);
				}
				else if(msgType == MSG_SET_PARAMS)
				{
					// send an ACK immediately afterwards, if requested (and it should be!)
					if(rfm.ACKRequested()) rfm.sendACK();
					ledFlashFast(LED_NUM_FLASHES_CONFIG_COMMAND);

					// store the floating-point PID gain values
					commandContents = (uint8_t*)(&rfm.DATA[4]);							// in this context, DATA[5] is the start of the parameters
					copyParamsFrom(rfm.DATA[3], commandContents);						// in this context, DATA[3] is the parameter group requested

					// now write those values to EEPROM and reset our internal state
					saveParamsToEEPROM();
					resetPIDControllers();
					resetOrientation();
				}
				else if(msgType == MSG_REQUEST_DATA)
				{
					// determine what kind of data is being requested
					msgDataPacketType = rfm.DATA[3];

					// determine message length based on kind of data being requested/sent
					if     (msgDataPacketType == DATA_PARAMS_LEVELING)          msgLen = NUM_LEVELING_PID_BYTES;
					else if(msgDataPacketType == DATA_PARAMS_ROTATION_RATE)     msgLen = NUM_ROTATION_RATE_PID_BYTES;
					else if(msgDataPacketType == DATA_PARAMS_RESPONSE_AND_TRIM) msgLen = NUM_RESPONSE_AND_TRIM_BYTES;
					else if(msgDataPacketType == DATA_STATUS)                   msgLen = NUM_STATUS_BYTES;
					else                                                        msgLen = 0;

					// make sure we're not requesting some weird parameter group
					if(msgLen > 0)
					{
						// configure message size: add room for ID byte and data packet type
						msgLen += 2;

						// build the return message containing the data
						tempDataTX[0] = MSG_DATA_PACKET;									// indicate that we're sending a requested data packet back
						tempDataTX[1] = msgLen;												// total length of message after the LEN byte
						tempDataTX[2] = id;													// say who this is coming from
						tempDataTX[3] = msgDataPacketType;									// say what kind of data we're sending back
						copyParamsTo(msgDataPacketType, &tempDataTX[4]);

						// transmit after a brief delay
						_delay_ms(10);
						rfm.receiveDone();
						while(!rfm.canSend());
						rfm.send(REMOTE_ID, (const void*)tempDataTX, MSG_HEADER_SIZE + msgLen);		// we send (cmd, type) + (id, data type requested) + (data) bytes

						// LED confirmation *after* we transmit so we don't delay our response beforehand
						ledFlashFast(LED_NUM_FLASHES_CONFIG_COMMAND);
					}
					else
					{
						// show an error LED flash sequence; some weird parameter group was requested
						ledFlashFast(LED_NUM_FLASHES_ERROR);
					}
				}
				else if(msgType == MSG_RECALIBRATE_IMU)
				{
					// device performs IMU calibration
					if(rfm.ACKRequested()) rfm.sendACK();
					calibrateIMU();
				}
				else if(msgType == MSG_START_IMU_TEST)
				{
					// device begins transmission
					if(rfm.ACKRequested()) rfm.sendACK();
					reportingIMUState = true;
				}
				else if(msgType == MSG_END_IMU_TEST)
				{
					// device ends transmission
					if(rfm.ACKRequested()) rfm.sendACK();
					reportingIMUState = false;
				}
			}
		}
		// special case: this is a compound flight command and might be addressed to us
		else if(msgType == MSG_COMPOUND_FLIGHT_COMMAND)
		{
			// prime the reading of the compound flight command
			commandContents = (uint8_t*)&rfm.DATA[2];
			bytesProcessed = 0;

			// look for our ID in the compound flight command until we run out of commands
			while(bytesProcessed < msgLen)
			{
				msgID = *commandContents;
				if(msgID == id)
				{
					// pull demands in this compound flight command that are addressed to us
					++ commandContents;						// skip over ID
					yawThrust = *(commandContents++);
					throttle = *(commandContents++);
					rollThrust = *(commandContents++);
					pitchThrust = *(commandContents++);

					// early exit
					bytesProcessed = msgLen;
				}
				else
				{
					// advance to next segment of compound flight command
					commandContents += 5;					// skip over ID and flight command bytes
					bytesProcessed += 5;					// factor into what we skipped over
				}
			}
		}
	}
}

void handleFlight(uint16_t dt)
{
	int16_t ax, ay, az, gx, gy, gz;								// raw accel and gyro readings from the MPU6050
	float accelAngleX, accelAngleY;								// accelerometer angles
	float gyroX, gyroY;											// (integrated) gyro values
	float seconds = (float)dt / 1000000.0;
	float pitchRate, rollRate;
	int16_t pitchDemand, rollDemand, yawDemand;
	float pitchAngleOutput, rollAngleOutput;
	int16_t pitchAngleRateOutput, rollAngleRateOutput, yawAngleRateOutput;
	int16_t throttleMinusPitch, throttlePlusPitch;
	int16_t throttleMinusPitchMinusRoll, throttlePlusPitchMinusRoll, throttlePlusPitchPlusRoll, throttleMinusPitchPlusRoll;

	// - - - orientation sensing - - - //
	// special thanks to: http://www.pieter-jan.com/node/11
	// for the complementary filter stuff, upon which this function's code is somewhat based

	// retrieve raw accelerometer and gyro data from the MPU6050
	mpu6050.getAcceleration(&ax, &ay, &az);
	mpu6050.getRotation(&gx, &gy, &gz);

	// compute pitch and roll in degrees from raw linear accelerometer values
	//float accelAngleX = ((float)atan2f(ay, az) * 180.0) / M_PI;
	//float accelAngleY = ((float)atan2f(ax, az) * 180.0) / M_PI;
	//accelAngleX = ((float)atan2Approx(ay, az) * 180.0) / M_PI;
	//accelAngleY = ((float)atan2Approx(ax, az) * 180.0) / M_PI;
	accelAngleX = atan2Approx(ay, az) * RAD_TO_DEG;
	accelAngleY = atan2Approx(ax, az) * RAD_TO_DEG;

	// compute pitch and roll rate
	pitchRate = (gx / 131) * seconds;
	rollRate = -(gy / 131) * seconds;

	// compute yaw rate (passed through averaging buffer); by default this is left in degrees per second
	//yawFilter.addEntry(-(gz / 131) * seconds);
	//yawRate = yawFilter.getFilteredOutput();
	yawRate = -(gz / 131);// * seconds;

	// integrate gyro values
	gyroX = pitch + pitchRate;
	gyroY = roll + rollRate;

	// now pass accelerometer and integrated gyro value into a complimentary filter
	pitch = (compFilterAlpha * gyroX) + (oneMinusCompFilterAlpha * accelAngleX);
	roll = (compFilterAlpha * gyroY) + (oneMinusCompFilterAlpha * accelAngleY);

	// compute desired demands for pitch and roll
	//pitchDemand = ((int16_t)pitchThrust - 128) / 10;
	//rollDemand = ((int16_t)rollThrust - 128) / 10;
	//yawDemand = ((int16_t)yawThrust - 128) / 10;
	pitchDemand = (((float)pitchThrust - 128.0f) / 128.0f) * pitchDemandGain;
	rollDemand = (((float)rollThrust - 128.0f) / 128.0f) * rollDemandGain;
	yawDemand = (((float)yawThrust - 128.0f) / 128.0f) * yawDemandGain;

	// get correction for orientation; this is fed into the rotation-rate controller below
	pitchAngleOutput = pitchPID.update(pitchDemand - (pitch + pitchTrim), seconds);
	rollAngleOutput = rollPID.update(rollDemand - (roll + rollTrim), seconds);

	// get correction for rotation rate
	pitchAngleRateOutput = pitchRatePID.update(pitchAngleOutput - pitchRate, seconds);
	rollAngleRateOutput = rollRatePID.update(rollAngleOutput - rollRate, seconds);
	yawAngleRateOutput = yawRatePID.update(yawDemand - yawRate, seconds);

	// note: there's really no need to do these calculations below if the throttle is low (~10 or less, or so), but we do them anyways
	// since it results in a more accurate wireless reporting of the drone's delta time step; thus, we get a better reading of the drone's
	// stabilization update rate even though we may not currently be flying

	// flight stabilization; organized to remove some redundant calculations

	throttleMinusPitch = throttle - pitchAngleRateOutput;
	throttlePlusPitch = throttle + pitchAngleRateOutput;

	throttleMinusPitchMinusRoll = throttleMinusPitch - rollAngleRateOutput;
	throttlePlusPitchMinusRoll = throttlePlusPitch - rollAngleRateOutput;
	throttlePlusPitchPlusRoll = throttlePlusPitch + rollAngleRateOutput;
	throttleMinusPitchPlusRoll = throttleMinusPitch + rollAngleRateOutput;

	motor1Speed = throttleMinusPitchMinusRoll + yawAngleRateOutput;
	motor2Speed = throttlePlusPitchMinusRoll - yawAngleRateOutput;
	motor3Speed = throttlePlusPitchPlusRoll + yawAngleRateOutput;
	motor4Speed = throttleMinusPitchPlusRoll - yawAngleRateOutput;

	// flight stabilization; old version with redundant calculations
	//motor1Speed = throttle - pitchAngleRateOutput - rollAngleRateOutput + yawAngleRateOutput;
	//motor2Speed = throttle + pitchAngleRateOutput - rollAngleRateOutput - yawAngleRateOutput;
	//motor3Speed = throttle + pitchAngleRateOutput + rollAngleRateOutput + yawAngleRateOutput;
	//motor4Speed = throttle - pitchAngleRateOutput + rollAngleRateOutput - yawAngleRateOutput;

	// throttle dead zone (see "note" above for why we don't skip the calculations altogether)
	if(throttle < 20)
	{
		motor1Speed = 0;
		motor2Speed = 0;
		motor3Speed = 0;
		motor4Speed = 0;
		resetPIDControllers();
	}

	// set the final motor speeds; these will be clamped to the range 0-255
	setMotor1Speed(motor1Speed);
	setMotor2Speed(motor2Speed);
	setMotor3Speed(motor3Speed);
	setMotor4Speed(motor4Speed);
}

void handleIMUTest(uint16_t dt)
{
	uint8_t data[4];
	uint8_t pitchByte;
	uint8_t rollByte;
	uint8_t yawRateByte;

	// only transmit periodically
	if(reportingIMUState)
	{
		imuStateUS += dt;
		while(imuStateUS >= 1000)
		{
			imuStateUS -= 1000;
			imuStateMS ++;
			if(imuStateMS >= IMU_TRANSMIT_MS)
			{
				imuStateUS = 0;
				imuStateMS = 0;

				// massage our data
				pitchByte = (uint8_t)(clampf((pitch + pitchTrim), -127, 127) + 127);
				rollByte = (uint8_t)(clampf((roll + rollTrim), -127, 127) + 127);
				yawRateByte = (uint8_t)(clampf(yawRate, -127, 127) + 127);

				// build our data packet
				data[0] = MSG_IMU_STATE;
				data[1] = pitchByte;
				data[2] = rollByte;
				data[3] = yawRateByte;

				// send the packet
				rfm.receiveDone();
				//while(!rfm.canSend());
				rfm.send(REMOTE_ID, (const void*)&data, 4);

				// indicate transmission is on its way
				ledFlashFast(1);
			}
		}
	}
}

void calibrateIMU()
{
	int16_t ax, ay, az;
	int16_t i;

	// running angle sums for average
	float pitchAngle = 0.0;
	float rollAngle = 0.0;
	float accelZ = 0.0;

	// short delay with warning light in case user is man-handling the damn thing
	ledOn();
	_delay_ms(500);

	// take IMU samples
	for(i = 0; i < NUM_IMU_CALIBRATION_STEPS; ++ i)
	{
		// sample accelerometer values and add to running total
		_delay_ms(10);
		mpu6050.getAcceleration(&ax, &ay, &az);
		pitchAngle += (float)atan2(ay, az) * 180.0 / M_PI;
		rollAngle += (float)atan2(ax, az) * 180.0 / M_PI;
		accelZ += (float)az;

		// occasional flash
		if(i % 10 == 9)
		{
			ledFlashFast(1);
		}
	}

	// compute average that will become our zero position
	pitchCenter = pitchAngle / (float)NUM_IMU_CALIBRATION_STEPS;
	rollCenter = rollAngle / (float)NUM_IMU_CALIBRATION_STEPS;
	accelZCenter = accelZ / (float)NUM_IMU_CALIBRATION_STEPS;

	// save to EEPROM
	saveCalibrationParamsToEEPROM();
}

// - - - utility functions - - - //

void loadIDFromEEPROM()
{
	// read the ID we have stored
	id = eeprom_read_byte((uint8_t*)EEPROM_ID_ADDRESS);

	// if the ID is 255 (0xFF is the default value for unprogrammed EEPROM), set it to zero
	if(id == 0xff)
	{
		id = 0;
	}
}

void saveIDToEEPROM()
{
	eeprom_write_byte((uint8_t*)EEPROM_ID_ADDRESS, id);
}

void loadParamsFromEEPROM()
{
	uint8_t valuesTemp[64];
	uint16_t address = EEPROM_PARAMS_ADDRESS;

	// read params from EEPROM
	eeprom_read_block((void*)valuesTemp, (const void*)address, NUM_LEVELING_PID_BYTES);				address += NUM_LEVELING_PID_BYTES;
	copyParamsFrom(DATA_PARAMS_LEVELING, valuesTemp);
	eeprom_read_block((void*)valuesTemp, (const void*)address, NUM_ROTATION_RATE_PID_BYTES);		address += NUM_ROTATION_RATE_PID_BYTES;
	copyParamsFrom(DATA_PARAMS_ROTATION_RATE, valuesTemp);
	eeprom_read_block((void*)valuesTemp, (const void*)address, NUM_RESPONSE_AND_TRIM_BYTES);		address += NUM_RESPONSE_AND_TRIM_BYTES;
	copyParamsFrom(DATA_PARAMS_RESPONSE_AND_TRIM, valuesTemp);
}

void saveParamsToEEPROM()
{
	uint8_t valuesTemp[64];
	uint16_t address = EEPROM_PARAMS_ADDRESS;

	// put params into array and write the whole block to EEPROM
	copyParamsTo(DATA_PARAMS_LEVELING, valuesTemp);
	eeprom_write_block((const void*)&valuesTemp[0], (void*)address, NUM_LEVELING_PID_BYTES);		address += NUM_LEVELING_PID_BYTES;
	copyParamsTo(DATA_PARAMS_ROTATION_RATE, valuesTemp);
	eeprom_write_block((const void*)&valuesTemp[0], (void*)address, NUM_ROTATION_RATE_PID_BYTES);	address += NUM_ROTATION_RATE_PID_BYTES;
	copyParamsTo(DATA_PARAMS_RESPONSE_AND_TRIM, valuesTemp);
	eeprom_write_block((const void*)&valuesTemp[0], (void*)address, NUM_RESPONSE_AND_TRIM_BYTES);	address += NUM_RESPONSE_AND_TRIM_BYTES;
}

void loadCalibrationParamsFromEEPROM()
{
	float valuesTemp[3];

	eeprom_read_block((void*)valuesTemp, (const void*)EEPROM_IMU_CALIBRATION_ADDRESS, sizeof(float) * 3);
	pitchCenter = valuesTemp[0];
	rollCenter = valuesTemp[1];
	accelZCenter = valuesTemp[2];
}

void saveCalibrationParamsToEEPROM()
{
	float valuesTemp[3];

	valuesTemp[0] = pitchCenter;
	valuesTemp[1] = rollCenter;
	valuesTemp[2] = accelZCenter;
	eeprom_write_block((const void*)&valuesTemp[0], (void*)EEPROM_IMU_CALIBRATION_ADDRESS, sizeof(float) * 3);
}

void resetPIDControllers()
{
	pitchPID.reset();
	rollPID.reset();
	pitchRatePID.reset();
	rollRatePID.reset();
	yawRatePID.reset();
}

void resetOrientation()
{
	pitch = 0.0;
	roll = 0.0;
	pitchRate = 0.0;
	rollRate = 0.0;
	yawRate = 0.0;
}

void copyParamsFrom(uint8_t paramGroup, uint8_t *bytes)
{
	float *ptrFloat = (float*)bytes;

	if(paramGroup == DATA_PARAMS_LEVELING)
	{
		pitchPID.pGain = *(ptrFloat++);
		pitchPID.iGain = *(ptrFloat++);
		pitchPID.dGain = *(ptrFloat++);
		pitchPID.iLimit = *(ptrFloat++);
		rollPID.pGain = *(ptrFloat++);
		rollPID.iGain = *(ptrFloat++);
		rollPID.dGain = *(ptrFloat++);
		rollPID.iLimit = *(ptrFloat++);
	}
	else if(paramGroup == DATA_PARAMS_ROTATION_RATE)
	{
		pitchRatePID.pGain = *(ptrFloat++);
		pitchRatePID.iGain = *(ptrFloat++);
		pitchRatePID.dGain = *(ptrFloat++);
		pitchRatePID.iLimit = *(ptrFloat++);
		rollRatePID.pGain = *(ptrFloat++);
		rollRatePID.iGain = *(ptrFloat++);
		rollRatePID.dGain = *(ptrFloat++);
		rollRatePID.iLimit = *(ptrFloat++);
		yawRatePID.pGain = *(ptrFloat++);
		yawRatePID.iGain = *(ptrFloat++);
		yawRatePID.dGain = *(ptrFloat++);
		yawRatePID.iLimit = *(ptrFloat++);
	}
	else if(paramGroup == DATA_PARAMS_RESPONSE_AND_TRIM)
	{
		pitchDemandGain = *(ptrFloat++);
		rollDemandGain = *(ptrFloat++);
		yawDemandGain = *(ptrFloat++);
		pitchTrim = *(ptrFloat++);
		rollTrim = *(ptrFloat++);
		compFilterAlpha = *(ptrFloat++);
		oneMinusCompFilterAlpha = 1.0 - compFilterAlpha;
	}
}

void copyParamsTo(uint8_t paramGroup, uint8_t *bytes)
{
	float *ptrFloat;
	uint32_t *ptrUint32;
	uint16_t *ptrUint16;

	if(paramGroup == DATA_PARAMS_LEVELING)
	{
		ptrFloat = (float*)bytes;
		(*ptrFloat++) = pitchPID.pGain;
		(*ptrFloat++) = pitchPID.iGain;
		(*ptrFloat++) = pitchPID.dGain;
		(*ptrFloat++) = pitchPID.iLimit;
		(*ptrFloat++) = rollPID.pGain;
		(*ptrFloat++) = rollPID.iGain;
		(*ptrFloat++) = rollPID.dGain;
		(*ptrFloat++) = rollPID.iLimit;
	}
	else if(paramGroup == DATA_PARAMS_ROTATION_RATE)
	{
		ptrFloat = (float*)bytes;
		(*ptrFloat++) = pitchRatePID.pGain;
		(*ptrFloat++) = pitchRatePID.iGain;
		(*ptrFloat++) = pitchRatePID.dGain;
		(*ptrFloat++) = pitchRatePID.iLimit;
		(*ptrFloat++) = rollRatePID.pGain;
		(*ptrFloat++) = rollRatePID.iGain;
		(*ptrFloat++) = rollRatePID.dGain;
		(*ptrFloat++) = rollRatePID.iLimit;
		(*ptrFloat++) = yawRatePID.pGain;
		(*ptrFloat++) = yawRatePID.iGain;
		(*ptrFloat++) = yawRatePID.dGain;
		(*ptrFloat++) = yawRatePID.iLimit;
	}
	else if(paramGroup == DATA_PARAMS_RESPONSE_AND_TRIM)
	{
		ptrFloat = (float*)bytes;
		(*ptrFloat++) = pitchDemandGain;
		(*ptrFloat++) = rollDemandGain;
		(*ptrFloat++) = yawDemandGain;
		(*ptrFloat++) = pitchTrim;
		(*ptrFloat++) = rollTrim;
		(*ptrFloat++) = compFilterAlpha;
	}
	else if(paramGroup == DATA_STATUS)
	{
		*(bytes++) = id;								// 0
		ptrFloat = (float*)bytes;						// switch to float
		*(ptrFloat++) = (pitch + pitchTrim);			// 1
		*(ptrFloat++) = (roll + rollTrim);				// 5
		ptrUint32 = (uint32_t*)ptrFloat;				// switch to uint32_t
		*(ptrUint32++) = getBatteryMillivolts();		// 9
		ptrUint16 = (uint16_t*)ptrUint32;				// switch to uint16_t
		*(ptrUint16++) = dt;							// 13
	}
}

float clampf(float value, float min, float max)
{
	if     (value < min) value = min;
	else if(value > max) value = max;
	return value;
}

// atan2 approximation function adapted from:
// https://gist.github.com/volkansalma/2972237
float atan2Approx(float y, float x)
{
	// http://pubs.opengroup.org/onlinepubs/009695399/functions/atan2.html
	// Volkan SALMA

	float r, angle;
	float abs_y = fabs(y);

	// prevent 0/0 condition
	if(abs_y == 0.0) abs_y = 0.000001;

	if(x < 0.0f)
	{
		r = (x + abs_y) / (abs_y - x);
		angle = THREE_QTR_PI;
	}
	else
	{
		r = (x - abs_y) / (x + abs_y);
		angle = ONE_QTR_PI;
	}

	angle += (0.1963f * r * r - 0.9817f) * r;
	if(y < 0.0f)
		return(-angle);     // negate if in quad III or IV
	else
		return(angle);
}
