#pragma once

#include <avr/io.h>

void initLED();
void ledFlashSlow(uint8_t times);
void ledFlashFast(uint8_t times);
void ledOn();
void ledOff();
void led(bool on);
