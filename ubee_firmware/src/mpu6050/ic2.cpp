#include "i2c.h"
#include <avr/io.h>

#define BDIV ((F_CPU / 100000 - 16) / 2) + 1

void i2cInit()
{
	TWSR = 0;			// use clock prescalar of 1
	TWBR = BDIV;		// set the bit rate register
}

uint8_t i2cWrite(uint8_t device, uint8_t *data, uint16_t n)
{
	uint8_t status;

	// send start condition
	TWCR = (1 << TWINT ) | (1 << TWEN ) | (1 << TWSTA );

	// wait for TWINT to be sent
	while (!( TWCR & (1 << TWINT )));
	
	// check that START was sent okay
	status = TWSR & 0xf8;
	if(status != 0x08)
		return status;

	TWDR = device & 0xfe ;						// load device address and R / W = 0;
	TWCR = (1 << TWINT ) | (1 << TWEN );		// start transmission
	while (!( TWCR & (1 << TWINT )));			// wait for TWINT to be set
	status = TWSR & 0xf8;
	if(status != 0x18)							// check that SLA + W was sent OK
		return status;

	// write n data bytes to the slave device
	while (n-- > 0)
	{
		TWDR = *data++;							// put next data byte in TWDR
		TWCR = (1 << TWINT ) | (1 << TWEN );	// start transmission
		while (!( TWCR & (1 << TWINT )));		// wait for TWINT to be set
		status = TWSR & 0xf8;
		if(status != 0x28)						// check that data was sent OK
			return status;
	}

	// send stop condition
	TWCR = (1 << TWINT ) | (1 << TWEN ) | (1 << TWSTO );

	// everything went okay
	return 0;
}

uint8_t i2cRead(uint8_t device, char *data, uint16_t n, uint16_t address)
{
	uint8_t status ;

	// send start condition
	TWCR = (1 << TWINT ) | (1 << TWEN ) | (1 << TWSTA );

	// wait for TWINT to be sent
	while (!( TWCR & (1 << TWINT )));

	// check thst START was sent okay
	status = TWSR & 0xf8;
	if(status != 0x08)
		return status;

	TWDR = device & 0xfe;						// load device address and R / W = 0;
	TWCR = (1 << TWINT ) | (1 << TWEN );		// start transmission
	while (!( TWCR & (1 << TWINT )));			// wait for TWINT to be set
	status = TWSR & 0xf8 ;
	if(status != 0x18)							// check that SLA + W was sent OK
		return status;

	TWDR = address >> 8;						// load high byte of address
	TWCR = (1 << TWINT ) | (1 << TWEN );		// start transmission
	while (!( TWCR & (1 << TWINT )));			// wait for TWINT to be set
	status = TWSR & 0xf8;
	if ( status != 0x28 )						// check that data was sent OK
		return status;

	TWDR = address & 0xff;						// load low byte of address
	TWCR = (1 << TWINT ) | (1 << TWEN );		// start transmission
	while (!( TWCR & (1 << TWINT )));			// wait for TWINT to be set
	status = TWSR & 0xf8;
	if(status != 0x28)							// check that data was sent OK
		return status;

	// put TWI into Master Receive mode by sending a repeated START condition
	TWCR = (1 << TWINT ) | (1 << TWEN ) | (1 << TWSTA );

	// send repeated start condition
	while (!( TWCR & (1 << TWINT )));			// wait for TWINT to be set
	status = TWSR & 0xf8;
	if(status != 0x10)							// check that repeated START sent OK
		return status;

	TWDR = device | 0x01;						// load device address and R / W = 1;
	TWCR = (1 << TWINT ) | (1 << TWEN );		// send address + r
	while (!( TWCR & (1 << TWINT )));			// wait for TWINT to be set
	status = TWSR & 0xf8;
	if(status != 0x40)							// check that SLA + R was sent OK
		return status;

	// read all but the last of n bytes from the slave device in this loop
	n --;
	while(n -- > 0)
	{
		TWCR = (1 << TWINT ) | (1 << TWEN ) | (1 << TWEA );		// read byte and send ACK
		while (!( TWCR & (1 << TWINT )));						// wait for TWINT to be set
		status = TWSR & 0xf8;
		if(status != 0x50)										// check that data received OK
			return status;
		*data++ = TWDR ;										// read the data
	}

	// Read the last byte
	TWCR = (1 << TWINT ) | (1 << TWEN );		// read last byte with NOT ACK sent
	while (!( TWCR & (1 << TWINT )));			// wait for TWINT to be set
	status = TWSR & 0xf8;
	if(status != 0x58)							// check that data received OK
		return status;

	// put the data into the buffer and send the STOP condition
	*data++ = TWDR;
	TWCR = (1 << TWINT ) | (1 << TWEN ) | (1 << TWSTO );
	return 0;
}
