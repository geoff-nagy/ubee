#pragma once

#include <avr/io.h>

void initBatteryMonitor();
void handleBatteryMonitor(uint16_t dt);

uint16_t getBatteryMillivolts();
bool isBatteryLow();