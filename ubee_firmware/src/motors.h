#pragma once

#include <avr/io.h>

void initMotors();

void setMotor1Speed(int16_t speed);
void setMotor2Speed(int16_t speed);
void setMotor3Speed(int16_t speed);
void setMotor4Speed(int16_t speed);
