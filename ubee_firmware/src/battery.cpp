#include "battery.h"
#include "led.h"

#define MONITOR_PORT PORTD
#define MONITOR_DIR DDRD
#define MONITOR_INPUT PIND
#define MONITOR_PIN 0

#define DIVIDER_R1 100L								// KOhms, not like it matters here; we just include these for max flexibility in
#define DIVIDER_R2 300L								// case our resistor values change at all in the future

#define REFERENCE_VOLTAGE_MV 3319L					// we provide externally regulated 3.3V (measured to be about 3.319V)
#define BATTERY_MIN_MV 3800L						// millivolt level at which battery warning goes off

#define CHECK_BATTERY_SECS 3						// check the battery level this often
#define FLASH_ON_TIME_MS 100						// low-battery flash LED is on for this length of time every second

static uint32_t batteryMillivolts = 0;
static bool lowBattery = 0;

static uint16_t usecs = 0;
static uint16_t msecs = 0;
static uint16_t secs = CHECK_BATTERY_SECS;			// check immediately

static uint16_t readVoltageMillivolts();
static void controlFlashing();

void initBatteryMonitor()
{
	// monitor as input with internal pull-up
	MONITOR_DIR &= ~(1 << MONITOR_PIN);
	MONITOR_PORT |= (1 << MONITOR_PIN);
	
	// enable ADC
	ADCSRA |= (1 << ADEN);												// turn the sucker on
	ADMUX &= ~((1 << REFS0) | (1 << REFS1));							// use external ARef pin voltage as reference
	
	// select battery input channel---this assumes no other code needs the ADC
	ADMUX &= 0xf0;															// clear channel-select bits; results in selecting ADC0
	
	// default values for now until we do an ADC read
	batteryMillivolts = 0;
	lowBattery = false;
}

void handleBatteryMonitor(uint16_t dt)
{
	// advance timer
	usecs += dt;
	while(usecs >= 1000)
	{
		msecs ++;
		usecs -= 1000;
		if(msecs >= 1000)
		{
			secs ++;
			msecs -= 1000;
		}
	}
		
	// time to check voltage?
	if(secs > CHECK_BATTERY_SECS)
	{
		secs = 0;
		lowBattery = (batteryMillivolts = readVoltageMillivolts()) < BATTERY_MIN_MV;
	}
	
	// flash LED if battery is low
	if(lowBattery)
	{
		controlFlashing();
	}
}

uint16_t readVoltageMillivolts()
{
	uint32_t adc;
	uint32_t divided;
	uint32_t actual;
	
	// start the conversion, wait for completion, read result, trigger a hardware-level clear of conversion complete bit
	ADCSRA |= (1 << ADSC);
	while(ADCSRA & (1 << ADSC)) { }
	adc = ADCW;
	ADCSRA |= (1 << ADIF);
	
	// convert from ADC to divided voltage
	divided = (adc * REFERENCE_VOLTAGE_MV) / 1024;
	
	// convert from divided to actual
	actual = (divided * (DIVIDER_R1 + DIVIDER_R2)) / DIVIDER_R2;
	
	// convert to millivolts in the range we expect
	return actual;
}

void controlFlashing()
{
	led(msecs <= FLASH_ON_TIME_MS);
}

uint16_t getBatteryMillivolts()
{
	return batteryMillivolts;
}

bool isBatteryLow()
{
	return lowBattery;
}
