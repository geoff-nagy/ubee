#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#define DIVIDER_R1 100L						// x100 KOhms, not like it matters here; we just include these for max flexibility in
#define DIVIDER_R2 300L						// case our resistor values change at all in the future

#define REFERENCE_VOLTAGE 3300L

#define BATTERY_MAX_MV 4200L
#define BATTERY_MIN_MV 3700L

uint32_t divideVoltage(uint32_t in, uint32_t r1, uint32_t r2)
{
	return (in * r2) / (r1 + r2);
}

uint32_t convertADCToVoltage(uint32_t value, uint32_t minADCToVoltage, uint32_t maxADCToVoltage, uint32_t minVoltage, uint32_t maxVoltage)
{
	return minVoltage + (value / maxADCToVoltage) * (maxVoltage - minVoltage);
}

int main(int args, char *argv[])
{
	// compute the min and max voltage values we could get based on the voltage divider values we're using
	//uint32_t minDividedVoltage = divideVoltage(BATTERY_MIN_MV, DIVIDER_R1, DIVIDER_R2);
	//uint32_t maxDividedVoltage = divideVoltage(BATTERY_MAX_MV, DIVIDER_R1, DIVIDER_R2);

	// now compute the min and max ADC values we could get from those
	//uint32_t minADCReading = (minDividedVoltage * 1024) / REFERENCE_VOLTAGE;
	//uint32_t maxADCReading = (maxDividedVoltage * 1024) / REFERENCE_VOLTAGE;

	//uint16_t minADCMV = minADCReading / 0.75f;//BATTERY_MIN_MV + (minADCReading / maxADCReading) * (BATTERY_MAX_MV - BATTERY_MIN_MV);
	//uint16_t maxADCMV = maxADCReading / 0.75f;//BATTERY_MIN_MV + (maxADCReading / maxADCReading) * (BATTERY_MAX_MV - BATTERY_MIN_MV);

	uint32_t readingADC = 1023;
	uint32_t readingMV = (readingADC * REFERENCE_VOLTAGE) / 1024;
	uint32_t readingCD = (readingMV * (DIVIDER_R1 + DIVIDER_R2)) / DIVIDER_R2;

	printf("reading ADC: %d\n", readingADC);
	printf("reading MV:  %d\n", readingMV);
	printf("reading CD:  %d\n", readingCD);

/*
	const int R1 = 100000;
	const int R2 = 300000;

	const int MIN_VOLTAGE = 3600;
	const int MAX_VOLTAGE = 4200;

	const int MIN_DIVIDED_VOLTAGE = divideVoltage(MIN_VOLTAGE, R1, R2);
	const int MAX_DIVIDED_VOLTAGE = divideVoltage(MAX_VOLTAGE, R1, R2);

	const int ADC_REF_VOLTAGE = 3300;

	const int ADC_MIN_READING = (MIN_DIVIDED_VOLTAGE * 1024) / ADC_REF_VOLTAGE;
	const int ADC_MAX_READING = (MAX_DIVIDED_VOLTAGE * 1024) / ADC_REF_VOLTAGE;

	const int MIN_ADC_TO_VOLTAGE = convertADCToVoltage(ADC_MIN_READING, ADC_MIN_READING, ADC_MAX_READING, MIN_VOLTAGE, MAX_VOLTAGE);
	const int MAX_ADC_TO_VOLTAGE = convertADCToVoltage(ADC_MAX_READING, ADC_MIN_READING, ADC_MAX_READING, MIN_VOLTAGE, MAX_VOLTAGE);

	printf("Resistor 1 value:      %d\n", R1);
	printf("Resistor 2 value:      %d\n", R2);

	printf("Min real voltage:      %d\n", MIN_VOLTAGE);
	printf("Max real voltage:      %d\n", MAX_VOLTAGE);

	printf("Min divided voltage:   %d\n", MIN_DIVIDED_VOLTAGE);
	printf("Max divided voltage:   %d\n", MAX_DIVIDED_VOLTAGE);

	printf("Min adc reading:       %d\n", ADC_MIN_READING);
	printf("Max adc reading:       %d\n", ADC_MAX_READING);

	printf("Min adc-to-voltage:    %d\n", MIN_ADC_TO_VOLTAGE);
	printf("Max adc-to-voltage:    %d\n", MAX_ADC_TO_VOLTAGE);
*/
	return 0;
}
