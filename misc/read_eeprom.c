#include <stdio.h>
#include <stdlib.h>

int main(int args, char *argv[])
{
	const int EEPROM_1_SIZE = 2893;
	const int EEPROM_2_SIZE = 2893;

	FILE *eep1 = fopen("mqc_eeprom.eep", "rb");
	FILE *eep2 = fopen("mqc_eeprom_2.eep", "rb");

	unsigned char contents1[2893];
	unsigned char contents2[2893];
	int i;

	fgets((char*)contents1, EEPROM_1_SIZE, eep1);
	fgets((char*)contents2, EEPROM_2_SIZE, eep2);

	printf("EEPROM 1 contents: \n");
	printf("------------------\n");
	for(i = 0; i < EEPROM_1_SIZE; ++ i)
	{
		printf("[%2d]: %d\n", i, (int)contents1[i]);
	}

	printf("EEPROM 2 contents: \n");
	printf("------------------\n");
	for(i = 0; i < EEPROM_2_SIZE; ++ i)
	{
		printf("[%2d]: %d\n", i, (int)contents2[i]);
	}

	return 0;
}
