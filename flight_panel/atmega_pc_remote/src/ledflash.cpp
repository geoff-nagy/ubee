#include "ledflash.h"

#include <avr/io.h>
#include <util/delay.h>

#define LED_PORT PORTC
#define LED_DIR DDRC
#define LED_PIN 3

#define ON_TIME_MS 20
#define OFF_TIME_MS 40

void ledInit()
{
	LED_PORT &= ~(1 << LED_PIN);
	LED_DIR |= (1 << LED_PIN);
}

void ledFlash(uint8_t times)
{
	while(times--)
	{
		LED_PORT |= (1 << LED_PIN);
		_delay_ms(ON_TIME_MS);
		LED_PORT &= ~(1 << LED_PIN);
		_delay_ms(OFF_TIME_MS);
	}
}
