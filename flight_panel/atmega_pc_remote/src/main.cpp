#include "OrangutanSerial.h"

#include "ledflash.h"
#include "rfm69/RFM69.h"

#include <avr/io.h>
#include <util/delay.h>

// - - - wireless config settings - - - //

// common across MQC and remote control
#define FREQUENCY RF69_915MHZ
#define NETWORK_ID 45

// IDs are device-specific
#define REMOTE_ID 43												// ID of remote control
#define MQC_ID 44													// ID of MQC device

// defines how TX retries are handled, in the event the remote wants data back from us
#define NUM_RETRIES 10
#define RETRY_WAIT_MS 20

// - - - messaging settings - - - //

// generic message format:

// format       : [header, 2 bytes]  |  [ ... data ... ]
// byte index   :    0         1     |     2      [...]                 (len - 2)
// byte meaning : [ cmd ]   [ len ]  |  [ id ]    [ ...command-specific data... ]

// "cmd" is one of the command bytes below
// "len" is the remaining length of the message in bytes, after (and not including) this "len" byte
// "id" is either a unicast ID [0-254] referring to a single drone or the broadcast ID 255

// this means that every command will have a "len" byte of at least 1, containing at least the ID byte

// each message begins with a command and a length
#define MSG_HEADER_SIZE 2

// command bytes
#define MSG_FLASH_LED 1						// instruct drone to flash its LED
#define MSG_SET_ID 2						// instruct drone to change its ID
#define MSG_SET_PARAMS 3					// instruct drone to change a set of parameters
#define MSG_REQUEST_DATA 4					// instruct drone to return some data
#define MSG_SINGLE_FLIGHT_COMMAND 5			// send flight control command (pitch, roll, yaw)
#define MSG_COMPOUND_FLIGHT_COMMAND 6		// send multiple different flight control commands to multiple ID'd drones
#define MSG_ACK 7							// generic acknowledgment
#define MSG_NAK 8							// generic non-acknowledgment
#define MSG_RECALIBRATE_IMU 9				// instruct drone to recalibrate its IMU
#define MSG_START_IMU_TEST 10				// instruct drone to start continuous transmission of IMU values
#define MSG_END_IMU_TEST 11					// instruct drone to stop continuous transmission of IMU values
#define MSG_IMU_STATE 12					// this is a special message from the drone containing IMU state
#define MSG_DATA_PACKET 13					// some data packet sent by the drone

// these denote groups of data that can be requested from the uBee; various flight parameters, current IMU reading, and generic uBee status
#define DATA_PARAMS_LEVELING 0
#define DATA_PARAMS_ROTATION_RATE 1
#define DATA_PARAMS_RESPONSE_AND_TRIM 2
#define DATA_STATUS 3

// different parameter group lengths; relevant to MSG_{REQUEST, SET}_PARAMS
#define NUM_LEVELING_PID_BYTES 32
#define NUM_ROTATION_RATE_PID_BYTES 48
#define NUM_RESPONSE_AND_TRIM_BYTES 24
#define NUM_STATUS_BYTES 15
#define NUM_IMU_BYTES 3

// - - - misc debugging/visual info settings - - - //

// number of times to flash LED for certain events
#define LED_NUM_FLASHES_INIT 4										// after successful initialization
#define LED_NUM_FLASHES_CONFIG_COMMAND 2							// after receiving a non-[dis]arm configuration command
//#define LED_NUM_FLASHES_ARM_COMMAND 6								// after receiving an arm or disarm command
#define LED_NUM_FLASHES_ERROR 10									// after some kind of error occurs

// - - - communication settings - - - //

#define SERIAL_BAUD_RATE 38400 //57600 //115200 //9600										// baud rate of serial communication with PC
#define MESSAGE_CONTENTS_WAIT_TIMEOUT 200							// how long to wait in milliseconds when waiting for incoming data to finish coming in from the PC
#define RECEIVE_UBEE_DATA_INITIAL_WAIT_TIMEOUT 200					// how long to wait in milliseconds when waiting for incoming data to *start* coming in from the uBee
#define RECEIVE_UBEE_DATA_REMAINING_WAIT_TIMEOUT 1000				// how long to wait in milliseconds when waiting for incoming data to *finish* coming in from the uBee

// - - - global vars - - - //

bool reportingIMUState = false;										// are we taking radio-received IMU bytes and forwarding them to the PC?

// - - - main function - - - //

int main(void)
{
	RFM69 rfm;
	uint8_t msgType;
	uint8_t msgLen;
	//uint8_t msgID;
	uint8_t data[255];
	bool dataReceived;
	unsigned long startTime;

	// initialize Arduino library
	init();

	// initialize LED output settings
	ledInit();

	// initialize the RF transceiver
	rfm.initialize(FREQUENCY, REMOTE_ID, NETWORK_ID);
	rfm.setHighPower(true);
	rfm.setPowerLevel(31);

	// initialize serial settings
	serial_set_baud_rate(SERIAL_BAUD_RATE);

	// successful initialization
	ledFlash(LED_NUM_FLASHES_INIT);

	// try to receive a message asynchronously from the PC---note that this resets the input buffer
	serial_cancel_receive();
	serial_receive((char*)&data[0], 255);

	// main control loop
	while(true)
	{
		// see if we've received something; we expect at least a command type and an ID
		if(serial_get_received_bytes() >= MSG_HEADER_SIZE)
		{
			// determine what kind of message this is, how long it is, and to whom it's addressed
			msgType = data[0];
			msgLen = data[1];
			
			// wait for the remaining data; there should be at least 1 byte (the ID byte)
			startTime = millis();
			while(!(dataReceived = serial_get_received_bytes() >= MSG_HEADER_SIZE + msgLen) && millis() - startTime < MESSAGE_CONTENTS_WAIT_TIMEOUT);

			// if we got the data in time, process it
			if(dataReceived)
			{
				// if this was a request for data, then we don't bother with retries; we need to listen for the data right away that should be coming back to us from the uBee
				if(msgType == MSG_REQUEST_DATA)
				{
					// flash the LED
					ledFlash(LED_NUM_FLASHES_CONFIG_COMMAND);

					// just attempt to send the data once, and then start listening
					rfm.send(MQC_ID, data, MSG_HEADER_SIZE + msgLen);
					rfm.receiveDone();

					// wait for the data to *start* arriving---we can't check right away for completion via rfm.receiveDone() yet because if we do, it may indicate that the reception is
					// (erroneously) complete if the uBee has actually not started to send its response via radio yet
					startTime = millis();
					while(millis() - startTime < RECEIVE_UBEE_DATA_INITIAL_WAIT_TIMEOUT);

					// after that initial delay, wait for the remaining data to come in; at this point it's safe to check if the data has been received via rfm.receiveDone();
					// this loop exits if either (a) we receive it all or (b) the timeout expired
					startTime = millis();
					while(!(dataReceived = rfm.receiveDone()) && millis() - startTime < RECEIVE_UBEE_DATA_REMAINING_WAIT_TIMEOUT);

					// check if we received something during our wait
					if(dataReceived)
					{
						// ACK the MQC's data if an ACK was requested
						if(rfm.ACKRequested())
						{
							rfm.sendACK();
						}

						// we got all the data, so send an ACK followed by the data requested to the PC
						serial_send_blocking((char*)&rfm.DATA[0], 2);					// send header (message and len)
						serial_send_blocking((char*)&rfm.DATA[2], rfm.DATA[1]);			// send ID and data that was received
					}
					else
					{
						// didn't get anything in time, so send a NAK
						ledFlash(LED_NUM_FLASHES_ERROR);
						msgType = MSG_NAK;
						serial_send_blocking((char*)&msgType, 1);
					}
				}
				else
				{
					// flight commands are expected to be quick and frequent, so don't flash the LED or request an ACK if that's what we're sending
					if(msgType != MSG_SINGLE_FLIGHT_COMMAND && msgType != MSG_COMPOUND_FLIGHT_COMMAND)
					{
						//serial_cancel_receive();
						//serial_receive((char*)&data[0], 128);

						// this was not a flight command, so we flash the LED and request an ACK
						rfm.sendWithRetry(MQC_ID, data, MSG_HEADER_SIZE + msgLen, NUM_RETRIES, RETRY_WAIT_MS);
						ledFlash(LED_NUM_FLASHES_CONFIG_COMMAND);
					}
					else
					{
						// this is some kind of flight command; we do not need to request ACKs for this
						rfm.send(MQC_ID, data, MSG_HEADER_SIZE + msgLen);
					}

					// special case: we're requesting continuous IMU data
					if(msgType == MSG_START_IMU_TEST)
					{
						// send the start IMU test instruction to the MQC
						//rfm.sendWithRetry(MQC_ID, data, MSG_HEADER_SIZE, NUM_RETRIES, RETRY_WAIT_MS);
						//ledFlash(LED_NUM_FLASHES_CONFIG_COMMAND);
						reportingIMUState = true;
					}
					else if(msgType == MSG_END_IMU_TEST)
					{
						// send the stop IMU test instruction to the MQC
						//rfm.sendWithRetry(MQC_ID, data, MSG_HEADER_SIZE, NUM_RETRIES, RETRY_WAIT_MS);
						//ledFlash(LED_NUM_FLASHES_CONFIG_COMMAND);
						reportingIMUState = false;
					}
				}
			}

			// we're done with whatever message we got, so reset the input buffer
			serial_cancel_receive();
			serial_receive((char*)&data[0], 255);
		}

		// should we be on the lookout for IMU values?
		if(reportingIMUState)
		{
			// check if we received something in the meantime
			if(rfm.receiveDone())
			{
				// forward IMU bytes over serial
				if(rfm.DATA[0] == MSG_IMU_STATE)
				{
					serial_send_blocking((char*)&rfm.DATA[0], NUM_IMU_BYTES + 1);
					ledFlash(1);
				}
			}
		}
	}

	// good practice to have at end of main()
	while(true);
}
