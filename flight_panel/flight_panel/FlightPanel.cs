﻿using System;
using System.Windows.Forms;
using System.IO.Ports;
using SharpDX.DirectInput;
using System.IO;

namespace PCRemote
{
	public partial class FlightPanel : Form
    {
		enum ThrustMode
		{
			THRUST_MODE_DOWN = 0,
			THRUST_MODE_HOVER,
			THRUST_MODE_UP
		}

		private const string VERSION_INFO = "0.9.6";

		private const string PARAMS_FILENAME = "params.txt";

		// instruct the drone to flash its LED
		// [MSG_FLASH_LED] [ID]
        private const byte MSG_FLASH_LED = 1;					// instruct drone to flash its LED
		
		// instruct the drone to change its ID
		// [MSG_SET_ID] [OLD ID] [NEW ID]
		private const byte MSG_SET_ID = 2;						// instruct drone to change its ID

		// instruct the drone to change a set of parameters
		// [MSG_SET_PARAMS] [ID] [PARAMS_XXXX] [NUM_XXXX_BYTES] [param0] [param1] ... [paramN - 1]
		private const byte MSG_SET_PARAMS = 3;

		// instruct the drone to return a set of parameters
		// [MSG_REQUEST_PARAMS] [ID] [PARAMS_XXXX]
		private const byte MSG_REQUEST_PARAMS = 4;

		private const byte MSG_SINGLE_FLIGHT_COMMAND = 5;		// send flight control command (pitch, roll, yaw)
		private const byte MSG_COMPOUND_FLIGHT_COMMAND = 6;		// send multiple different flight control commands to multiple ID'd drones
		private const byte MSG_ACK = 7;							// generic acknowledgment
		private const byte MSG_NAK = 8;							// generic non-acknowledgment
		private const byte MSG_RECALIBRATE_IMU = 9;				// instruct drone to recalibrate its IMU
		private const byte MSG_START_IMU_TEST = 10;				// instruct drone to start transmission of IMU values
		private const byte MSG_END_IMU_TEST = 11;				// instruct drone to end transmission of IMU values
		private const byte MSG_IMU_STATE = 12;					// device is reporting its IMU state as part of a continuous test
		private const byte MSG_DATA_PACKET = 13;				// device is reporting some data packet that we requested

		// different parameter groups that can be sent or requested via MSG_{REQUEST, SET}_PARAMS
		private const int PARAMS_LEVELING = 0;
		private const int PARAMS_ROTATION_RATE = 1;
		private const int PARAMS_RESPONSE_AND_TRIM = 2;
		private const int PARAMS_STATUS = 3;

		// different parameter group lengths; relevant to MSG_{REQUEST, SET}_PARAMS
		private const int NUM_LEVELING_PID_BYTES = (8 * 4);
		private const int NUM_ROTATION_RATE_PID_BYTES = (12 * 4);
		private const int NUM_RESPONSE_AND_TRIM_BYTES = (6 * 4);
		private const int NUM_STATUS_BYTES = 15;
		private const int NUM_IMU_BYTES = 3;					// how many bytes are needed to store continuous IMU plot values

		private const byte BROADCAST_ID = 255;					// used to address all drones

        private Joystick joystick = null;
        private SerialPort serialPort = null;

		// controller values are bytes so we can easily transmit them
		private ThrustMode thrustMode;
		private byte thrust = 0;
		private byte forward = 0;
		private byte strafe = 0;
		private byte yaw = 0;

		// parameter loading
		private String fullParamsFilename;
		private String shortParamsFilename;

		// for IMU value plotting
		private int imuTimeIndex = 0;

		private bool flightCommandsEnabled = true;

        public FlightPanel()
        {
            InitializeComponent();

			// no parameter file is open
			fullParamsFilename = null;
			EnableOrDisableParamFileButtons();
			UpdateFormTitleWithParamsFile();

			// can't do anything until a connection is made
			grpCommands.Enabled = false;
        }

		private double GetSeconds()
		{
			return (DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds;
		}

		private void RequestParams(int paramGroup, int numBytesToReceive)
		{
			byte[] received;
			byte response;
			double startTime = GetSeconds();
			int deltaTimeStep;
			int byteIndex = 0;

			// make sure ID is valid
			int id = GetOutgoingDroneID();
			if(id >= 0)
			{
				// write a message requesting flight params, and wait for a response
				flightCommandsEnabled = false;
				tmrInputTick.Enabled = false;
				Log("Requesting " + numBytesToReceive + " param bytes from " + GetDroneString(id) + "...");
				byte[] message = {MSG_REQUEST_PARAMS, 3, (byte)id, (byte)paramGroup, (byte)numBytesToReceive};
				serialPort.ReadExisting();
				serialPort.Write(message, 0, 5);
				while(serialPort.BytesToRead == 0 && GetSeconds() - startTime < 2.0);

				// the bridge should respond with SOMEthing...
				if(serialPort.BytesToRead > 0)
				{
					// so, read the first byte for the message type
					response = (byte)serialPort.ReadByte();
					if(response == MSG_DATA_PACKET)
					{
						// take into account header plus data packet info---note that we've already read the message type
						//numBytesToReceive += 3;

						// make sure we received enough data!
						while(serialPort.BytesToRead < numBytesToReceive + 3);

						// now copy and then store the values we received
						received = new byte[numBytesToReceive + 3];
						serialPort.Read(received, 0, numBytesToReceive + 3);

						// skip past message len, and ID, and data group, to data part of message
						byteIndex += 3;
					
						// fill in the text box values according to what param group we requested
						if(paramGroup == PARAMS_LEVELING)
						{
							numPitchLevelingGainP.Value = (decimal)BitConverter.ToSingle(received, byteIndex);	byteIndex += 4;
							numPitchLevelingGainI.Value = (decimal)BitConverter.ToSingle(received, byteIndex);	byteIndex += 4;
							numPitchLevelingGainD.Value = (decimal)BitConverter.ToSingle(received, byteIndex);	byteIndex += 4;
							numPitchLevelingILimit.Value = (decimal)BitConverter.ToSingle(received, byteIndex); byteIndex += 4;
							numRollLevelingGainP.Value = (decimal)BitConverter.ToSingle(received, byteIndex);	byteIndex += 4;
							numRollLevelingGainI.Value = (decimal)BitConverter.ToSingle(received, byteIndex);	byteIndex += 4;
							numRollLevelingGainD.Value = (decimal)BitConverter.ToSingle(received, byteIndex);	byteIndex += 4;
							numRollLevelingILimit.Value = (decimal)BitConverter.ToSingle(received, byteIndex);	byteIndex += 4;
						}
						else if(paramGroup == PARAMS_ROTATION_RATE)
						{
							numPitchRateGainP.Value = (decimal)BitConverter.ToSingle(received, byteIndex);		byteIndex += 4;
							numPitchRateGainI.Value = (decimal)BitConverter.ToSingle(received, byteIndex);		byteIndex += 4;
							numPitchRateGainD.Value = (decimal)BitConverter.ToSingle(received, byteIndex);		byteIndex += 4;
							numPitchRateILimit.Value = (decimal)BitConverter.ToSingle(received, byteIndex);		byteIndex += 4;
							numRollRateGainP.Value = (decimal)BitConverter.ToSingle(received, byteIndex);		byteIndex += 4;
							numRollRateGainI.Value = (decimal)BitConverter.ToSingle(received, byteIndex);		byteIndex += 4;
							numRollRateGainD.Value = (decimal)BitConverter.ToSingle(received, byteIndex);		byteIndex += 4;
							numRollRateILimit.Value = (decimal)BitConverter.ToSingle(received, byteIndex);		byteIndex += 4;
							numYawRateGainP.Value = (decimal)BitConverter.ToSingle(received, byteIndex);		byteIndex += 4;
							numYawRateGainI.Value = (decimal)BitConverter.ToSingle(received, byteIndex);		byteIndex += 4;
							numYawRateGainD.Value = (decimal)BitConverter.ToSingle(received, byteIndex);		byteIndex += 4;
							numYawRateILimit.Value = (decimal)BitConverter.ToSingle(received, byteIndex);		byteIndex += 4;
						}
						else if(paramGroup == PARAMS_RESPONSE_AND_TRIM)
						{
							numPitchDemandGain.Value = (decimal)BitConverter.ToSingle(received, byteIndex);		byteIndex += 4;
							numRollDemandGain.Value = (decimal)BitConverter.ToSingle(received, byteIndex);		byteIndex += 4;
							numYawDemandGain.Value = (decimal)BitConverter.ToSingle(received, byteIndex);		byteIndex += 4;
							numPitchTrim.Value = (decimal)BitConverter.ToSingle(received, byteIndex);			byteIndex += 4;
							numRollTrim.Value = (decimal)BitConverter.ToSingle(received, byteIndex);			byteIndex += 4;
							numCompFilterAlpha.Value = (decimal)BitConverter.ToSingle(received, byteIndex);		byteIndex += 4;
						}
						else if(paramGroup == PARAMS_STATUS)
						{
							txtCurrentID.Text = "" + received[byteIndex];										byteIndex += 1;
							txtCurrentPitch.Text = "" + BitConverter.ToSingle(received, byteIndex);				byteIndex += 4;
							txtCurrentRoll.Text = "" + BitConverter.ToSingle(received, byteIndex);				byteIndex += 4;
							txtCurrentBattery.Text = "" + BitConverter.ToUInt32(received, byteIndex);			byteIndex += 4;
							deltaTimeStep =	BitConverter.ToUInt16(received, byteIndex);							byteIndex += 2;
							txtCurrentDeltaTime.Text = "" + deltaTimeStep;
							txtCurrentHz.Text = "" + Math.Round(1000000.0f / deltaTimeStep);
						}

						// erase the rest
						serialPort.ReadExisting();

						// indicate success---we successfully retrieved the params from the MQC
						Log("Successfully received " + numBytesToReceive + " bytes from " + GetDroneString(id) + ".");
					}
					else if(response == MSG_NAK)
					{
						// nothing---indicate so
						Log("Bridge received nothing from " + GetDroneString(id) + ".");

						// erase the rest
						serialPort.ReadExisting();
					}
					else
					{
						// nothing sensible; erase the rest; this can happen if, e.g., we miss an IMU report and it's still stuck in the serial buffer (no biggie)
						serialPort.ReadExisting();
						//Log("Discarded nonsensible byte stream---this shouldn't happen");
					}
				}
				else
				{
					Log("No response from bridge (check your connections!).");
				}
			}

			tmrInputTick.Enabled = true;
			flightCommandsEnabled = true;
		}

		private void SendParams(int paramGroup)
		{
			// make sure ID was valid
			int id = GetOutgoingDroneID();
			if(id >= 0)
			{
				// send the bytes according to the parameter group requested
				if(paramGroup == PARAMS_LEVELING)
				{
					byte[] message = {MSG_SET_PARAMS, NUM_LEVELING_PID_BYTES + 2, (byte)id, PARAMS_LEVELING};
					Log("Sending " + NUM_LEVELING_PID_BYTES + " bytes to " + GetDroneString(id) + ".");
					serialPort.Write(message, 0, 4);
					serialPort.Write(BitConverter.GetBytes((float)numPitchLevelingGainP.Value), 0, 4);
					serialPort.Write(BitConverter.GetBytes((float)numPitchLevelingGainI.Value), 0, 4);
					serialPort.Write(BitConverter.GetBytes((float)numPitchLevelingGainD.Value), 0, 4);
					serialPort.Write(BitConverter.GetBytes((float)numPitchLevelingILimit.Value), 0, 4);
					serialPort.Write(BitConverter.GetBytes((float)numRollLevelingGainP.Value), 0, 4);
					serialPort.Write(BitConverter.GetBytes((float)numRollLevelingGainI.Value), 0, 4);
					serialPort.Write(BitConverter.GetBytes((float)numRollLevelingGainD.Value), 0, 4);
					serialPort.Write(BitConverter.GetBytes((float)numRollLevelingILimit.Value), 0, 4);
				}
				else if(paramGroup == PARAMS_ROTATION_RATE)
				{
					byte[] message = {MSG_SET_PARAMS, NUM_ROTATION_RATE_PID_BYTES + 2, (byte)id, PARAMS_ROTATION_RATE};
					Log("Sending " + NUM_ROTATION_RATE_PID_BYTES + " bytes to " + GetDroneString(id) + ".");
					serialPort.Write(message, 0, 4);
					serialPort.Write(BitConverter.GetBytes((float)numPitchRateGainP.Value), 0, 4);
					serialPort.Write(BitConverter.GetBytes((float)numPitchRateGainI.Value), 0, 4);
					serialPort.Write(BitConverter.GetBytes((float)numPitchRateGainD.Value), 0, 4);
					serialPort.Write(BitConverter.GetBytes((float)numPitchRateILimit.Value), 0, 4);
					serialPort.Write(BitConverter.GetBytes((float)numRollRateGainP.Value), 0, 4);
					serialPort.Write(BitConverter.GetBytes((float)numRollRateGainI.Value), 0, 4);
					serialPort.Write(BitConverter.GetBytes((float)numRollRateGainD.Value), 0, 4);
					serialPort.Write(BitConverter.GetBytes((float)numRollRateILimit.Value), 0, 4);
					serialPort.Write(BitConverter.GetBytes((float)numYawRateGainP.Value), 0, 4);
					serialPort.Write(BitConverter.GetBytes((float)numYawRateGainI.Value), 0, 4);
					serialPort.Write(BitConverter.GetBytes((float)numYawRateGainD.Value), 0, 4);
					serialPort.Write(BitConverter.GetBytes((float)numYawRateILimit.Value), 0, 4);
				}
				else if(paramGroup == PARAMS_RESPONSE_AND_TRIM)
				{
					byte[] message = {MSG_SET_PARAMS, NUM_RESPONSE_AND_TRIM_BYTES + 2, (byte)id, PARAMS_RESPONSE_AND_TRIM};
					Log("Sending " + NUM_RESPONSE_AND_TRIM_BYTES + " bytes to " + GetDroneString(id) + ".");
					serialPort.Write(message, 0, 4);
					serialPort.Write(BitConverter.GetBytes((float)numPitchDemandGain.Value), 0, 4);
					serialPort.Write(BitConverter.GetBytes((float)numRollDemandGain.Value), 0, 4);
					serialPort.Write(BitConverter.GetBytes((float)numYawDemandGain.Value), 0, 4);
					serialPort.Write(BitConverter.GetBytes((float)numPitchTrim.Value), 0, 4);
					serialPort.Write(BitConverter.GetBytes((float)numRollTrim.Value), 0, 4);
					serialPort.Write(BitConverter.GetBytes((float)numCompFilterAlpha.Value), 0, 4);
				}
			}
		}

		private void LoadParamsFrom(String filename)
		{
			// read from params file
			try
			{
				using(TextReader reader = File.OpenText(filename))
				{
					// leveling params
					numPitchLevelingGainP.Value = decimal.Parse(reader.ReadLine());
					numPitchLevelingGainI.Value = decimal.Parse(reader.ReadLine());
					numPitchLevelingGainD.Value = decimal.Parse(reader.ReadLine());
					numPitchLevelingILimit.Value = decimal.Parse(reader.ReadLine());
					numRollLevelingGainP.Value = decimal.Parse(reader.ReadLine());
					numRollLevelingGainI.Value = decimal.Parse(reader.ReadLine());
					numRollLevelingGainD.Value = decimal.Parse(reader.ReadLine());
					numRollLevelingILimit.Value = decimal.Parse(reader.ReadLine());

					// rotation rate params
					numPitchRateGainP.Value = decimal.Parse(reader.ReadLine());
					numPitchRateGainI.Value = decimal.Parse(reader.ReadLine());
					numPitchRateGainD.Value = decimal.Parse(reader.ReadLine());
					numPitchRateILimit.Value = decimal.Parse(reader.ReadLine());
					numRollRateGainP.Value = decimal.Parse(reader.ReadLine());
					numRollRateGainI.Value = decimal.Parse(reader.ReadLine());
					numRollRateGainD.Value = decimal.Parse(reader.ReadLine());
					numRollRateILimit.Value = decimal.Parse(reader.ReadLine());
					numYawRateGainP.Value = decimal.Parse(reader.ReadLine());
					numYawRateGainI.Value = decimal.Parse(reader.ReadLine());
					numYawRateGainD.Value = decimal.Parse(reader.ReadLine());
					numYawRateILimit.Value = decimal.Parse(reader.ReadLine());

					// response and trim params
					numPitchDemandGain.Value = decimal.Parse(reader.ReadLine());
					numRollDemandGain.Value = decimal.Parse(reader.ReadLine());
					numYawDemandGain.Value = decimal.Parse(reader.ReadLine());
					numPitchTrim.Value = decimal.Parse(reader.ReadLine());
					numRollTrim.Value = decimal.Parse(reader.ReadLine());
					numCompFilterAlpha.Value = decimal.Parse(reader.ReadLine());
				}

				Log("Loaded \"" + GetShortFilename(filename) + "\".");
			}
			catch(FileNotFoundException)
			{
				Error("Parameters file \"" + GetShortFilename(filename) + "\" could not be found.", "Could Not Find Params File");
			}
			catch(Exception e)
			{
				Error("Encountered error while parsing \"" + GetShortFilename(filename) + "\": " + e.Message, "Error While Parsing Params File");
			}
		}

		private void SaveParamsTo(String filename)
		{
			// write to params file
			try
			{
				using(StreamWriter writer = File.CreateText(filename))
				{
					// leveling params
					writer.WriteLine(numPitchLevelingGainP.Value);
					writer.WriteLine(numPitchLevelingGainI.Value);
					writer.WriteLine(numPitchLevelingGainD.Value);
					writer.WriteLine(numPitchLevelingILimit.Value);
					writer.WriteLine(numRollLevelingGainP.Value);
					writer.WriteLine(numRollLevelingGainI.Value);
					writer.WriteLine(numRollLevelingGainD.Value);
					writer.WriteLine(numRollLevelingILimit.Value);

					// rotation rate params
					writer.WriteLine(numPitchRateGainP.Value);
					writer.WriteLine(numPitchRateGainI.Value);
					writer.WriteLine(numPitchRateGainD.Value);
					writer.WriteLine(numPitchRateILimit.Value);
					writer.WriteLine(numRollRateGainP.Value);
					writer.WriteLine(numRollRateGainI.Value);
					writer.WriteLine(numRollRateGainD.Value);
					writer.WriteLine(numRollRateILimit.Value);
					writer.WriteLine(numYawRateGainP.Value);
					writer.WriteLine(numYawRateGainI.Value);
					writer.WriteLine(numYawRateGainD.Value);
					writer.WriteLine(numYawRateILimit.Value);

					// response and trim params
					writer.WriteLine(numPitchDemandGain.Value);
					writer.WriteLine(numRollDemandGain.Value);
					writer.WriteLine(numYawDemandGain.Value);
					writer.WriteLine(numPitchTrim.Value);
					writer.WriteLine(numRollTrim.Value);
					writer.WriteLine(numCompFilterAlpha.Value);
				}

				Log("Saved \"" + GetShortFilename(filename) + "\".");
			}
			catch(Exception e)
			{
				Error("Encountered error while outputting \"" + GetShortFilename(filename) + "\": " + e.Message, "Param File Write Error");
			}
		}

		private void LoadParamsFromDialog()
		{
		    OpenFileDialog open = new OpenFileDialog();

            open.Title = "Open Params Text File";
            open.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";

            if (open.ShowDialog() == DialogResult.OK)
            {
				fullParamsFilename = open.FileName;
				shortParamsFilename = GetShortFilename(fullParamsFilename);

				LoadParamsFrom(fullParamsFilename);
				EnableOrDisableParamFileButtons();
				UpdateFormTitleWithParamsFile();
            }
		}

		private void SaveParamsFromDialog()
		{
			SaveFileDialog save = new SaveFileDialog();

            save.Title = "Save Params Text File";
            save.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
			save.AddExtension = false;
			save.FileName = "drone_" + numUnicastID.Value + "_params.txt";

            if (save.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    fullParamsFilename = save.FileName;
					shortParamsFilename = GetShortFilename(fullParamsFilename);

                    if(!fullParamsFilename.Contains("."))
					{
                        fullParamsFilename += ".txt";
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }

                SaveParamsTo(fullParamsFilename);
				EnableOrDisableParamFileButtons();
				UpdateFormTitleWithParamsFile();
            }
		}

		private void EnableOrDisableParamFileButtons()
		{
			if(fullParamsFilename == null)
			{
				btnReloadParams.Enabled = false;
				btnSaveParams.Enabled = false;
			}
			else
			{
				btnReloadParams.Enabled = true;
				btnSaveParams.Enabled = true;
			}
		}

		private void UpdateFormTitleWithParamsFile()
		{
			if(fullParamsFilename != null)
			{
				this.Text = "uBee Flight Config Panel v" + VERSION_INFO + " [" + fullParamsFilename + "]";
			}
			else
			{
				this.Text = "uBee Flight Config Panel v" + VERSION_INFO + " [no params file]";
			}
		}

        private String GetShortFilename(String longFilename)
        {
            int shortFilenameStart = longFilename.LastIndexOf(Path.DirectorySeparatorChar) + 1;
            return longFilename.Substring(shortFilenameStart, longFilename.Length - shortFilenameStart);
        }

		private void FlashLED()
		{
			int id = GetOutgoingDroneID();

			if(id >= 0)
			{
				// the LED on the MQC should flash when it receives this
				// (note that we do not require a response)
				try
				{
					Log("Instructing " + GetDroneString(id) + " to flash LED.");
					byte[] message = {MSG_FLASH_LED, 1, (byte)id};
					serialPort.Write(message, 0, 3);
				}
				catch (Exception)
				{
					Error("Serial port could not be written to.", "Serial Port Error");
					serialPort = null;
				}
			}
		}

		private void RecalibrateIMU()
		{
			int id = GetOutgoingDroneID();

			if(id >= 0)
			{
				// simple instruction to recalibrate the IMU
				try
				{
					Log("Instructing " + GetDroneString(id) + " to recalibrate IMU.");
					byte[] message = {MSG_RECALIBRATE_IMU, 1, (byte)id};
					serialPort.Write(message, 0, 3);
				}
				catch (Exception)
				{
					Error("Serial port could not be written to.", "Serial Port Error");
					serialPort = null;
				}
			}
		}

		private void StartOrStopRangeTest()
		{
			if(!tmrRangeTest.Enabled)
			{
				// if the timer is off, turn it on and allow the user to disable the test
				btnRangeTest.Text = "Stop Range Test";
				tmrRangeTest.Enabled = true;

				// turn off all other controls
				foreach(Control control in grpCommands.Controls)
				{
					if(control != btnRangeTest)
					{
						control.Enabled = false;
					}
				}

				// indicate that we're turning the test on
				Log("Started range test...");
			}
			else
			{
				// if the timer is on, turn it off and allow the user to start the test
				btnRangeTest.Text = "Start Range Test";
				tmrRangeTest.Enabled = false;

				// turn back on other controls
				foreach(Control control in grpCommands.Controls)
				{
					control.Enabled = true;
				}

				// indicate that we're turning the test off
				Log("...Ended range test.");
				CheckForBroadcast();
			}
		}

		private void StartOrStopIMUPlot()
		{
			int id = GetOutgoingDroneID();

			if(id >= 0)
			{
				if(!tmrIMUTest.Enabled)
				{
					// turn it on and allow the user to disable the plot
					btnStartIMUTest.Text = "Stop IMU Plot";
					tmrIMUTest.Enabled = true;
					txtCurrentID.Enabled = false;
					numNewID.Enabled = false;
					btnAssignID.Enabled = false;

					// turn off all other controls
					foreach(Control control in grpCommands.Controls)
					{
						if(control != btnStartIMUTest)
						{
							control.Enabled = false;
						}
					}

					// insruct the MQC to start continuously transmitting IMU values
					try
					{
						byte[] message = {MSG_START_IMU_TEST, 1, (byte)id};
						serialPort.Write(message, 0, 3);
					}
					catch (Exception)
					{
						Error("Serial port could not be written to.", "Serial Port Error");
						serialPort = null;
					}

					// clear the chart
					imuTimeIndex = 0;
					chtIMUChart.Series[0].Points.Clear();
					chtIMUChart.Series[1].Points.Clear();
					chtIMUChart.Series[2].Points.Clear();

					// indicate that we're turning the plot on
					Log("Started IMU plot...");
				}
				else
				{
					// if the timer is on, turn it off and allow the user to start the test
					btnStartIMUTest.Text = "Start IMU Plot";
					tmrIMUTest.Enabled = false;
					txtCurrentID.Enabled = true;
					numNewID.Enabled = true;
					btnAssignID.Enabled = true;

					// turn back on other controls
					foreach(Control control in grpCommands.Controls)
					{
						control.Enabled = true;
					}

					// insruct the MQC to stop continuously transmitting IMU values
					try
					{
						byte[] message = {MSG_END_IMU_TEST, 1, (byte)id};
						serialPort.Write(message, 0, 3);
					}
					catch (Exception)
					{
						Error("Serial port could not be written to.", "Serial Port Error");
						serialPort = null;
					}

					// indicate that we're turning the test off
					Log("...Ended IMU plot.");
					CheckForBroadcast();
				}
			}
		}

        private void Connect()
        {
			// attempt to open the serial port
			OpenSerialPort();

			// if it is open, then we can enable the command panel and try to find a joystick
			if(serialPort != null)
			{
				grpCommands.Enabled = true;
				btnConnect.Text = "Disconnect";
				txtPort.Enabled = false;
				InitJoystick();
				//LoadDefaultParams();
			}
		}

        private void Disconnect()
        {
			CloseSerialPort();

            btnConnect.Text = "Connect";
            lblJoystickValues.Visible = false;
            tmrInputTick.Enabled = false;
			grpCommands.Enabled = false;
			txtPort.Enabled = true;
        }

        private void OpenSerialPort()
        {
			try
            {
                serialPort = new SerialPort(txtPort.Text, 38400);//57600);//115200);//9600);
                serialPort.DataBits = 8;
                serialPort.StopBits = StopBits.One;
                serialPort.Parity = Parity.None;
                serialPort.Open();

                Log("Serial port " + txtPort.Text + " opened.");
            }
            catch (Exception e)
            {
                Error("Serial port " + txtPort.Text + " could not be opened: " + e, "Error Opening Serial Port");
                serialPort = null;
            }
        }

        private void CloseSerialPort()
        {
            try
            {
                if (serialPort != null && serialPort.IsOpen)
                {
                    serialPort.Close();
					serialPort = null;
                }

                Log("Serial port closed.");
            }
            catch (Exception)
            {
                serialPort = null;
            }
        }

        private void InitJoystick()
        {
            DirectInput directInput = new DirectInput();
            Guid joystickGuid = Guid.Empty;

			// retrieve ID of last joystick---there's probably a better way to do this
            foreach (var deviceInstance in directInput.GetDevices(DeviceType.Joystick, DeviceEnumerationFlags.AllDevices))
            {
                joystickGuid = deviceInstance.InstanceGuid;
            }

			// if there is a joystick present, attempt to connect to it
            if (joystickGuid != Guid.Empty)
            {
				// joystick settings
                joystick = new Joystick(directInput, joystickGuid);
                joystick.Properties.BufferSize = 128;
                joystick.Properties.AxisMode = DeviceAxisMode.Absolute;//Relative;
                joystick.Acquire();

				// enable joystick position debugging
				tmrInputTick.Enabled = true;
				lblJoystickValues.Visible = true;

				// indicate success
                Log("Connected to joystick.");
            }
            else
            {
                //Error("Joystick was not found. This is okay, but flight commands can't be sent.", "Joystick Not Found");
				Log("Joystick not found (but this is okay).");
			}
        }

        private void UpdateInputData()
        {
			UpdateMotionsWithJoystick();
        }

        private void UpdateMotionsWithJoystick()
        {
			// retrieve joystick values (in this case, we're using the Logitech Extreme 3D Pro)
            int joystickX = joystick.GetCurrentState().X;
            int joystickY = joystick.GetCurrentState().Y;
			int joystickSlider = joystick.GetCurrentState().Sliders[0];
			int joystickTwist = joystick.GetCurrentState().RotationZ;
			bool downButton = joystick.GetCurrentState().Buttons[2];
			bool upButton = joystick.GetCurrentState().Buttons[4];

			// bring from [0-65535] range to [0-255] range
            thrust = (byte)Clamp(joystickSlider / 257, 0, 255);
            forward = (byte)Clamp(joystickY / 257, 0, 255);
			strafe = (byte)Clamp(joystickX / 257, 0, 255);
			yaw = (byte)Clamp(joystickTwist / 257, 0, 255);

			// thrust and forward axes need to be inverted for this joystick---your mileage may vary
			thrust = (byte)(255 - thrust);
			forward = (byte)(255 - forward);

			// set thrust mode
			if(upButton && !downButton)
				thrustMode = ThrustMode.THRUST_MODE_UP;
			else if(downButton)
				thrustMode = ThrustMode.THRUST_MODE_DOWN;
			else
				thrustMode = ThrustMode.THRUST_MODE_HOVER;
        }

        private void UpdatePositionLabel()
        {
			string[] THRUST_STRINGS = {"DOWN", "HOVER", "UP"};

            lblJoystickValues.Text = "Thrust:    " + THRUST_STRINGS[(int)thrustMode] + Environment.NewLine +
									 "Forward:   " + forward + Environment.NewLine +
									 "Strafe:    " + strafe + Environment.NewLine +
									 "Yaw:       " + yaw + Environment.NewLine;
        }

		private void SendFlightCommand()
		{
			// make sure the drone ID is valid and send the new flight command
			int id = GetOutgoingDroneID();
			if(id >= 0 && flightCommandsEnabled)
			{
				byte[] message = {MSG_SINGLE_FLIGHT_COMMAND, 5, (byte)id, (byte)thrustMode, forward, strafe, yaw};
				serialPort.Write(message, 0, 7);
			}
		}

		private void SendDroneID()
		{
			// make sure the drone ID is valid and send the new ID
			int targetID = GetOutgoingDroneID();
			int newID = GetNewDroneID();
			if(targetID >= 0 && newID >= 0)
			{
				Log("Reassigning drone ID from " + targetID + " to " + newID + ".");
				byte[] message = {MSG_SET_ID, 2, (byte)targetID, (byte)newID};
				serialPort.Write(message, 0, 4);
			}
		}

		private int GetOutgoingDroneID()
		{
			int id = (int)numUnicastID.Value;

			if(chkBroadcast.Checked)
			{
				id = BROADCAST_ID;
			}

			return id;
		}

		private int GetNewDroneID()
		{
			return (int)numNewID.Value;
		}

		private String GetDroneString(int id)
		{
			String result;

			if(id == 255) result = "all drones";
			else          result = "drone " + id;

			return result;
		}

		private void CheckForBroadcast()
		{
			EnableUnicastCommands(!chkBroadcast.Checked);
		}

		private void EnableUnicastCommands(bool enable)
		{
			btnRequestLevelingPIDParams.Enabled = enable;
			btnRequestRotationRatePIDParams.Enabled = enable;
			btnRequestResponseAndTrimParams.Enabled = enable;
			btnRequestStatus.Enabled = enable;
			btnStartIMUTest.Enabled = enable;
			numUnicastID.Enabled = enable;
		}

        // error and logging methods

        private void Error(String message, String title)
        {
            MessageBox.Show(message, title, MessageBoxButtons.OK, MessageBoxIcon.Error);
            Log(message);
        }

        public void Log(String message)
        {
            txtLog.AppendText(DateTime.Now.ToShortTimeString() + ": " + message + Environment.NewLine);
        }

		// - - - utility methods - - - //

		private int Clamp(int val, int min, int max)
		{
			if(val < min) val = min;
			else if(val > max) val = max;
			return val;
		}

        // - - - .NET events - - - //

        private void btnConnect_Click(object sender, EventArgs e)
        {
            if (serialPort == null)
            {
                Connect();
            }
            else
            {
                Disconnect();
            }
        }

        private void tmrInputTick_Tick(object sender, EventArgs e)
        {
			if(joystick != null)
			{
				UpdateInputData();
				UpdatePositionLabel();
				SendFlightCommand();
			}
        }

		private void RemoteControl_FormClosing(object sender, FormClosingEventArgs e)
		{
			Disconnect();
		}

		private void btnPing_Click(object sender, EventArgs e)
		{
			FlashLED();
		}

		private void tmrRangeTest_Tick(object sender, EventArgs e)
		{
			FlashLED();
		}

		private void btnRangeTest_Click(object sender, EventArgs e)
		{
			StartOrStopRangeTest();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			if(fullParamsFilename != null)
			{
				LoadParamsFrom(fullParamsFilename);
			}
		}

		private void btnCalibrateIMU_Click(object sender, EventArgs e)
		{
			RecalibrateIMU();
		}

		private void btnSaveParams_Click(object sender, EventArgs e)
		{
			if(fullParamsFilename != null)
			{
				SaveParamsTo(fullParamsFilename);
			}
		}

		private void btnStartIMUTest_Click(object sender, EventArgs e)
		{
			StartOrStopIMUPlot();
		}

		private void tmrIMUTest_Tick(object sender, EventArgs e)
		{
			byte response;
			byte[] received;

			// increase our time index
			imuTimeIndex = (imuTimeIndex + tmrIMUTest.Interval) % 5000;
			if(imuTimeIndex == 0)
			{
				chtIMUChart.Series[0].Points.Clear();
				chtIMUChart.Series[1].Points.Clear();
				chtIMUChart.Series[2].Points.Clear();
			}

			// the bridge should respond with SOMEthing...
			if(serialPort.BytesToRead > 0)
			{
				response = (byte)serialPort.ReadByte();
				if(response == MSG_IMU_STATE)
				{
					// make sure we received enough data!
					while(serialPort.BytesToRead < NUM_IMU_BYTES);

					// now copy and then store the values we received
					received = new byte[serialPort.BytesToRead];
					serialPort.Read(received, 0, NUM_IMU_BYTES);

					// plot the values
					chtIMUChart.Series[0].Points.AddXY(imuTimeIndex, ((int)received[0]) - 127);
					chtIMUChart.Series[1].Points.AddXY(imuTimeIndex, ((int)received[1]) - 127);
					chtIMUChart.Series[2].Points.AddXY(imuTimeIndex, ((int)received[2]) - 127);

					// erase the rest
					//serialPort.ReadExisting();
				}
			}
		}

		private void chkBroadcast_CheckedChanged(object sender, EventArgs e)
		{
			CheckForBroadcast();
			if(chkBroadcast.Checked)
			{
				Log("Broadcast enabled; RX commands disabled.");
			}
			else
			{
				Log("Broadcast disabled; RX commands enabled.");
			}
		}

		private void btnAssignID_Click(object sender, EventArgs e)
		{
			SendDroneID();
		}

		private void btnRequestLevelingPIDParams_Click(object sender, EventArgs e)
		{
			RequestParams(PARAMS_LEVELING, NUM_LEVELING_PID_BYTES);
		}

		private void btnRequestRotationRatePIDParams_Click(object sender, EventArgs e)
		{
			RequestParams(PARAMS_ROTATION_RATE, NUM_ROTATION_RATE_PID_BYTES);
		}

		private void btnRequestResponseAndTrimParams_Click(object sender, EventArgs e)
		{
			RequestParams(PARAMS_RESPONSE_AND_TRIM, NUM_RESPONSE_AND_TRIM_BYTES);
		}

		private void btnRequestValues_Click(object sender, EventArgs e)
		{
			RequestParams(PARAMS_STATUS, NUM_STATUS_BYTES);
		}

		private void btnSendLevelingPIDParams_Click(object sender, EventArgs e)
		{
			SendParams(PARAMS_LEVELING);
		}

		private void btnSendRotationRatePIDParams_Click(object sender, EventArgs e)
		{
			SendParams(PARAMS_ROTATION_RATE);
		}

		private void btnSendResponseAndTrimParams_Click(object sender, EventArgs e)
		{
			SendParams(PARAMS_RESPONSE_AND_TRIM);
		}

		private void btnLoadParams_Click(object sender, EventArgs e)
		{
			LoadParamsFromDialog();
		}

		private void btnSaveParamsAs_Click(object sender, EventArgs e)
		{
			SaveParamsFromDialog();
		}
	}
}
