﻿namespace PCRemote
{
	partial class FlightPanel
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if(disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FlightPanel));
			System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
			System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
			System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
			System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint1 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 5D);
			System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint2 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1D, 10D);
			System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
			System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
			this.grpInputMethod = new System.Windows.Forms.GroupBox();
			this.txtPort = new System.Windows.Forms.TextBox();
			this.btnConnect = new System.Windows.Forms.Button();
			this.grpLog = new System.Windows.Forms.GroupBox();
			this.txtLog = new System.Windows.Forms.TextBox();
			this.lblJoystickValues = new System.Windows.Forms.Label();
			this.tmrInputTick = new System.Windows.Forms.Timer(this.components);
			this.grpData = new System.Windows.Forms.GroupBox();
			this.grpCommands = new System.Windows.Forms.GroupBox();
			this.label9 = new System.Windows.Forms.Label();
			this.txtCurrentHz = new System.Windows.Forms.TextBox();
			this.btnLoadParams = new System.Windows.Forms.Button();
			this.label5 = new System.Windows.Forms.Label();
			this.numUnicastID = new System.Windows.Forms.NumericUpDown();
			this.numNewID = new System.Windows.Forms.NumericUpDown();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.numCompFilterAlpha = new System.Windows.Forms.NumericUpDown();
			this.numRollTrim = new System.Windows.Forms.NumericUpDown();
			this.numPitchTrim = new System.Windows.Forms.NumericUpDown();
			this.numYawDemandGain = new System.Windows.Forms.NumericUpDown();
			this.numRollDemandGain = new System.Windows.Forms.NumericUpDown();
			this.numPitchDemandGain = new System.Windows.Forms.NumericUpDown();
			this.label2 = new System.Windows.Forms.Label();
			this.label34 = new System.Windows.Forms.Label();
			this.label26 = new System.Windows.Forms.Label();
			this.label33 = new System.Windows.Forms.Label();
			this.label25 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.label24 = new System.Windows.Forms.Label();
			this.btnRequestResponseAndTrimParams = new System.Windows.Forms.Button();
			this.btnSendResponseAndTrimParams = new System.Windows.Forms.Button();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.numYawRateILimit = new System.Windows.Forms.NumericUpDown();
			this.numRollRateILimit = new System.Windows.Forms.NumericUpDown();
			this.numPitchRateILimit = new System.Windows.Forms.NumericUpDown();
			this.numYawRateGainD = new System.Windows.Forms.NumericUpDown();
			this.numYawRateGainI = new System.Windows.Forms.NumericUpDown();
			this.numYawRateGainP = new System.Windows.Forms.NumericUpDown();
			this.numRollRateGainD = new System.Windows.Forms.NumericUpDown();
			this.numRollRateGainI = new System.Windows.Forms.NumericUpDown();
			this.numRollRateGainP = new System.Windows.Forms.NumericUpDown();
			this.numPitchRateGainD = new System.Windows.Forms.NumericUpDown();
			this.label22 = new System.Windows.Forms.Label();
			this.numPitchRateGainI = new System.Windows.Forms.NumericUpDown();
			this.btnRequestRotationRatePIDParams = new System.Windows.Forms.Button();
			this.numPitchRateGainP = new System.Windows.Forms.NumericUpDown();
			this.btnSendRotationRatePIDParams = new System.Windows.Forms.Button();
			this.label10 = new System.Windows.Forms.Label();
			this.label20 = new System.Windows.Forms.Label();
			this.label16 = new System.Windows.Forms.Label();
			this.grpLevelling = new System.Windows.Forms.GroupBox();
			this.numRollLevelingILimit = new System.Windows.Forms.NumericUpDown();
			this.numPitchLevelingILimit = new System.Windows.Forms.NumericUpDown();
			this.numRollLevelingGainD = new System.Windows.Forms.NumericUpDown();
			this.numRollLevelingGainI = new System.Windows.Forms.NumericUpDown();
			this.numRollLevelingGainP = new System.Windows.Forms.NumericUpDown();
			this.numPitchLevelingGainD = new System.Windows.Forms.NumericUpDown();
			this.numPitchLevelingGainI = new System.Windows.Forms.NumericUpDown();
			this.numPitchLevelingGainP = new System.Windows.Forms.NumericUpDown();
			this.btnRequestLevelingPIDParams = new System.Windows.Forms.Button();
			this.btnSendLevelingPIDParams = new System.Windows.Forms.Button();
			this.label23 = new System.Windows.Forms.Label();
			this.label21 = new System.Windows.Forms.Label();
			this.btnCalibrateIMU = new System.Windows.Forms.Button();
			this.btnPing = new System.Windows.Forms.Button();
			this.btnSaveParamsAs = new System.Windows.Forms.Button();
			this.txtCurrentDeltaTime = new System.Windows.Forms.TextBox();
			this.label17 = new System.Windows.Forms.Label();
			this.btnReloadParams = new System.Windows.Forms.Button();
			this.btnSaveParams = new System.Windows.Forms.Button();
			this.btnAssignID = new System.Windows.Forms.Button();
			this.txtCurrentID = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.chkBroadcast = new System.Windows.Forms.CheckBox();
			this.label7 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.btnStartIMUTest = new System.Windows.Forms.Button();
			this.btnRequestStatus = new System.Windows.Forms.Button();
			this.label31 = new System.Windows.Forms.Label();
			this.txtCurrentBattery = new System.Windows.Forms.TextBox();
			this.label32 = new System.Windows.Forms.Label();
			this.label29 = new System.Windows.Forms.Label();
			this.txtCurrentRoll = new System.Windows.Forms.TextBox();
			this.label30 = new System.Windows.Forms.Label();
			this.label27 = new System.Windows.Forms.Label();
			this.txtCurrentPitch = new System.Windows.Forms.TextBox();
			this.label28 = new System.Windows.Forms.Label();
			this.picQuadcopter = new System.Windows.Forms.PictureBox();
			this.btnRangeTest = new System.Windows.Forms.Button();
			this.tmrRangeTest = new System.Windows.Forms.Timer(this.components);
			this.chtIMUChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
			this.tmrIMUTest = new System.Windows.Forms.Timer(this.components);
			this.grpChart = new System.Windows.Forms.GroupBox();
			this.grpInputMethod.SuspendLayout();
			this.grpLog.SuspendLayout();
			this.grpData.SuspendLayout();
			this.grpCommands.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numUnicastID)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numNewID)).BeginInit();
			this.groupBox3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numCompFilterAlpha)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numRollTrim)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numPitchTrim)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numYawDemandGain)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numRollDemandGain)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numPitchDemandGain)).BeginInit();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numYawRateILimit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numRollRateILimit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numPitchRateILimit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numYawRateGainD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numYawRateGainI)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numYawRateGainP)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numRollRateGainD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numRollRateGainI)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numRollRateGainP)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numPitchRateGainD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numPitchRateGainI)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numPitchRateGainP)).BeginInit();
			this.grpLevelling.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numRollLevelingILimit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numPitchLevelingILimit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numRollLevelingGainD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numRollLevelingGainI)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numRollLevelingGainP)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numPitchLevelingGainD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numPitchLevelingGainI)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numPitchLevelingGainP)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.picQuadcopter)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chtIMUChart)).BeginInit();
			this.grpChart.SuspendLayout();
			this.SuspendLayout();
			// 
			// grpInputMethod
			// 
			this.grpInputMethod.Controls.Add(this.txtPort);
			this.grpInputMethod.Controls.Add(this.btnConnect);
			this.grpInputMethod.Location = new System.Drawing.Point(12, 12);
			this.grpInputMethod.Name = "grpInputMethod";
			this.grpInputMethod.Size = new System.Drawing.Size(297, 89);
			this.grpInputMethod.TabIndex = 0;
			this.grpInputMethod.TabStop = false;
			this.grpInputMethod.Text = "Serial Connection";
			// 
			// txtPort
			// 
			this.txtPort.Location = new System.Drawing.Point(6, 20);
			this.txtPort.Name = "txtPort";
			this.txtPort.Size = new System.Drawing.Size(111, 20);
			this.txtPort.TabIndex = 0;
			this.txtPort.Text = "COM3";
			// 
			// btnConnect
			// 
			this.btnConnect.Location = new System.Drawing.Point(6, 46);
			this.btnConnect.Name = "btnConnect";
			this.btnConnect.Size = new System.Drawing.Size(111, 34);
			this.btnConnect.TabIndex = 1;
			this.btnConnect.Text = "Connect";
			this.btnConnect.UseVisualStyleBackColor = true;
			this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
			// 
			// grpLog
			// 
			this.grpLog.Controls.Add(this.txtLog);
			this.grpLog.Location = new System.Drawing.Point(12, 199);
			this.grpLog.Name = "grpLog";
			this.grpLog.Size = new System.Drawing.Size(297, 380);
			this.grpLog.TabIndex = 2;
			this.grpLog.TabStop = false;
			this.grpLog.Text = "Log";
			// 
			// txtLog
			// 
			this.txtLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.txtLog.Location = new System.Drawing.Point(7, 20);
			this.txtLog.Multiline = true;
			this.txtLog.Name = "txtLog";
			this.txtLog.ReadOnly = true;
			this.txtLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.txtLog.Size = new System.Drawing.Size(284, 354);
			this.txtLog.TabIndex = 2;
			// 
			// lblJoystickValues
			// 
			this.lblJoystickValues.AutoSize = true;
			this.lblJoystickValues.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblJoystickValues.Location = new System.Drawing.Point(6, 21);
			this.lblJoystickValues.Name = "lblJoystickValues";
			this.lblJoystickValues.Size = new System.Drawing.Size(79, 52);
			this.lblJoystickValues.TabIndex = 4;
			this.lblJoystickValues.Text = "Thrust:    0\r\nForward:   0\r\nStrafe:    0\r\nYaw:       0\r\n";
			// 
			// tmrInputTick
			// 
			this.tmrInputTick.Interval = 50;
			this.tmrInputTick.Tick += new System.EventHandler(this.tmrInputTick_Tick);
			// 
			// grpData
			// 
			this.grpData.Controls.Add(this.lblJoystickValues);
			this.grpData.Location = new System.Drawing.Point(12, 107);
			this.grpData.Name = "grpData";
			this.grpData.Size = new System.Drawing.Size(297, 86);
			this.grpData.TabIndex = 1;
			this.grpData.TabStop = false;
			this.grpData.Text = "Joystick";
			// 
			// grpCommands
			// 
			this.grpCommands.Controls.Add(this.label9);
			this.grpCommands.Controls.Add(this.txtCurrentHz);
			this.grpCommands.Controls.Add(this.btnLoadParams);
			this.grpCommands.Controls.Add(this.label5);
			this.grpCommands.Controls.Add(this.numUnicastID);
			this.grpCommands.Controls.Add(this.numNewID);
			this.grpCommands.Controls.Add(this.groupBox3);
			this.grpCommands.Controls.Add(this.groupBox1);
			this.grpCommands.Controls.Add(this.label16);
			this.grpCommands.Controls.Add(this.grpLevelling);
			this.grpCommands.Controls.Add(this.btnCalibrateIMU);
			this.grpCommands.Controls.Add(this.btnPing);
			this.grpCommands.Controls.Add(this.btnSaveParamsAs);
			this.grpCommands.Controls.Add(this.txtCurrentDeltaTime);
			this.grpCommands.Controls.Add(this.label17);
			this.grpCommands.Controls.Add(this.btnReloadParams);
			this.grpCommands.Controls.Add(this.btnSaveParams);
			this.grpCommands.Controls.Add(this.btnAssignID);
			this.grpCommands.Controls.Add(this.txtCurrentID);
			this.grpCommands.Controls.Add(this.label8);
			this.grpCommands.Controls.Add(this.chkBroadcast);
			this.grpCommands.Controls.Add(this.label7);
			this.grpCommands.Controls.Add(this.label6);
			this.grpCommands.Controls.Add(this.btnStartIMUTest);
			this.grpCommands.Controls.Add(this.btnRequestStatus);
			this.grpCommands.Controls.Add(this.label31);
			this.grpCommands.Controls.Add(this.txtCurrentBattery);
			this.grpCommands.Controls.Add(this.label32);
			this.grpCommands.Controls.Add(this.label29);
			this.grpCommands.Controls.Add(this.txtCurrentRoll);
			this.grpCommands.Controls.Add(this.label30);
			this.grpCommands.Controls.Add(this.label27);
			this.grpCommands.Controls.Add(this.txtCurrentPitch);
			this.grpCommands.Controls.Add(this.label28);
			this.grpCommands.Controls.Add(this.picQuadcopter);
			this.grpCommands.Controls.Add(this.btnRangeTest);
			this.grpCommands.Location = new System.Drawing.Point(315, 12);
			this.grpCommands.Name = "grpCommands";
			this.grpCommands.Size = new System.Drawing.Size(687, 567);
			this.grpCommands.TabIndex = 3;
			this.grpCommands.TabStop = false;
			this.grpCommands.Text = "Flight Configuration";
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(644, 323);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(20, 13);
			this.label9.TabIndex = 492;
			this.label9.Text = "Hz";
			// 
			// txtCurrentHz
			// 
			this.txtCurrentHz.Location = new System.Drawing.Point(599, 319);
			this.txtCurrentHz.Name = "txtCurrentHz";
			this.txtCurrentHz.ReadOnly = true;
			this.txtCurrentHz.Size = new System.Drawing.Size(39, 20);
			this.txtCurrentHz.TabIndex = 491;
			this.txtCurrentHz.Text = "N/A";
			// 
			// btnLoadParams
			// 
			this.btnLoadParams.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnLoadParams.Location = new System.Drawing.Point(444, 477);
			this.btnLoadParams.Name = "btnLoadParams";
			this.btnLoadParams.Size = new System.Drawing.Size(111, 34);
			this.btnLoadParams.TabIndex = 430;
			this.btnLoadParams.Text = "Open Params...";
			this.btnLoadParams.UseVisualStyleBackColor = true;
			this.btnLoadParams.Click += new System.EventHandler(this.btnLoadParams_Click);
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(274, 23);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(34, 13);
			this.label5.TabIndex = 309;
			this.label5.Text = "0-255";
			// 
			// numUnicastID
			// 
			this.numUnicastID.Location = new System.Drawing.Point(123, 21);
			this.numUnicastID.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
			this.numUnicastID.Name = "numUnicastID";
			this.numUnicastID.Size = new System.Drawing.Size(78, 20);
			this.numUnicastID.TabIndex = 3;
			// 
			// numNewID
			// 
			this.numNewID.Location = new System.Drawing.Point(207, 46);
			this.numNewID.Maximum = new decimal(new int[] {
            254,
            0,
            0,
            0});
			this.numNewID.Name = "numNewID";
			this.numNewID.Size = new System.Drawing.Size(61, 20);
			this.numNewID.TabIndex = 6;
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add(this.numCompFilterAlpha);
			this.groupBox3.Controls.Add(this.numRollTrim);
			this.groupBox3.Controls.Add(this.numPitchTrim);
			this.groupBox3.Controls.Add(this.numYawDemandGain);
			this.groupBox3.Controls.Add(this.numRollDemandGain);
			this.groupBox3.Controls.Add(this.numPitchDemandGain);
			this.groupBox3.Controls.Add(this.label2);
			this.groupBox3.Controls.Add(this.label34);
			this.groupBox3.Controls.Add(this.label26);
			this.groupBox3.Controls.Add(this.label33);
			this.groupBox3.Controls.Add(this.label25);
			this.groupBox3.Controls.Add(this.label4);
			this.groupBox3.Controls.Add(this.label3);
			this.groupBox3.Controls.Add(this.label1);
			this.groupBox3.Controls.Add(this.label24);
			this.groupBox3.Controls.Add(this.btnRequestResponseAndTrimParams);
			this.groupBox3.Controls.Add(this.btnSendResponseAndTrimParams);
			this.groupBox3.Location = new System.Drawing.Point(6, 342);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(432, 216);
			this.groupBox3.TabIndex = 306;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Response and Trim";
			// 
			// numCompFilterAlpha
			// 
			this.numCompFilterAlpha.DecimalPlaces = 2;
			this.numCompFilterAlpha.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
			this.numCompFilterAlpha.Location = new System.Drawing.Point(110, 150);
			this.numCompFilterAlpha.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.numCompFilterAlpha.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
			this.numCompFilterAlpha.Name = "numCompFilterAlpha";
			this.numCompFilterAlpha.Size = new System.Drawing.Size(73, 20);
			this.numCompFilterAlpha.TabIndex = 330;
			// 
			// numRollTrim
			// 
			this.numRollTrim.DecimalPlaces = 2;
			this.numRollTrim.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
			this.numRollTrim.Location = new System.Drawing.Point(110, 124);
			this.numRollTrim.Maximum = new decimal(new int[] {
            90,
            0,
            0,
            0});
			this.numRollTrim.Minimum = new decimal(new int[] {
            90,
            0,
            0,
            -2147483648});
			this.numRollTrim.Name = "numRollTrim";
			this.numRollTrim.Size = new System.Drawing.Size(73, 20);
			this.numRollTrim.TabIndex = 320;
			// 
			// numPitchTrim
			// 
			this.numPitchTrim.DecimalPlaces = 2;
			this.numPitchTrim.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
			this.numPitchTrim.Location = new System.Drawing.Point(110, 98);
			this.numPitchTrim.Maximum = new decimal(new int[] {
            90,
            0,
            0,
            0});
			this.numPitchTrim.Minimum = new decimal(new int[] {
            90,
            0,
            0,
            -2147483648});
			this.numPitchTrim.Name = "numPitchTrim";
			this.numPitchTrim.Size = new System.Drawing.Size(73, 20);
			this.numPitchTrim.TabIndex = 310;
			// 
			// numYawDemandGain
			// 
			this.numYawDemandGain.DecimalPlaces = 2;
			this.numYawDemandGain.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.numYawDemandGain.Location = new System.Drawing.Point(110, 72);
			this.numYawDemandGain.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.numYawDemandGain.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
			this.numYawDemandGain.Name = "numYawDemandGain";
			this.numYawDemandGain.Size = new System.Drawing.Size(73, 20);
			this.numYawDemandGain.TabIndex = 300;
			// 
			// numRollDemandGain
			// 
			this.numRollDemandGain.DecimalPlaces = 2;
			this.numRollDemandGain.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.numRollDemandGain.Location = new System.Drawing.Point(110, 46);
			this.numRollDemandGain.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.numRollDemandGain.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
			this.numRollDemandGain.Name = "numRollDemandGain";
			this.numRollDemandGain.Size = new System.Drawing.Size(73, 20);
			this.numRollDemandGain.TabIndex = 290;
			// 
			// numPitchDemandGain
			// 
			this.numPitchDemandGain.DecimalPlaces = 2;
			this.numPitchDemandGain.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.numPitchDemandGain.Location = new System.Drawing.Point(110, 20);
			this.numPitchDemandGain.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.numPitchDemandGain.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
			this.numPitchDemandGain.Name = "numPitchDemandGain";
			this.numPitchDemandGain.Size = new System.Drawing.Size(73, 20);
			this.numPitchDemandGain.TabIndex = 280;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(189, 152);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(49, 13);
			this.label2.TabIndex = 316;
			this.label2.Text = "[0.0, 1.0]";
			// 
			// label34
			// 
			this.label34.AutoSize = true;
			this.label34.Location = new System.Drawing.Point(7, 152);
			this.label34.Name = "label34";
			this.label34.Size = new System.Drawing.Size(88, 13);
			this.label34.TabIndex = 297;
			this.label34.Text = "Comp. filter alpha";
			// 
			// label26
			// 
			this.label26.AutoSize = true;
			this.label26.Location = new System.Drawing.Point(189, 126);
			this.label26.Name = "label26";
			this.label26.Size = new System.Drawing.Size(28, 13);
			this.label26.TabIndex = 313;
			this.label26.Text = "deg.";
			// 
			// label33
			// 
			this.label33.AutoSize = true;
			this.label33.Location = new System.Drawing.Point(7, 126);
			this.label33.Name = "label33";
			this.label33.Size = new System.Drawing.Size(44, 13);
			this.label33.TabIndex = 314;
			this.label33.Text = "Roll trim";
			// 
			// label25
			// 
			this.label25.AutoSize = true;
			this.label25.Location = new System.Drawing.Point(189, 100);
			this.label25.Name = "label25";
			this.label25.Size = new System.Drawing.Size(28, 13);
			this.label25.TabIndex = 297;
			this.label25.Text = "deg.";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(7, 100);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(50, 13);
			this.label4.TabIndex = 311;
			this.label4.Text = "Pitch trim";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(7, 74);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(92, 13);
			this.label3.TabIndex = 309;
			this.label3.Text = "Yaw demand gain";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(7, 48);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(89, 13);
			this.label1.TabIndex = 307;
			this.label1.Text = "Roll demand gain";
			// 
			// label24
			// 
			this.label24.AutoSize = true;
			this.label24.Location = new System.Drawing.Point(7, 22);
			this.label24.Name = "label24";
			this.label24.Size = new System.Drawing.Size(95, 13);
			this.label24.TabIndex = 305;
			this.label24.Text = "Pitch demand gain";
			// 
			// btnRequestResponseAndTrimParams
			// 
			this.btnRequestResponseAndTrimParams.Location = new System.Drawing.Point(6, 175);
			this.btnRequestResponseAndTrimParams.Name = "btnRequestResponseAndTrimParams";
			this.btnRequestResponseAndTrimParams.Size = new System.Drawing.Size(111, 34);
			this.btnRequestResponseAndTrimParams.TabIndex = 340;
			this.btnRequestResponseAndTrimParams.Text = "Request";
			this.btnRequestResponseAndTrimParams.UseVisualStyleBackColor = true;
			this.btnRequestResponseAndTrimParams.Click += new System.EventHandler(this.btnRequestResponseAndTrimParams_Click);
			// 
			// btnSendResponseAndTrimParams
			// 
			this.btnSendResponseAndTrimParams.Location = new System.Drawing.Point(123, 175);
			this.btnSendResponseAndTrimParams.Name = "btnSendResponseAndTrimParams";
			this.btnSendResponseAndTrimParams.Size = new System.Drawing.Size(111, 34);
			this.btnSendResponseAndTrimParams.TabIndex = 350;
			this.btnSendResponseAndTrimParams.Text = "Send";
			this.btnSendResponseAndTrimParams.UseVisualStyleBackColor = true;
			this.btnSendResponseAndTrimParams.Click += new System.EventHandler(this.btnSendResponseAndTrimParams_Click);
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.numYawRateILimit);
			this.groupBox1.Controls.Add(this.numRollRateILimit);
			this.groupBox1.Controls.Add(this.numPitchRateILimit);
			this.groupBox1.Controls.Add(this.numYawRateGainD);
			this.groupBox1.Controls.Add(this.numYawRateGainI);
			this.groupBox1.Controls.Add(this.numYawRateGainP);
			this.groupBox1.Controls.Add(this.numRollRateGainD);
			this.groupBox1.Controls.Add(this.numRollRateGainI);
			this.groupBox1.Controls.Add(this.numRollRateGainP);
			this.groupBox1.Controls.Add(this.numPitchRateGainD);
			this.groupBox1.Controls.Add(this.label22);
			this.groupBox1.Controls.Add(this.numPitchRateGainI);
			this.groupBox1.Controls.Add(this.btnRequestRotationRatePIDParams);
			this.groupBox1.Controls.Add(this.numPitchRateGainP);
			this.groupBox1.Controls.Add(this.btnSendRotationRatePIDParams);
			this.groupBox1.Controls.Add(this.label10);
			this.groupBox1.Controls.Add(this.label20);
			this.groupBox1.Location = new System.Drawing.Point(6, 194);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(432, 142);
			this.groupBox1.TabIndex = 301;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Rotation Rate            P                        I                         D    " +
    "                     iLimit";
			// 
			// numYawRateILimit
			// 
			this.numYawRateILimit.DecimalPlaces = 2;
			this.numYawRateILimit.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.numYawRateILimit.Location = new System.Drawing.Point(347, 75);
			this.numYawRateILimit.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.numYawRateILimit.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
			this.numYawRateILimit.Name = "numYawRateILimit";
			this.numYawRateILimit.Size = new System.Drawing.Size(73, 20);
			this.numYawRateILimit.TabIndex = 250;
			// 
			// numRollRateILimit
			// 
			this.numRollRateILimit.DecimalPlaces = 2;
			this.numRollRateILimit.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.numRollRateILimit.Location = new System.Drawing.Point(347, 49);
			this.numRollRateILimit.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.numRollRateILimit.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
			this.numRollRateILimit.Name = "numRollRateILimit";
			this.numRollRateILimit.Size = new System.Drawing.Size(73, 20);
			this.numRollRateILimit.TabIndex = 210;
			// 
			// numPitchRateILimit
			// 
			this.numPitchRateILimit.DecimalPlaces = 2;
			this.numPitchRateILimit.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.numPitchRateILimit.Location = new System.Drawing.Point(347, 22);
			this.numPitchRateILimit.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.numPitchRateILimit.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
			this.numPitchRateILimit.Name = "numPitchRateILimit";
			this.numPitchRateILimit.Size = new System.Drawing.Size(73, 20);
			this.numPitchRateILimit.TabIndex = 170;
			// 
			// numYawRateGainD
			// 
			this.numYawRateGainD.DecimalPlaces = 2;
			this.numYawRateGainD.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.numYawRateGainD.Location = new System.Drawing.Point(268, 75);
			this.numYawRateGainD.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.numYawRateGainD.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
			this.numYawRateGainD.Name = "numYawRateGainD";
			this.numYawRateGainD.Size = new System.Drawing.Size(73, 20);
			this.numYawRateGainD.TabIndex = 240;
			// 
			// numYawRateGainI
			// 
			this.numYawRateGainI.DecimalPlaces = 2;
			this.numYawRateGainI.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.numYawRateGainI.Location = new System.Drawing.Point(189, 75);
			this.numYawRateGainI.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.numYawRateGainI.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
			this.numYawRateGainI.Name = "numYawRateGainI";
			this.numYawRateGainI.Size = new System.Drawing.Size(73, 20);
			this.numYawRateGainI.TabIndex = 230;
			// 
			// numYawRateGainP
			// 
			this.numYawRateGainP.DecimalPlaces = 2;
			this.numYawRateGainP.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.numYawRateGainP.Location = new System.Drawing.Point(110, 75);
			this.numYawRateGainP.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.numYawRateGainP.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
			this.numYawRateGainP.Name = "numYawRateGainP";
			this.numYawRateGainP.Size = new System.Drawing.Size(73, 20);
			this.numYawRateGainP.TabIndex = 220;
			// 
			// numRollRateGainD
			// 
			this.numRollRateGainD.DecimalPlaces = 2;
			this.numRollRateGainD.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.numRollRateGainD.Location = new System.Drawing.Point(268, 49);
			this.numRollRateGainD.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.numRollRateGainD.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
			this.numRollRateGainD.Name = "numRollRateGainD";
			this.numRollRateGainD.Size = new System.Drawing.Size(73, 20);
			this.numRollRateGainD.TabIndex = 200;
			// 
			// numRollRateGainI
			// 
			this.numRollRateGainI.DecimalPlaces = 2;
			this.numRollRateGainI.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.numRollRateGainI.Location = new System.Drawing.Point(189, 49);
			this.numRollRateGainI.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.numRollRateGainI.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
			this.numRollRateGainI.Name = "numRollRateGainI";
			this.numRollRateGainI.Size = new System.Drawing.Size(73, 20);
			this.numRollRateGainI.TabIndex = 190;
			// 
			// numRollRateGainP
			// 
			this.numRollRateGainP.DecimalPlaces = 2;
			this.numRollRateGainP.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.numRollRateGainP.Location = new System.Drawing.Point(110, 49);
			this.numRollRateGainP.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.numRollRateGainP.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
			this.numRollRateGainP.Name = "numRollRateGainP";
			this.numRollRateGainP.Size = new System.Drawing.Size(73, 20);
			this.numRollRateGainP.TabIndex = 180;
			// 
			// numPitchRateGainD
			// 
			this.numPitchRateGainD.DecimalPlaces = 2;
			this.numPitchRateGainD.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.numPitchRateGainD.Location = new System.Drawing.Point(268, 22);
			this.numPitchRateGainD.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.numPitchRateGainD.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
			this.numPitchRateGainD.Name = "numPitchRateGainD";
			this.numPitchRateGainD.Size = new System.Drawing.Size(73, 20);
			this.numPitchRateGainD.TabIndex = 160;
			// 
			// label22
			// 
			this.label22.AutoSize = true;
			this.label22.Location = new System.Drawing.Point(6, 77);
			this.label22.Name = "label22";
			this.label22.Size = new System.Drawing.Size(77, 13);
			this.label22.TabIndex = 301;
			this.label22.Text = "Yaw rate gains";
			// 
			// numPitchRateGainI
			// 
			this.numPitchRateGainI.DecimalPlaces = 2;
			this.numPitchRateGainI.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.numPitchRateGainI.Location = new System.Drawing.Point(189, 22);
			this.numPitchRateGainI.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.numPitchRateGainI.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
			this.numPitchRateGainI.Name = "numPitchRateGainI";
			this.numPitchRateGainI.Size = new System.Drawing.Size(73, 20);
			this.numPitchRateGainI.TabIndex = 150;
			// 
			// btnRequestRotationRatePIDParams
			// 
			this.btnRequestRotationRatePIDParams.Location = new System.Drawing.Point(6, 101);
			this.btnRequestRotationRatePIDParams.Name = "btnRequestRotationRatePIDParams";
			this.btnRequestRotationRatePIDParams.Size = new System.Drawing.Size(111, 34);
			this.btnRequestRotationRatePIDParams.TabIndex = 260;
			this.btnRequestRotationRatePIDParams.Text = "Request";
			this.btnRequestRotationRatePIDParams.UseVisualStyleBackColor = true;
			this.btnRequestRotationRatePIDParams.Click += new System.EventHandler(this.btnRequestRotationRatePIDParams_Click);
			// 
			// numPitchRateGainP
			// 
			this.numPitchRateGainP.DecimalPlaces = 2;
			this.numPitchRateGainP.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.numPitchRateGainP.Location = new System.Drawing.Point(110, 22);
			this.numPitchRateGainP.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.numPitchRateGainP.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
			this.numPitchRateGainP.Name = "numPitchRateGainP";
			this.numPitchRateGainP.Size = new System.Drawing.Size(73, 20);
			this.numPitchRateGainP.TabIndex = 140;
			// 
			// btnSendRotationRatePIDParams
			// 
			this.btnSendRotationRatePIDParams.Location = new System.Drawing.Point(123, 101);
			this.btnSendRotationRatePIDParams.Name = "btnSendRotationRatePIDParams";
			this.btnSendRotationRatePIDParams.Size = new System.Drawing.Size(111, 34);
			this.btnSendRotationRatePIDParams.TabIndex = 270;
			this.btnSendRotationRatePIDParams.Text = "Send";
			this.btnSendRotationRatePIDParams.UseVisualStyleBackColor = true;
			this.btnSendRotationRatePIDParams.Click += new System.EventHandler(this.btnSendRotationRatePIDParams_Click);
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(6, 24);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(80, 13);
			this.label10.TabIndex = 291;
			this.label10.Text = "Pitch rate gains";
			// 
			// label20
			// 
			this.label20.AutoSize = true;
			this.label20.Location = new System.Drawing.Point(6, 51);
			this.label20.Name = "label20";
			this.label20.Size = new System.Drawing.Size(74, 13);
			this.label20.TabIndex = 287;
			this.label20.Text = "Roll rate gains";
			// 
			// label16
			// 
			this.label16.AutoSize = true;
			this.label16.Location = new System.Drawing.Point(565, 322);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(18, 13);
			this.label16.TabIndex = 282;
			this.label16.Text = "μs";
			// 
			// grpLevelling
			// 
			this.grpLevelling.Controls.Add(this.numRollLevelingILimit);
			this.grpLevelling.Controls.Add(this.numPitchLevelingILimit);
			this.grpLevelling.Controls.Add(this.numRollLevelingGainD);
			this.grpLevelling.Controls.Add(this.numRollLevelingGainI);
			this.grpLevelling.Controls.Add(this.numRollLevelingGainP);
			this.grpLevelling.Controls.Add(this.numPitchLevelingGainD);
			this.grpLevelling.Controls.Add(this.numPitchLevelingGainI);
			this.grpLevelling.Controls.Add(this.numPitchLevelingGainP);
			this.grpLevelling.Controls.Add(this.btnRequestLevelingPIDParams);
			this.grpLevelling.Controls.Add(this.btnSendLevelingPIDParams);
			this.grpLevelling.Controls.Add(this.label23);
			this.grpLevelling.Controls.Add(this.label21);
			this.grpLevelling.Location = new System.Drawing.Point(6, 72);
			this.grpLevelling.Name = "grpLevelling";
			this.grpLevelling.Size = new System.Drawing.Size(432, 116);
			this.grpLevelling.TabIndex = 297;
			this.grpLevelling.TabStop = false;
			this.grpLevelling.Text = "Levelling                    P                        I                         D" +
    "                         iLimit";
			// 
			// numRollLevelingILimit
			// 
			this.numRollLevelingILimit.DecimalPlaces = 5;
			this.numRollLevelingILimit.Increment = new decimal(new int[] {
            1,
            0,
            0,
            196608});
			this.numRollLevelingILimit.Location = new System.Drawing.Point(347, 48);
			this.numRollLevelingILimit.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.numRollLevelingILimit.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
			this.numRollLevelingILimit.Name = "numRollLevelingILimit";
			this.numRollLevelingILimit.Size = new System.Drawing.Size(73, 20);
			this.numRollLevelingILimit.TabIndex = 110;
			// 
			// numPitchLevelingILimit
			// 
			this.numPitchLevelingILimit.DecimalPlaces = 5;
			this.numPitchLevelingILimit.Increment = new decimal(new int[] {
            1,
            0,
            0,
            196608});
			this.numPitchLevelingILimit.Location = new System.Drawing.Point(347, 22);
			this.numPitchLevelingILimit.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.numPitchLevelingILimit.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
			this.numPitchLevelingILimit.Name = "numPitchLevelingILimit";
			this.numPitchLevelingILimit.Size = new System.Drawing.Size(73, 20);
			this.numPitchLevelingILimit.TabIndex = 70;
			// 
			// numRollLevelingGainD
			// 
			this.numRollLevelingGainD.DecimalPlaces = 5;
			this.numRollLevelingGainD.Increment = new decimal(new int[] {
            1,
            0,
            0,
            262144});
			this.numRollLevelingGainD.Location = new System.Drawing.Point(268, 49);
			this.numRollLevelingGainD.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.numRollLevelingGainD.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
			this.numRollLevelingGainD.Name = "numRollLevelingGainD";
			this.numRollLevelingGainD.Size = new System.Drawing.Size(73, 20);
			this.numRollLevelingGainD.TabIndex = 100;
			// 
			// numRollLevelingGainI
			// 
			this.numRollLevelingGainI.DecimalPlaces = 5;
			this.numRollLevelingGainI.Increment = new decimal(new int[] {
            1,
            0,
            0,
            262144});
			this.numRollLevelingGainI.Location = new System.Drawing.Point(189, 49);
			this.numRollLevelingGainI.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.numRollLevelingGainI.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
			this.numRollLevelingGainI.Name = "numRollLevelingGainI";
			this.numRollLevelingGainI.Size = new System.Drawing.Size(73, 20);
			this.numRollLevelingGainI.TabIndex = 90;
			// 
			// numRollLevelingGainP
			// 
			this.numRollLevelingGainP.DecimalPlaces = 5;
			this.numRollLevelingGainP.Increment = new decimal(new int[] {
            1,
            0,
            0,
            262144});
			this.numRollLevelingGainP.Location = new System.Drawing.Point(110, 49);
			this.numRollLevelingGainP.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.numRollLevelingGainP.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
			this.numRollLevelingGainP.Name = "numRollLevelingGainP";
			this.numRollLevelingGainP.Size = new System.Drawing.Size(73, 20);
			this.numRollLevelingGainP.TabIndex = 80;
			// 
			// numPitchLevelingGainD
			// 
			this.numPitchLevelingGainD.DecimalPlaces = 5;
			this.numPitchLevelingGainD.Increment = new decimal(new int[] {
            1,
            0,
            0,
            262144});
			this.numPitchLevelingGainD.Location = new System.Drawing.Point(268, 21);
			this.numPitchLevelingGainD.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.numPitchLevelingGainD.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
			this.numPitchLevelingGainD.Name = "numPitchLevelingGainD";
			this.numPitchLevelingGainD.Size = new System.Drawing.Size(73, 20);
			this.numPitchLevelingGainD.TabIndex = 60;
			// 
			// numPitchLevelingGainI
			// 
			this.numPitchLevelingGainI.DecimalPlaces = 5;
			this.numPitchLevelingGainI.Increment = new decimal(new int[] {
            1,
            0,
            0,
            262144});
			this.numPitchLevelingGainI.Location = new System.Drawing.Point(189, 21);
			this.numPitchLevelingGainI.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.numPitchLevelingGainI.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
			this.numPitchLevelingGainI.Name = "numPitchLevelingGainI";
			this.numPitchLevelingGainI.Size = new System.Drawing.Size(73, 20);
			this.numPitchLevelingGainI.TabIndex = 50;
			// 
			// numPitchLevelingGainP
			// 
			this.numPitchLevelingGainP.DecimalPlaces = 5;
			this.numPitchLevelingGainP.Increment = new decimal(new int[] {
            1,
            0,
            0,
            262144});
			this.numPitchLevelingGainP.Location = new System.Drawing.Point(110, 21);
			this.numPitchLevelingGainP.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.numPitchLevelingGainP.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
			this.numPitchLevelingGainP.Name = "numPitchLevelingGainP";
			this.numPitchLevelingGainP.Size = new System.Drawing.Size(73, 20);
			this.numPitchLevelingGainP.TabIndex = 40;
			// 
			// btnRequestLevelingPIDParams
			// 
			this.btnRequestLevelingPIDParams.Location = new System.Drawing.Point(6, 75);
			this.btnRequestLevelingPIDParams.Name = "btnRequestLevelingPIDParams";
			this.btnRequestLevelingPIDParams.Size = new System.Drawing.Size(111, 34);
			this.btnRequestLevelingPIDParams.TabIndex = 120;
			this.btnRequestLevelingPIDParams.Text = "Request";
			this.btnRequestLevelingPIDParams.UseVisualStyleBackColor = true;
			this.btnRequestLevelingPIDParams.Click += new System.EventHandler(this.btnRequestLevelingPIDParams_Click);
			// 
			// btnSendLevelingPIDParams
			// 
			this.btnSendLevelingPIDParams.Location = new System.Drawing.Point(123, 76);
			this.btnSendLevelingPIDParams.Name = "btnSendLevelingPIDParams";
			this.btnSendLevelingPIDParams.Size = new System.Drawing.Size(111, 34);
			this.btnSendLevelingPIDParams.TabIndex = 130;
			this.btnSendLevelingPIDParams.Text = "Send";
			this.btnSendLevelingPIDParams.UseVisualStyleBackColor = true;
			this.btnSendLevelingPIDParams.Click += new System.EventHandler(this.btnSendLevelingPIDParams_Click);
			// 
			// label23
			// 
			this.label23.AutoSize = true;
			this.label23.Location = new System.Drawing.Point(6, 24);
			this.label23.Name = "label23";
			this.label23.Size = new System.Drawing.Size(98, 13);
			this.label23.TabIndex = 291;
			this.label23.Text = "Pitch leveling gains";
			// 
			// label21
			// 
			this.label21.AutoSize = true;
			this.label21.Location = new System.Drawing.Point(6, 51);
			this.label21.Name = "label21";
			this.label21.Size = new System.Drawing.Size(92, 13);
			this.label21.TabIndex = 287;
			this.label21.Text = "Roll leveling gains";
			// 
			// btnCalibrateIMU
			// 
			this.btnCalibrateIMU.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.btnCalibrateIMU.Location = new System.Drawing.Point(561, 437);
			this.btnCalibrateIMU.Name = "btnCalibrateIMU";
			this.btnCalibrateIMU.Size = new System.Drawing.Size(111, 34);
			this.btnCalibrateIMU.TabIndex = 470;
			this.btnCalibrateIMU.Text = "Re-calibrate IMU";
			this.btnCalibrateIMU.UseVisualStyleBackColor = true;
			this.btnCalibrateIMU.Click += new System.EventHandler(this.btnCalibrateIMU_Click);
			// 
			// btnPing
			// 
			this.btnPing.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.btnPing.Location = new System.Drawing.Point(561, 397);
			this.btnPing.Name = "btnPing";
			this.btnPing.Size = new System.Drawing.Size(111, 34);
			this.btnPing.TabIndex = 460;
			this.btnPing.Text = "Flash LED";
			this.btnPing.UseVisualStyleBackColor = true;
			this.btnPing.Click += new System.EventHandler(this.btnPing_Click);
			// 
			// btnSaveParamsAs
			// 
			this.btnSaveParamsAs.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnSaveParamsAs.Location = new System.Drawing.Point(444, 517);
			this.btnSaveParamsAs.Name = "btnSaveParamsAs";
			this.btnSaveParamsAs.Size = new System.Drawing.Size(111, 34);
			this.btnSaveParamsAs.TabIndex = 440;
			this.btnSaveParamsAs.Text = "Save Params As...";
			this.btnSaveParamsAs.UseVisualStyleBackColor = true;
			this.btnSaveParamsAs.Click += new System.EventHandler(this.btnSaveParamsAs_Click);
			// 
			// txtCurrentDeltaTime
			// 
			this.txtCurrentDeltaTime.Location = new System.Drawing.Point(521, 319);
			this.txtCurrentDeltaTime.Name = "txtCurrentDeltaTime";
			this.txtCurrentDeltaTime.ReadOnly = true;
			this.txtCurrentDeltaTime.Size = new System.Drawing.Size(38, 20);
			this.txtCurrentDeltaTime.TabIndex = 400;
			this.txtCurrentDeltaTime.Text = "N/A";
			// 
			// label17
			// 
			this.label17.AutoSize = true;
			this.label17.Location = new System.Drawing.Point(445, 322);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(54, 13);
			this.label17.TabIndex = 281;
			this.label17.Text = "Delta time";
			// 
			// btnReloadParams
			// 
			this.btnReloadParams.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnReloadParams.Location = new System.Drawing.Point(444, 357);
			this.btnReloadParams.Name = "btnReloadParams";
			this.btnReloadParams.Size = new System.Drawing.Size(111, 34);
			this.btnReloadParams.TabIndex = 410;
			this.btnReloadParams.Text = "Reload Params";
			this.btnReloadParams.UseVisualStyleBackColor = true;
			this.btnReloadParams.Click += new System.EventHandler(this.button1_Click);
			// 
			// btnSaveParams
			// 
			this.btnSaveParams.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnSaveParams.Location = new System.Drawing.Point(444, 397);
			this.btnSaveParams.Name = "btnSaveParams";
			this.btnSaveParams.Size = new System.Drawing.Size(111, 34);
			this.btnSaveParams.TabIndex = 420;
			this.btnSaveParams.Text = "Save Params";
			this.btnSaveParams.UseVisualStyleBackColor = true;
			this.btnSaveParams.Click += new System.EventHandler(this.btnSaveParams_Click);
			// 
			// btnAssignID
			// 
			this.btnAssignID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnAssignID.Location = new System.Drawing.Point(123, 46);
			this.btnAssignID.Name = "btnAssignID";
			this.btnAssignID.Size = new System.Drawing.Size(78, 20);
			this.btnAssignID.TabIndex = 5;
			this.btnAssignID.Text = "Assign ID:";
			this.btnAssignID.UseVisualStyleBackColor = true;
			this.btnAssignID.Click += new System.EventHandler(this.btnAssignID_Click);
			// 
			// txtCurrentID
			// 
			this.txtCurrentID.Location = new System.Drawing.Point(521, 215);
			this.txtCurrentID.Name = "txtCurrentID";
			this.txtCurrentID.ReadOnly = true;
			this.txtCurrentID.Size = new System.Drawing.Size(117, 20);
			this.txtCurrentID.TabIndex = 360;
			this.txtCurrentID.Text = "N/A";
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(445, 218);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(50, 13);
			this.label8.TabIndex = 98;
			this.label8.Text = "Drone ID";
			// 
			// chkBroadcast
			// 
			this.chkBroadcast.AutoSize = true;
			this.chkBroadcast.Location = new System.Drawing.Point(9, 46);
			this.chkBroadcast.Name = "chkBroadcast";
			this.chkBroadcast.Size = new System.Drawing.Size(74, 17);
			this.chkBroadcast.TabIndex = 4;
			this.chkBroadcast.Text = "Broadcast";
			this.chkBroadcast.UseVisualStyleBackColor = true;
			this.chkBroadcast.CheckedChanged += new System.EventHandler(this.chkBroadcast_CheckedChanged);
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(274, 50);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(34, 13);
			this.label7.TabIndex = 95;
			this.label7.Text = "0-254";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(6, 23);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(60, 13);
			this.label6.TabIndex = 94;
			this.label6.Text = "Unicast ID:";
			// 
			// btnStartIMUTest
			// 
			this.btnStartIMUTest.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.btnStartIMUTest.Location = new System.Drawing.Point(561, 477);
			this.btnStartIMUTest.Name = "btnStartIMUTest";
			this.btnStartIMUTest.Size = new System.Drawing.Size(111, 34);
			this.btnStartIMUTest.TabIndex = 480;
			this.btnStartIMUTest.Text = "Start IMU Plot";
			this.btnStartIMUTest.UseVisualStyleBackColor = true;
			this.btnStartIMUTest.Click += new System.EventHandler(this.btnStartIMUTest_Click);
			// 
			// btnRequestStatus
			// 
			this.btnRequestStatus.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.btnRequestStatus.Location = new System.Drawing.Point(561, 357);
			this.btnRequestStatus.Name = "btnRequestStatus";
			this.btnRequestStatus.Size = new System.Drawing.Size(111, 34);
			this.btnRequestStatus.TabIndex = 450;
			this.btnRequestStatus.Text = "Request Status";
			this.btnRequestStatus.UseVisualStyleBackColor = true;
			this.btnRequestStatus.Click += new System.EventHandler(this.btnRequestValues_Click);
			// 
			// label31
			// 
			this.label31.AutoSize = true;
			this.label31.Location = new System.Drawing.Point(644, 296);
			this.label31.Name = "label31";
			this.label31.Size = new System.Drawing.Size(22, 13);
			this.label31.TabIndex = 75;
			this.label31.Text = "mV";
			// 
			// txtCurrentBattery
			// 
			this.txtCurrentBattery.Location = new System.Drawing.Point(521, 293);
			this.txtCurrentBattery.Name = "txtCurrentBattery";
			this.txtCurrentBattery.ReadOnly = true;
			this.txtCurrentBattery.Size = new System.Drawing.Size(117, 20);
			this.txtCurrentBattery.TabIndex = 390;
			this.txtCurrentBattery.Text = "N/A";
			// 
			// label32
			// 
			this.label32.AutoSize = true;
			this.label32.Location = new System.Drawing.Point(445, 296);
			this.label32.Name = "label32";
			this.label32.Size = new System.Drawing.Size(40, 13);
			this.label32.TabIndex = 74;
			this.label32.Text = "Battery";
			// 
			// label29
			// 
			this.label29.AutoSize = true;
			this.label29.Location = new System.Drawing.Point(644, 270);
			this.label29.Name = "label29";
			this.label29.Size = new System.Drawing.Size(28, 13);
			this.label29.TabIndex = 72;
			this.label29.Text = "deg.";
			// 
			// txtCurrentRoll
			// 
			this.txtCurrentRoll.Location = new System.Drawing.Point(521, 267);
			this.txtCurrentRoll.Name = "txtCurrentRoll";
			this.txtCurrentRoll.ReadOnly = true;
			this.txtCurrentRoll.Size = new System.Drawing.Size(117, 20);
			this.txtCurrentRoll.TabIndex = 380;
			this.txtCurrentRoll.Text = "N/A";
			// 
			// label30
			// 
			this.label30.AutoSize = true;
			this.label30.Location = new System.Drawing.Point(445, 270);
			this.label30.Name = "label30";
			this.label30.Size = new System.Drawing.Size(54, 13);
			this.label30.TabIndex = 71;
			this.label30.Text = "Roll angle";
			// 
			// label27
			// 
			this.label27.AutoSize = true;
			this.label27.Location = new System.Drawing.Point(644, 244);
			this.label27.Name = "label27";
			this.label27.Size = new System.Drawing.Size(28, 13);
			this.label27.TabIndex = 69;
			this.label27.Text = "deg.";
			// 
			// txtCurrentPitch
			// 
			this.txtCurrentPitch.Location = new System.Drawing.Point(521, 241);
			this.txtCurrentPitch.Name = "txtCurrentPitch";
			this.txtCurrentPitch.ReadOnly = true;
			this.txtCurrentPitch.Size = new System.Drawing.Size(117, 20);
			this.txtCurrentPitch.TabIndex = 370;
			this.txtCurrentPitch.Text = "N/A";
			// 
			// label28
			// 
			this.label28.AutoSize = true;
			this.label28.Location = new System.Drawing.Point(445, 244);
			this.label28.Name = "label28";
			this.label28.Size = new System.Drawing.Size(60, 13);
			this.label28.TabIndex = 68;
			this.label28.Text = "Pitch angle";
			// 
			// picQuadcopter
			// 
			this.picQuadcopter.Image = ((System.Drawing.Image)(resources.GetObject("picQuadcopter.Image")));
			this.picQuadcopter.InitialImage = ((System.Drawing.Image)(resources.GetObject("picQuadcopter.InitialImage")));
			this.picQuadcopter.Location = new System.Drawing.Point(448, 12);
			this.picQuadcopter.Name = "picQuadcopter";
			this.picQuadcopter.Size = new System.Drawing.Size(230, 197);
			this.picQuadcopter.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.picQuadcopter.TabIndex = 7;
			this.picQuadcopter.TabStop = false;
			// 
			// btnRangeTest
			// 
			this.btnRangeTest.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.btnRangeTest.Location = new System.Drawing.Point(561, 517);
			this.btnRangeTest.Name = "btnRangeTest";
			this.btnRangeTest.Size = new System.Drawing.Size(111, 34);
			this.btnRangeTest.TabIndex = 490;
			this.btnRangeTest.Text = "Start Range Test";
			this.btnRangeTest.UseVisualStyleBackColor = true;
			this.btnRangeTest.Click += new System.EventHandler(this.btnRangeTest_Click);
			// 
			// tmrRangeTest
			// 
			this.tmrRangeTest.Interval = 1000;
			this.tmrRangeTest.Tick += new System.EventHandler(this.tmrRangeTest_Tick);
			// 
			// chtIMUChart
			// 
			this.chtIMUChart.BorderlineColor = System.Drawing.Color.DimGray;
			this.chtIMUChart.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
			chartArea1.AxisX.Interval = 1000D;
			chartArea1.AxisX.LabelStyle.Interval = 1000D;
			chartArea1.AxisX.MajorGrid.LineColor = System.Drawing.Color.Gainsboro;
			chartArea1.AxisX.Maximum = 5000D;
			chartArea1.AxisX.Minimum = 0D;
			chartArea1.AxisX.Title = "Time (ms)";
			chartArea1.AxisY.Interval = 5D;
			chartArea1.AxisY.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
			chartArea1.AxisY.IntervalOffsetType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
			chartArea1.AxisY.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
			chartArea1.AxisY.IsLabelAutoFit = false;
			chartArea1.AxisY.IsStartedFromZero = false;
			chartArea1.AxisY.LabelAutoFitMaxFontSize = 7;
			chartArea1.AxisY.LabelAutoFitMinFontSize = 7;
			chartArea1.AxisY.LabelStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			chartArea1.AxisY.MajorGrid.Interval = 10D;
			chartArea1.AxisY.MajorGrid.LineColor = System.Drawing.Color.Gainsboro;
			chartArea1.AxisY.Maximum = 45D;
			chartArea1.AxisY.MaximumAutoSize = 20F;
			chartArea1.AxisY.Minimum = -45D;
			chartArea1.AxisY.MinorGrid.Enabled = true;
			chartArea1.AxisY.MinorGrid.Interval = 5D;
			chartArea1.AxisY.MinorGrid.LineColor = System.Drawing.Color.WhiteSmoke;
			chartArea1.Name = "ChartArea1";
			chartArea1.Position.Auto = false;
			chartArea1.Position.Height = 98F;
			chartArea1.Position.Width = 100F;
			chartArea1.Position.Y = 1F;
			this.chtIMUChart.ChartAreas.Add(chartArea1);
			this.chtIMUChart.Dock = System.Windows.Forms.DockStyle.Fill;
			legend1.BackColor = System.Drawing.Color.Transparent;
			legend1.DockedToChartArea = "ChartArea1";
			legend1.Name = "Legend1";
			this.chtIMUChart.Legends.Add(legend1);
			this.chtIMUChart.Location = new System.Drawing.Point(3, 16);
			this.chtIMUChart.Name = "chtIMUChart";
			this.chtIMUChart.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Pastel;
			series1.ChartArea = "ChartArea1";
			series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
			series1.Legend = "Legend1";
			series1.Name = "Pitch";
			series1.Points.Add(dataPoint1);
			series1.Points.Add(dataPoint2);
			series2.ChartArea = "ChartArea1";
			series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
			series2.Legend = "Legend1";
			series2.Name = "Roll";
			series3.ChartArea = "ChartArea1";
			series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
			series3.Legend = "Legend1";
			series3.Name = "Yaw rate";
			this.chtIMUChart.Series.Add(series1);
			this.chtIMUChart.Series.Add(series2);
			this.chtIMUChart.Series.Add(series3);
			this.chtIMUChart.Size = new System.Drawing.Size(984, 195);
			this.chtIMUChart.TabIndex = 4;
			// 
			// tmrIMUTest
			// 
			this.tmrIMUTest.Interval = 5;
			this.tmrIMUTest.Tick += new System.EventHandler(this.tmrIMUTest_Tick);
			// 
			// grpChart
			// 
			this.grpChart.Controls.Add(this.chtIMUChart);
			this.grpChart.Location = new System.Drawing.Point(12, 585);
			this.grpChart.Name = "grpChart";
			this.grpChart.Size = new System.Drawing.Size(990, 214);
			this.grpChart.TabIndex = 4;
			this.grpChart.TabStop = false;
			this.grpChart.Text = "IMU Plot (deg.)";
			// 
			// FlightPanel
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1018, 811);
			this.Controls.Add(this.grpChart);
			this.Controls.Add(this.grpCommands);
			this.Controls.Add(this.grpData);
			this.Controls.Add(this.grpLog);
			this.Controls.Add(this.grpInputMethod);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.Name = "FlightPanel";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "uBee Flight Config Panel";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.RemoteControl_FormClosing);
			this.grpInputMethod.ResumeLayout(false);
			this.grpInputMethod.PerformLayout();
			this.grpLog.ResumeLayout(false);
			this.grpLog.PerformLayout();
			this.grpData.ResumeLayout(false);
			this.grpData.PerformLayout();
			this.grpCommands.ResumeLayout(false);
			this.grpCommands.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.numUnicastID)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numNewID)).EndInit();
			this.groupBox3.ResumeLayout(false);
			this.groupBox3.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.numCompFilterAlpha)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numRollTrim)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numPitchTrim)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numYawDemandGain)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numRollDemandGain)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numPitchDemandGain)).EndInit();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.numYawRateILimit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numRollRateILimit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numPitchRateILimit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numYawRateGainD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numYawRateGainI)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numYawRateGainP)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numRollRateGainD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numRollRateGainI)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numRollRateGainP)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numPitchRateGainD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numPitchRateGainI)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numPitchRateGainP)).EndInit();
			this.grpLevelling.ResumeLayout(false);
			this.grpLevelling.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.numRollLevelingILimit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numPitchLevelingILimit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numRollLevelingGainD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numRollLevelingGainI)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numRollLevelingGainP)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numPitchLevelingGainD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numPitchLevelingGainI)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numPitchLevelingGainP)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.picQuadcopter)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chtIMUChart)).EndInit();
			this.grpChart.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.GroupBox grpInputMethod;
		private System.Windows.Forms.Button btnConnect;
		private System.Windows.Forms.GroupBox grpLog;
		private System.Windows.Forms.TextBox txtLog;
		private System.Windows.Forms.Label lblJoystickValues;
		private System.Windows.Forms.Timer tmrInputTick;
		private System.Windows.Forms.TextBox txtPort;
		private System.Windows.Forms.GroupBox grpData;
		private System.Windows.Forms.GroupBox grpCommands;
		private System.Windows.Forms.PictureBox picQuadcopter;
		private System.Windows.Forms.Button btnPing;
		private System.Windows.Forms.Button btnRangeTest;
		private System.Windows.Forms.Timer tmrRangeTest;
		private System.Windows.Forms.Button btnReloadParams;
		private System.Windows.Forms.Button btnCalibrateIMU;
		private System.Windows.Forms.Button btnSaveParams;
		private System.Windows.Forms.Label label31;
		private System.Windows.Forms.TextBox txtCurrentBattery;
		private System.Windows.Forms.Label label32;
		private System.Windows.Forms.Label label29;
		private System.Windows.Forms.TextBox txtCurrentRoll;
		private System.Windows.Forms.Label label30;
		private System.Windows.Forms.Label label27;
		private System.Windows.Forms.TextBox txtCurrentPitch;
		private System.Windows.Forms.Label label28;
		private System.Windows.Forms.Button btnRequestStatus;
		private System.Windows.Forms.Button btnStartIMUTest;
		private System.Windows.Forms.DataVisualization.Charting.Chart chtIMUChart;
		private System.Windows.Forms.Timer tmrIMUTest;
		private System.Windows.Forms.GroupBox grpChart;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.CheckBox chkBroadcast;
		private System.Windows.Forms.TextBox txtCurrentID;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Button btnAssignID;
		private System.Windows.Forms.Button btnSaveParamsAs;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.TextBox txtCurrentDeltaTime;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Label label23;
		private System.Windows.Forms.Label label21;
		private System.Windows.Forms.GroupBox grpLevelling;
		private System.Windows.Forms.Button btnRequestLevelingPIDParams;
		private System.Windows.Forms.Button btnSendLevelingPIDParams;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label22;
		private System.Windows.Forms.Button btnRequestRotationRatePIDParams;
		private System.Windows.Forms.Button btnSendRotationRatePIDParams;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.Label label34;
		private System.Windows.Forms.Label label26;
		private System.Windows.Forms.Label label33;
		private System.Windows.Forms.Label label25;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label24;
		private System.Windows.Forms.Button btnRequestResponseAndTrimParams;
		private System.Windows.Forms.Button btnSendResponseAndTrimParams;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.NumericUpDown numPitchLevelingGainP;
		private System.Windows.Forms.NumericUpDown numRollLevelingGainD;
		private System.Windows.Forms.NumericUpDown numRollLevelingGainI;
		private System.Windows.Forms.NumericUpDown numRollLevelingGainP;
		private System.Windows.Forms.NumericUpDown numPitchLevelingGainD;
		private System.Windows.Forms.NumericUpDown numPitchLevelingGainI;
		private System.Windows.Forms.NumericUpDown numYawRateGainD;
		private System.Windows.Forms.NumericUpDown numYawRateGainI;
		private System.Windows.Forms.NumericUpDown numYawRateGainP;
		private System.Windows.Forms.NumericUpDown numRollRateGainD;
		private System.Windows.Forms.NumericUpDown numRollRateGainI;
		private System.Windows.Forms.NumericUpDown numRollRateGainP;
		private System.Windows.Forms.NumericUpDown numPitchRateGainD;
		private System.Windows.Forms.NumericUpDown numPitchRateGainI;
		private System.Windows.Forms.NumericUpDown numPitchRateGainP;
		private System.Windows.Forms.NumericUpDown numCompFilterAlpha;
		private System.Windows.Forms.NumericUpDown numRollTrim;
		private System.Windows.Forms.NumericUpDown numPitchTrim;
		private System.Windows.Forms.NumericUpDown numYawDemandGain;
		private System.Windows.Forms.NumericUpDown numRollDemandGain;
		private System.Windows.Forms.NumericUpDown numPitchDemandGain;
		private System.Windows.Forms.NumericUpDown numNewID;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.NumericUpDown numUnicastID;
		private System.Windows.Forms.NumericUpDown numYawRateILimit;
		private System.Windows.Forms.NumericUpDown numRollRateILimit;
		private System.Windows.Forms.NumericUpDown numPitchRateILimit;
		private System.Windows.Forms.NumericUpDown numRollLevelingILimit;
		private System.Windows.Forms.NumericUpDown numPitchLevelingILimit;
		private System.Windows.Forms.Button btnLoadParams;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox txtCurrentHz;
	}
}

