Monday, August 6th
------------------

altitude control is a challenge when working with hand-held controller---here are the options:

1) standard drone remote control with one axis not auto centering; this will become the altitude axis
	- I hate this configuration because it's hard to adjust altitude without affecting other flight controls
	- also, I can't find a thumb joystick on DigiKey that would allow this

2) use the trigger buttons for ascent/descent
	- we have to be careful here; we need to make sure we take off very quickly (at full throttle) to have a smooth steady take off---this means that the
	- A. OR, we could have the right trigger mean full throttle when the throttle is already < 20 or whatever, and then the triggers would just increase/decrease gradually whenever throttle is > 20 or whatever
		- this is also nice because we could implement altitude stabilization this way
		- we could also implement up/down altitude control instead of throttle control, too
		- I like this option
	- B. OR, we could have set commands for landing and takeoff
		- meh; this is tough because we could then theoretically still land without engaging the land command, so this isn't a great option
	- C. OR, we could just have one single command for automated takeoff, and the triggers just make gradual altitude adjustments
		- this is pretty similar to A, except that here we have an actual takeoff command; BUT, an actual takeoff command gives us less control and is slightly trickier to implement: I'd rather have full control all the time, especially if we need to take off at an angle or something
	- D. OR, hold the right trigger and use the right joystick Y axis to control altitude changes; otherwise, we just hover
		- no, because then we can't change altitude and also whatever the right joystick normally does, at the same time

3) use a separate potentiometer for altitude control
	- no. just no.